﻿using System.Web;

namespace P4PI.Common
{
    public class EncodingHelper
    {

        public static string Decode(string url)
        {
            return HttpUtility.UrlDecode(url);
        }

        public static string Encode(string url)
        {
            return HttpUtility.UrlEncode(url);
        }

    }
}
