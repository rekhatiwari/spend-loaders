﻿using System;
using System.Net;

namespace P4PI.Common
{
    public class CookieAwareWebClient : System.Net.WebClient
    {

        public int? TimeOut { get; set; }

        private CookieContainer m_container = new CookieContainer();

        protected override WebRequest GetWebRequest(Uri address)
        {
            //// Uncomment to see cookie collection being passed around
            //var c = m_container.GetCookies(address);
            //for (int i = 0; i < c.Count; i++)
            //{
            //    var j = c[i];
            //}

            WebRequest request = base.GetWebRequest(address);
            request.Timeout = this.TimeOut ?? 30000;    // default to 30 seconds
            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).CookieContainer = m_container;
            }
            return request;
        }
    }
}
