﻿using System.Net;
using System.Web;

namespace P4PI.Common
{
    public static class ContextHelper
    {

        /// <summary>
        /// Gets SessionId from HttpContext
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>sessionid string</returns>
        public static string GetSessionId(HttpContext context)
        {
            if (!string.IsNullOrEmpty(context.Request.QueryString["sessionId"]))
                return context.Request.QueryString["sessionId"].ToString();
            else
                return context.Session != null ? context.Session.SessionID : "unknown";
        }

        /// <summary>
        /// Gets UserAgent From HttpContext
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>UA string</returns>
        public static string GetUserAgent(HttpContext context)
        {
            try
            {
                //string uaFromQueryString = null;
                //if (!String.IsNullOrEmpty(context.Request["ua"]))
                //{
                //    uaFromQueryString = (string)context.Request["ua"];
                //    return uaFromQueryString;
                //}

                return context.Request.UserAgent == null ? string.Empty : context.Request.UserAgent;
            }
            catch
            {
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets the X-Forwarded For IP from the HttpContext
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>Xffip as string</returns>
        public static string GetXffIp(HttpContext context)
        {

            if (context == null) return "";

            string __ip = context.Request.Headers["X-Forwarded-For"];

            if (string.IsNullOrEmpty(__ip)) return "";

            if (__ip.IndexOf(',') > 0)
            {
                bool found = false;
                string[] ipTests = __ip.Split(new char[] { ',' });
                IPAddress address;
                foreach (string ipTest in ipTests)
                {
                    if (IPAddress.TryParse(ipTest, out address))
                    {
                        found = true;
                        __ip = address.ToString();
                        break;
                    }
                }

                if (!found)
                {
                    __ip = "127.0.0.1";
                }
            }
            else
                __ip = "";

            return __ip;

        }

        /// <summary>
        /// Gets ip from HttpContext
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>ip string</returns>
        public static string GetIp(HttpContext context)
        {
            try
            {
                //string ipAddressFromQueryString = null;
                //if (!String.IsNullOrEmpty(context.Request["ip"]))
                //{
                //    ipAddressFromQueryString = (string)context.Request["ip"];
                //    return ipAddressFromQueryString;
                //}

                System.Net.IPAddress address;
                if (!string.IsNullOrEmpty(context.Request.Headers["X-Forwarded-For"] as string))
                {
                    string ip = context.Request.Headers["X-Forwarded-For"] as string;
                    int commaIndex = ip.IndexOf(',');
                    if (commaIndex > 0)
                    {
                        bool found = false;
                        string[] ipTests = ip.Split(new char[] { ',' });
                        foreach (string ipTest in ipTests)
                        {
                            if (System.Net.IPAddress.TryParse(ipTest, out address))
                            {
                                found = true;
                                ip = address.ToString();
                                break;
                            }
                        }

                        if (!found)
                        {
                            ip = "127.0.0.1";
                        }
                    }
                    return ip;
                }
                if (System.Net.IPAddress.TryParse(context.Request.UserHostAddress, out address))
                {
                    return address.ToString();
                }
                return "127.0.0.1";
            }
            catch
            {
            }
            return string.Empty;
        }


        /// <summary>
        /// Gets entry page from request
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>entrypage string</returns>
        public static string GetEntryPage(HttpContext context)
        {
            return context.Request.Url != null ? context.Request.Url.AbsoluteUri : string.Empty;
        }


        /// <summary>
        /// Impossible to capture referrer from request
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns></returns>
        public static string GetReferrer(HttpContext context)
        {
            return context.Request.UrlReferrer != null ? context.Request.UrlReferrer.AbsoluteUri : string.Empty;
        }

        /// <summary>
        /// Gets the root domain from the context
        /// ex. abcsite.com/?page=123 returns abcsite.com
        /// </summary>
        /// <param name="context">httpcontext</param>
        /// <returns>string</returns>
        public static string RootDomain(HttpContext context)
        {
            return context.Request.Url.Host;
        }


        /// <summary>
        /// Gets the server name from the context
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>string server name</returns>
        public static string GetServer(HttpContext context)
        {
            return context.Server != null ? context.Server.MachineName : string.Empty;
        }

    }
}
