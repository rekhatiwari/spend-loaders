﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track
{
	public static class DateFormats
	{
		public const string DEFAULT			= "yyyy-MM-dd";
		public const string DATE_AND_TIME	= "MM/dd/yy H:mm:ss";
		public const string DAY_AND_TIME	= "MMM dd, hh:mm tt";
		public const string DAY				= "ddd-dd";
	}
}
