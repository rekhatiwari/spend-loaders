﻿using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Track.Domain.Interfaces;
using Track.Domain.Repositories;
using Track.Domain.Services;
using System.Web.WebPages;

namespace Track
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Graphs", // Route name
                "graphs/index", // URL with parameters
                new { controller = "Graphs", action = "Index" } // Parameter defaults
            );

            routes.MapRoute(
                "Api", // Route name
                "api/{action}", // URL with parameters
                new { controller = "TrackApi", action = "Index" } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

		protected void InitializeIoc()
		{
			// Autofac Ioc Integration
			var builder = new ContainerBuilder();
			builder.RegisterModule(new AutofacWebTypesModule());
			builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();

			//builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<RSTRepository>().As<IRSTRepository>();
            builder.RegisterType<SpendService>().As<ISpendService>().PropertiesAutowired();
            builder.RegisterType<FileService>().As<IFileService>();
            builder.RegisterType<LogService>().As<ILogService>();
			builder.RegisterType<HttpService>().As<IHttpService>();
			//builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<GhostService>().As<IGhostService>().PropertiesAutowired();
            builder.RegisterType<CacheService>().As<ICacheService>().SingleInstance();  // want every call to share the same cache
            builder.RegisterType<ParsingService>().As<IParsingService>();
           // builder.RegisterType<AWSService>().As<IAWSService>();
            builder.RegisterType<YahooGeminiService>().As<IYahooGeminiService>();
            
            var container = builder.Build();
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
		}
        protected void Application_Start()
        {
			InitializeIoc();

            //Start the Scheduling service
            //new ScheduledTasksService();

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode("mobile")
            {
                ContextCondition = Context =>
                                Context.Request.Browser["IsMobile"] == "True"
            });
        }

        protected void Application_Error()
        {
            // Logging Service is normally created by Unity.
            // We are kind of outside the normal Unity construction, which starts at the Controllers.
            // So create it here manually. This is probably one of (or the only) exception to this in the app.

            //var log = new LoggingService
            //{
            //    SessionService = new SessionService
            //    {
            //        HttpContext = new HttpContextWrapper(Context),
            //    },
            //};

            // Log all of the errors recursively through the inner exceptions

            var ex = Context.Server.GetLastError();
            Application[Context.Request.UserHostAddress] = ex;


           // Server.Transfer("~/Content/ErrorPages/Default.html");
        }


    }
}