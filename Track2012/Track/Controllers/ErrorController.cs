﻿using System;
using System.Web;
using System.Web.Mvc;
using Track.Controllers.Abstract;

namespace Track.Controllers
{
	public class ErrorController : BaseController
	{
		[OutputCache(Duration=0, VaryByCustom="none")]
		public ActionResult Index()
		{
			var ex = HttpContext.Application[Request.UserHostAddress] as Exception;

			ViewBag.Description = ex != null
				? ex.Message
				: "An error occurred.";

			ViewBag.Title = "Ah Crap! An error has occurred.";

			return View("Error");
		}

		public ActionResult NotFound()
		{
			ViewBag.Title = "The page you requested was not found.";

			return View("Error");
		}
	}
}
