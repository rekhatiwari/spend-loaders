﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Track.Common;
using Track.Common.ActionResults;
using Track.Controllers.Abstract;
using Track.Domain.Interfaces;

namespace Track.Controllers
{
    [Authorize(Roles = Globals.TrackGroup + ", " + Globals.TrackReadOnlyGroup)]
    public class FilesController : AuthenticatedController
    {
		public IFileService FileService { get; set; }

        public ActionResult GetDoc(string fileName)
        {
            var file = FileService.getDocument(fileName);

            if (!file.Exists)
            {
                throw new Exception(fileName + " not found");
            }
            return new FileActionResult(file.OpenRead())
            {
                ContentType = "application/x-download",
                FileName = fileName
            };

        }

        public ActionResult GetFile(string fileName)
        {
            var file = FileService.getDocument(fileName);

            if (!file.Exists)
            {
                throw new Exception(fileName + " not found");
            }
            return new FileActionResult(file.OpenRead())
            {
                ContentType = "application/x-download",
                FileName = fileName
            };

        }

        public ActionResult GetTrackingScript(string fileName, string fileType)
        {

            var file = FileService.GetTrackingScript(fileName);


            //Replace the header
            string fileTypeString = (fileType == null) ? "" : (string)fileType;
            if (fileTypeString.Equals("wordfence"))
            {
                StreamReader reader = new StreamReader(file.OpenRead());
                string text = reader.ReadToEnd();

                return new FileActionResult(FileService.GenerateThinScript(FileService.GenerateWordFenceScript(text)))
                {
                    ContentType = "application/php",
                    FileName = "wordfence.php"
                };
            }
            if (fileTypeString.Equals("thin"))
            {
                StreamReader reader = new StreamReader(file.OpenRead());
                string text = reader.ReadToEnd();

                return new FileActionResult(FileService.GenerateThinScript(text))
                {
                    ContentType = "application/php",
                    FileName = "index.php",
                };
            }
            return new FileActionResult(file.OpenRead())
            {
                ContentType = "application/php",
                FileName = "p4tracking.php"
            };
        }

        public ActionResult GetStyleScript(string fileName, string fileType)
        {

            var file = FileService.GetStyleScript(fileName);


            //Replace the header
            string fileTypeString = (fileType == null) ? "" : (string)fileType;
            
            return new FileActionResult(file.OpenRead())
            {
                ContentType = "application/php",
                FileName = "p4Styles.php"
            };
        }

        

        [ChildActionOnly]
		public ActionResult UploadTrackingScript()
		{
			if (User.IsInRole(Globals.TrackAdmin))
				return PartialView();

			return new EmptyResult();
		}


		[HttpPost]
		[Authorize(Roles = Globals.TrackAdmin)]
		public ActionResult UploadTrackingScript(HttpPostedFileBase file)
		{
			FileService.SaveTrackingScript(file);
			return RedirectToActionWithHash("Index", "TrackingScripts", new {controller = "Sites"});
		}

        public ActionResult BuildRDPFile(string host, string username, string password)
        {
            //var rdpStub = "full address:s:{0}\nscreen mode id:i:2\ndesktopwidth:i:1366\ndesktopheight:i:768\nusername:s:admin\npassword 51:b:{1}";
            //rdpStub = string.Format(rdpStub, host, EncryptionService.EncryptPw(password));
            var rdpStub = "full address:s:{0}\nscreen mode id:i:2\ndesktopwidth:i:1366\ndesktopheight:i:768\nusername:s:{1}";
            rdpStub = string.Format(rdpStub, host, username);
            return new FileActionResult(rdpStub)
            {
                ContentType = "application/x-rdp",
                FileName = string.Format("{0}.rdp", host)
            };
        }
    }
}
