﻿using System;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using Track.Controllers.Abstract;
using Track.Domain.Enums;
using Track.Domain.Interfaces;
using Track.Domain.Services;

namespace Track.Controllers
{
    public class GhostController : BaseController
    {
        public IGhostService GhostService { get; set; }
        public ICacheService CacheService { get; set; }

        public ActionResult DailySpendLoad()
        {
            var entries = RSTRepository.GetAllGhostEntries();
            foreach (int id in entries)
            {
                var entry = RSTRepository.GetGhostEntry(id);
                if (entry != null)
                {
                    GhostService.ProcessGhostQueueItem(entry);
                }
            }
            return new EmptyResult();
        }

        public ActionResult PollAccount(int id)
        {
            var entry = RSTRepository.GetGhostEntry(id);
            if (entry != null)
            {
                GhostService.ProcessGhostQueueItem(entry);
            }
            return new EmptyResult();
        }

        public ActionResult LoadGhost(int spendAccountId, DateTime startDate, DateTime endDate)
        {
            var ghostEntry = RSTRepository.GetGhostQueueForSpendAccount(spendAccountId);

            var loopDate = startDate;
            if (ghostEntry != null && ghostEntry.GhostQueue.Count > 0)
            {
                while (loopDate <= endDate)
                {
                    GhostService.ProcessItem(ghostEntry.GhostQueue.First(), loopDate);
                    Thread.Sleep(3000);
                    loopDate = loopDate.AddDays(1);

                    //Wait 5 seconds between date loads
                    if (loopDate <= endDate)
                        Thread.Sleep(5000);
                }
            }


            return new EmptyResult();
        }

        public ActionResult SendDailyStatus()
        {
            RSTRepository.SendDailyStatusEmail();
            return new EmptyResult();
        }

    }
}
