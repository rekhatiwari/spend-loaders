﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Track.Domain.Interfaces;
using Track.Data;
using Track.Common.ActionResults;

namespace Track.Controllers.Abstract
{
    public abstract class BaseController : Controller
    {
		//public IUserRepository UserRepository { get; set; }
        public IRSTRepository RSTRepository { get; set; }

		protected override void Initialize(System.Web.Routing.RequestContext requestContext)
		{
			base.Initialize(requestContext);
		}

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			if (filterContext.ActionParameters.ContainsKey("redirectUrl"))
			{
				ViewBag.RedirectUrl = filterContext.ActionParameters["redirectUrl"];
			}

			base.OnActionExecuting(filterContext);
		}

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding)
        {
            return new JsonNetResult
            {
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                Data = data
            };
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonNetResult
            {
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                Data = data,
                JsonRequestBehavior = behavior
            };
        }
    }
}
