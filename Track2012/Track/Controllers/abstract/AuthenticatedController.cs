﻿using System.Collections.Generic;
using System.Web.Mvc;
using Track.Domain.Interfaces;
using Track.Data;
using Track.Domain.Services;

namespace Track.Controllers.Abstract
{
	[Authorize]
    public abstract class AuthenticatedController : BaseController
    {
		public ILogService LogService { get; set; }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
			// INFO: if new repositories/services are added here, be sure to add them to the InitializeIoC in Global.asax

            base.Initialize(requestContext);
        }

		protected RedirectResult RedirectToActionWithHash(string actionName, string hash, object routeValues = null)
		{
			var url = Url.Action(actionName, routeValues);

			if (!string.IsNullOrWhiteSpace(hash))
			{
				url += "#" + hash;
			}

			return new RedirectResult(url);
		}

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{

			base.OnActionExecuting(filterContext);
		}

    }
}
