﻿using System;
using System.Web.Mvc;
using Track.Common;
using Track.Controllers.Abstract;
using Track.Data;
using Track.Domain.ViewModels;

namespace Track.Controllers
{
    [Authorize(Roles = Globals.TrackGroup + ", " + Globals.TrackReadOnlyGroup)]
    public class DashboardController : AuthenticatedController
    {
        public ActionResult Index()
        {
			var model = new DashboardViewModel()
			{
				FinstatsSummary = RSTRepository.FinstatsSummary(DateTime.Today.AddDays(-8), DateTime.Today.AddDays(-1), User.Identity.Name, null, null, null)
            };

            return View(model);
        }

	}
}
