﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Track.Domain.Enums;
using Track.Extensions;

namespace Track.Common.Attributes
{
	public class ReportParametersAttribute : ActionFilterAttribute
	{
		public ReportParametersAttribute()
		{

		}

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var from = HttpUtility.UrlDecode(filterContext.RequestContext.HttpContext.Request.QueryString["from"] ?? "").TryParseAs<DateTime>();
			var to = HttpUtility.UrlDecode(filterContext.RequestContext.HttpContext.Request.QueryString["to"] ?? "").TryParseAs<DateTime>();
			var dateRange = filterContext.RequestContext.HttpContext.Request.QueryString["dateRange"].TryParseAs<DateRanges>();

			if (!from.HasValue && !to.HasValue && dateRange.HasValue)
			{
				switch (dateRange.Value)
				{
					case DateRanges.Yesterday:
						from = DateTime.Today.AddDays(-1);
						to = DateTime.Today.AddDays(-1);
						break;
					case DateRanges.LastThreeDays:
						from = DateTime.Today.AddDays(-4);
						to = DateTime.Today.AddDays(-1);
						break;
					case DateRanges.LastWeek:
						from = DateTime.Today.AddDays(-8);
						to = DateTime.Today.AddDays(-1);
						break;
					case DateRanges.LastMonth:
						from = DateTime.Today.AddDays(-31);
						to = DateTime.Today.AddDays(-1);
						break;
					case DateRanges.MonthToDate:
						from = DateTime.Today.AddDays(-1 * (DateTime.Today.Day - 1));
						to = DateTime.Today.AddDays(-1);
						break;
				}
			}

			filterContext.ActionParameters["from"] = from ?? DateTime.Today.AddDays(-1);
			filterContext.ActionParameters["to"] = to ?? DateTime.Today.AddDays(-1);
		}
	}
}
