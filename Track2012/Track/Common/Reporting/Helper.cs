﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Track.Data;
using Track.Domain.Models;
using Track.ReportService;

namespace Track.Common.Reporting
{
    public class Helper
    {

        public static List<ReportListModel> GetAllReportsInFolder(string folder)
        {
            CatalogItem[] items = null;
            using (ReportService.ReportingService2005SoapClient client = new ReportService.ReportingService2005SoapClient())
            {
                client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                client.ClientCredentials.UserName.UserName = "SGITRReportUser";
                client.ClientCredentials.UserName.Password = "SGITR@flow!";

                client.ListChildren(folder, false, out items);
            }

            return items == null ? new List<ReportListModel>() : 
                items.Select(i => new ReportListModel 
                    { 
                        ReportName = i.Name, 
                        Uri = new UriBuilder("https://reports.p4pi.net/Reports/Pages/Report.aspx?ItemPath=" + HttpUtility.UrlEncode(i.Path)).Uri
                    }).ToList();
        }

    }
}