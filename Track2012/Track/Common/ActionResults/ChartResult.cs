﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;
using Track.ReportExecution;
using System.Net;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;
using System.Collections;

namespace Track.Common.ActionResults
{
	public class ChartResult : BinaryResult
	{
		public Size Size { get; set; }

		public string ChartTitle { get; set; }
		public IEnumerable AxisX { get; set; }
		public IEnumerable AxisY { get; set; }
		public SeriesChartType ChartType { get; set; }
		public bool Is3d { get; set; }
		public Color BackColor { get; set; }

		public ChartResult()
        {
            contentType = "image/png";
        }

        public override void ExecuteResult(ControllerContext context)
        {
            base.ExecuteResult(context);
        }

        protected override byte[] getBytes()
        {
			using (var ch = new Chart())
			{
				ch.Titles.Add(ChartTitle);
				ch.BackColor = BackColor;

				var area = new ChartArea();
				ch.ChartAreas.Add(area);

				if (Is3d)
				{
					var areaStyle = new ChartArea3DStyle(area);
					areaStyle.Enable3D = true;
					area.Area3DStyle = areaStyle;
				}

				var series = new Series();
				series.Font = new Font("Segoe UI", 8.0f, FontStyle.Regular);
				series.Legend = "ChartLegend";
				series.ChartType = ChartType;
				series.Points.DataBindXY(AxisX, AxisY);
				ch.Series.Add(series);
				ch.Size = this.Size;

				using (var ms = new MemoryStream())
				{
					ch.SaveImage(ms, ChartImageFormat.Png);
					return ms.ToArray();
				}
			}
        }
	}
}

