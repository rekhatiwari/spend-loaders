﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;
using Track.ReportExecution;
using System.Net;
using System.IO;

namespace Track.Common.ActionResults
{
	public abstract class BinaryResult : ActionResult
	{
        protected string contentType { get; set; }
        protected string fileName { get; set; }

        protected abstract byte[] getBytes();

		public override void ExecuteResult(ControllerContext context)
		{
			var response = context.HttpContext.Response;

			// Try to get the bytes prior to setting content-disposition and setting
			// ContentType so that the site will redirect to error page if an error
			// occurrs.
			var bytes = getBytes();

			response.Clear();
            if (!string.IsNullOrEmpty(fileName))
            {
                response.AddHeader("content-disposition", string.Format("attachment;filename={0}", fileName));
            }

			response.ContentType = contentType;

			response.BinaryWrite(bytes);
			response.End();
		}
	}
}

