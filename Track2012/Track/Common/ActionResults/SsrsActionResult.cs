﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;
using System.Web.Security;
using Track.ReportExecution;
using System.Net;
using System.IO;
using System.Security.Principal;
using System.ServiceModel.Security;

namespace Track.Common.ActionResults
{
	public enum ReportFormats
	{
		Pdf,
		Html,
		Excel,
	}

	public class SsrsActionResult : BinaryResult
	{
		public string FileName { get { return fileName; } set { fileName = value; } }
		public string ReportPath { get; set; }
		public IDictionary<string,string> Parameters { get; set; }
		public ReportFormats Format { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }

        protected override byte[] getBytes()
        {
            var client = new ReportExecutionServiceSoapClient();
            if (client.ClientCredentials != null)
            {
                client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Delegation;
                client.ClientCredentials.UserName.UserName = UserName;
                client.ClientCredentials.UserName.Password = Password;
            }


            var historyId = null as string;

            var execInfo = new ExecutionInfo();
            var trustedUserHeader = new TrustedUserHeader();
            var serviceInfo = new ServerInfoHeader();
            var execHeader = client.LoadReport(trustedUserHeader, ReportPath, historyId, out serviceInfo, out execInfo);

            var execParams = Parameters.Select(o =>
                    new ParameterValue() { Name = o.Key, Value = o.Value }
                ).ToArray();

            client.SetExecutionParameters(execHeader, trustedUserHeader, execParams, "en-US", out execInfo);

            string extension, encoding, mimeType;
            var warning = new[] { new Warning() };
            string[] streamIds;
            byte[] result;
            client.Render(execHeader, trustedUserHeader, Format.ToString("g"), null /* deviceInfo */
                    , out result
                    , out extension
                    , out mimeType
                    , out encoding
                    , out warning
                    , out streamIds
                );

            return result;

        }

		public override void ExecuteResult(ControllerContext context)
		{
			contentType = "application/" + getExtension();

			if (string.IsNullOrEmpty(fileName))
	            fileName = Path.GetFileName(string.Format("{0}.{1}", ReportPath, getExtension()));

            base.ExecuteResult(context);
		}

		protected string getExtension()
		{
			switch (Format)
			{
				case ReportFormats.Excel:
					return "vnd.ms-excel";
				case ReportFormats.Html:
					return "html";
				default:
					return "pdf";
			}
		}
	}
}

