﻿using System.IO;
using System.Text;
using System.Web.Mvc;

namespace Track.Common.ActionResults
{
	public class FileActionResult : BinaryResult
	{
		public string ContentType { get; set; }
		public string FileName { get { return fileName; } set { fileName = value; } }
		protected byte[] bytes { get; set; }
		public FileActionResult(Stream stream, string contentType = null)
		{
			ContentType = contentType;
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                stream.Position = 0;
                bytes = ms.ToArray();
            }
		}
		public FileActionResult(string fileContents, string contentType = null)
		{
			ContentType = contentType;
			bytes = Encoding.UTF8.GetBytes(fileContents);
		}

		public override void ExecuteResult(ControllerContext context)
		{
			contentType = ContentType ?? "plain/text";

			base.ExecuteResult(context);
		}

		protected override byte[] getBytes()
		{

			return bytes;
		}


	}
}

