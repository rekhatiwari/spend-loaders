﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;
using Track.ReportExecution;
using System.Net;
using System.IO;

namespace Track.Common.ActionResults
{
	public class ImageActionResult : BinaryResult
	{
        public string ContentType { get; set; }
        protected byte[] bytes { get; set; }
        public ImageActionResult(Stream stream, string contentType = null)
        {
            ContentType = contentType;

            if (stream == null) bytes = new byte[0];

            // Try to reset the stream in case someone has already used it.
            if (stream.CanSeek)
            {
                stream.Position = 0;
            }

            try
            {
                var bytesList = new List<byte>();
                var buffer = new byte[8192];
                var count = 0;
                do
                {
                    count = stream.Read(buffer, 0, buffer.Length);
                    if (count != 0)
                    {
                        bytesList.AddRange(buffer);
                    }

                } while (count > 0);



                bytes = bytesList.ToArray();
            }
            catch (Exception)
            {

            }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            contentType = ContentType ?? "image/png";

            base.ExecuteResult(context);
        }

        protected override byte[] getBytes()
        {

            return bytes;

        }


	}
}

