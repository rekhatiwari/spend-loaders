﻿using System.Net.Mail;

namespace Track.Common.Email
{
    public class EmailHelper
    {
        public static void Send(string toAddress, string subject, string body)
        {
            using (MailMessage message = new MailMessage("reports@richrivermedia.com", toAddress, subject, body))
            {
                SmtpClient client = new SmtpClient();
                client.Send(message);
            }
        }
    }
}