﻿namespace Track.Common
{
    public static class Globals
    {
        public const string TrackGroup = "TrackerUsers";
        public const string TrackAdmin = "TrackerAdmins";
        public const string TrackReadOnlyGroup = "Track-ReadOnly";

        public const string TrackScript = "TrackScripts";
        public const string TrackAdknownWhitelistUsers = "AdknownWhitelistUsers";

        public const int DefaultTestInterval = 60;
        public const int DefaultReportingInterval = 4320;
    }
}