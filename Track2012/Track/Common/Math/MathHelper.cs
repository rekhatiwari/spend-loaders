﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Track.Common.Math
{
    public class MathHelper
    {
        // Divides x by y. If y is zero, then the method will return zero as the result
        public static decimal Divide(decimal x, decimal y)
        {
            if (y == 0)
                return 0;
            else
                return x / y;
        }
    }
}