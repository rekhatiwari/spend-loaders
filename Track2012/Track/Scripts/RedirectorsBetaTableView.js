﻿var redirectorTableColumns = [];

var dataTable = null;

$(document).ready(function () {

    $("#loadingDialog").dialog({
        autoOpen: false,
        height: 150,
        width: 400,
        weight: 50,
        resizable: false,
        modal: true,
        draggable: false,
        open: function (event, ui) { $(".ui-dialog-titlebar-close", $(this).parent()).hide(); }
    });

    $("#savingDialog").dialog({
        autoOpen: false,
        height: 150,
        width: 400,
        weight: 50,
        resizable: false,
        modal: true,
        draggable: false,
        open: function (event, ui) { $(".ui-dialog-titlebar-close", $(this).parent()).hide(); }
    });

    $("#refreshingDataDialog").dialog({
        autoOpen: false,
        height: 150,
        width: 400,
        weight: 50,
        resizable: false,
        modal: true,
        draggable: false,
        open: function (event, ui) { $(".ui-dialog-titlebar-close", $(this).parent()).hide(); }
    });

    $("#loadingTQGraphDialog").dialog({
        autoOpen: false,
        height: 150,
        width: 400,
        weight: 50,
        resizable: false,
        modal: true,
        draggable: false,
        open: function (event, ui) { $(".ui-dialog-titlebar-close", $(this).parent()).hide(); }
    });

    $("#redirect-history-beta").dialog({
        title: 'Redirect History',
        autoOpen: false,
        height: 800,
        width: 800,
        weight: 50,
        resizable: false,
        modal: true,
        draggable: false,
    });

   

    $('#dataTableSearch').on('keyup', function () {
        dataTable.search(this.value).draw();
    });

    String.prototype.format = function () {
        var formatted = this;
        for (var i = 0; i < arguments.length; i++) {
            var regexp = new RegExp('\\{' + i + '\\}', 'gi');
            formatted = formatted.replace(regexp, arguments[i]);
        }
        return formatted;
    };

    function getApiTotal(api, columnName) {
        // Remove the formatting to get integer data for summation
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$\*\)\(\%,]/g, '').replace(/<[\s\S]*?>/g, "") * (i.indexOf(')') > 0 ? -1 : 1) :
                typeof i === 'number' ?
                i : 0;
        };
        var columnId = api.column(columnName, { page: 'current' }).index();
        var isParentId =  api.column(".redirectorsTableColumnParent").index();

        var totalColumns = api.rows().data().filter(function (item){
            return (item[isParentId] === "True");
        });
        return totalColumns.reduce(function (a, b) {
            return a + intVal(b[columnId]);
                           }, 0);

    }

    /* Footer */
    var footerCallback = function (row, data, start, end, display) {

        var api = this.api(), data;

        var getColumn = function (totalRows, columnName) {
            return totalRows.filter(function (obj) {
                return (obj.name == columnName);
            })[0].index;
        }
       
        
        var totalRows = [
            { name: 'clicks', index: ".redirectorsTableColumnClicks", text: "{0}" },
            { name: 'sessions', index: ".redirectorsTableColumnSessions", text: "{0}" },
            { name: 'purchasedclicks', index: ".redirectorsTableColumnPurchasedClicks", text: "{0}" },
            { name: 'paidclicks', index: ".redirectorsTableColumnPaidClicks", text: "{0}" },
            { name: 'revenue', index: ".redirectorsTableColumnRevenue", text: "${1}" },
            { name: 'spend', index: ".redirectorsTableColumnSpend", text: "${1}" },
            { name: 'gp', index: ".redirectorsTableColumnGp", text: "${1}" },
            { name: 'ctr', index: ".redirectorsTableColumnCtr", text: "{0} %" },
            { name: 'purchasedctr', index: ".redirectorsTableColumnPurchasedCtr", text: "{0} %" },
            { name: 'cpc', index: ".redirectorsTableColumnCpc", text: "${1}" },
            { name: 'purchasedcpc', index: ".redirectorsTableColumnPurchasedCpc", text: "${1}" },
            { name: 'rpc', index: ".redirectorsTableColumnRpc", text: "${1}" },
            { name: 'rpi', index: ".redirectorsTableColumnRpi", text: "${1}" },
            { name: 'purchasedrpi', index: ".redirectorsTableColumnPurchasedRpi", text: "${1}" },
            { name: 'weight', index: ".redirectorsTableColumnWeight", text: "{0}" },
            { name: 'roas', index: ".redirectorsTableColumnRoas", text: "{0} %" },
        ];

        for (var i = 0; i < totalRows.length; i++) {
            if (totalRows[i].index < 0)
                continue;

            var columnName = totalRows[i].index

            switch (totalRows[i].name) {
                case "clicks":
                case "sessions":
                case "purchasedclicks":
                case "paidclicks":
                case "revenue":
                case "spend":
                case "gp":
                case "weight":
                    // Total over all pages
                    total = getApiTotal(api, columnName);
                    break;
                case "roas":
                    total = (getApiTotal(api, getColumn(totalRows, 'revenue')) - getApiTotal(api, getColumn(totalRows, 'spend'))) * 100 / getApiTotal(api, getColumn(totalRows, 'spend'));
                    break;
                case "ctr":
                    total = (getApiTotal(api, getColumn(totalRows, 'clicks')) * 100) / getApiTotal(api, getColumn(totalRows, 'sessions'));
                    break;
                case "cpc":
                    total = (getApiTotal(api, getColumn(totalRows, 'spend'))) / getApiTotal(api, getColumn(totalRows, 'purchasedclicks'));
                    break;
                case "rpc":
                    total = (getApiTotal(api, getColumn(totalRows, 'revenue'))) / getApiTotal(api, getColumn(totalRows, 'clicks'));
                    break;
                case "rpi":
                    total = (getApiTotal(api, getColumn(totalRows, 'revenue'))) / getApiTotal(api, getColumn(totalRows, 'sessions'));
                    break;
                case "purchasedctr":
                    total = (getApiTotal(api, getColumn(totalRows, 'clicks')) * 100) / getApiTotal(api, getColumn(totalRows, 'purchasedclicks'));
                    break;
                case "purchasedcpc":
                    total = (getApiTotal(api, getColumn(totalRows, 'spend'))) / getApiTotal(api, getColumn(totalRows, 'purchasedclicks'));
                    break;
                case "purchasedrpi":
                    total = (getApiTotal(api, getColumn(totalRows, 'revenue'))) / getApiTotal(api, getColumn(totalRows, 'purchasedclicks'));
                    break;
            }

            // Update footer
            if (total) {
                $(api.column(columnName).footer()).html(
                   (((total < 0) ? "(" : "") + totalRows[i].text + ((total < 0) ? ")" : "")).format(Math.abs(Math.round(total)).toLocaleString(), Number(Math.abs(total)).toFixed(2).toLocaleString())
                );
            } else {
                $(api.column(columnName).footer()).html("");
            }
        }
    };

    // enable datepicker functionality for date fields
    $("#startDate").datepicker({ dateFormat: "yy-mm-dd" });
    $("#endDate").datepicker({ dateFormat: "yy-mm-dd" });

    // retrieve dateSelector values from sessionContext
    getDateSelectorContext();

    dataTable = $('.beta.dataTable').DataTable({
        lengthChange : false,
        pageLength: 999,
		retrieve : true,
        
        columnDefs: [
            { width: 210, targets: "redirectorsTableColumnExpander" },
            { width: 200, targets: "redirectorsTableColumnCampaignAdGroup" },
            { type: 'num-html', targets: "redirectorsTableColumnSessions" },
            { type: 'num-html', targets: "redirectorsTableColumnClicks" },
            { type: 'num-html', targets: "redirectorsTableColumnCtr" },
            { type: 'num-html', targets: "redirectorsTableColumnSessions" },
            { type: 'currency', targets: "redirectorsTableColumnGp" }

             
        ],
        order: [[ 5, 'desc' ]],
        stateSave: true,
        pagingType: "simple_numbers",
        fixedHeader: {
            header: true,
        },
        searching: true,
        footerCallback: footerCallback,
        drawCallback : function (){
            //Add the even class
            $('.beta.dataTable tbody tr:visible').each(function (i) {
                $(this).removeClass('even odd');
                if (i % 2 == 0) {
                    $(this).addClass('even');
                } else {
                    $(this).addClass('odd');
                }
            });

        },
        stripeClasses: []
    });

    
    //Re-apply the search term
    $('#dataTableSearch').val(dataTable.search());


    handleView();



});



/* 
 * Switching the view 
 */


function handleView() {
    
    var view = $("#redirectorsTableViewTemplate").val();
    var filter = $("#redirectorsTableFilterTemplate").val();

    // must first show all columns 
    dataTable.columns().visible(true, false);


    var hiddenColumns = [$(".redirectorsTableColumnParent").index()];



    if (filter == "Active") {
        hiddenColumns.push($(".redirectorsTableColumnEnabled").index());
    }

    // now hide columns based on view (Full/Performance/Traffic Routing).  Note that for a 'Full' view, we do not have to do anything (already showing all columns)
    if (view == "Performance") {
        hiddenColumns.push($(".redirectorsTableColumnDestination").index());
        hiddenColumns.push($(".redirectorsTableColumnProvider").index());
        hiddenColumns.push($(".redirectorsTableColumnImplementationType").index());
    }
    else if (view == "Traffic Routing") {
        hiddenColumns.push($(".redirectorsTableColumnCpc").index());
        hiddenColumns.push($(".redirectorsTableColumnRpc").index());
        hiddenColumns.push($(".redirectorsTableColumnRpi").index());
        hiddenColumns.push($(".redirectorsTableColumnSpend").index());
        hiddenColumns.push($(".redirectorsTableColumnRevenue").index());
        hiddenColumns.push($(".redirectorsTableColumnGp").index());
        hiddenColumns.push($(".redirectorsTableColumnRoas").index());
    } else if (view == "Ratio") {
        hiddenColumns.push($(".redirectorsTableColumnOptions").index());
        hiddenColumns.push($(".redirectorsTableColumnProvider").index());
        hiddenColumns.push($(".redirectorsTableColumnWeight").index());
        hiddenColumns.push($(".redirectorsTableColumnCtr").index());
        hiddenColumns.push($(".redirectorsTableColumnCpc").index());
        hiddenColumns.push($(".redirectorsTableColumnRpc").index());
        hiddenColumns.push($(".redirectorsTableColumnTq").index());
        hiddenColumns.push($(".redirectorsTableColumnImplementationType").index());
        hiddenColumns.push($(".redirectorsTableColumnDestination").index());
        hiddenColumns.push($(".redirectorsTableColumnSessions").index());
        hiddenColumns.push($(".redirectorsTableColumnRpi").index());
        hiddenColumns.push($(".redirectorsTableColumnSpend").index());
        hiddenColumns.push($(".redirectorsTableColumnRevenue").index());
        hiddenColumns.push($(".redirectorsTableColumnGp").index());
        hiddenColumns.push($(".redirectorsTableColumnRoas").index());
    }
    else if (view == "Purchased Clicks") {
        hiddenColumns.push($(".redirectorsTableColumnSessions").index());

        hiddenColumns.push($(".redirectorsTableColumnCtr").index());
        hiddenColumns.push($(".redirectorsTableColumnCpc").index());
        hiddenColumns.push($(".redirectorsTableColumnRpi").index());

        hiddenColumns.push($(".redirectorsTableColumnPaidClicks").index());
        hiddenColumns.push($(".redirectorsTableColumnClickPercent").index());
    } else { // view == "Full"
        
        hiddenColumns.push($(".redirectorsTableColumnPurchasedClicks").index());
        hiddenColumns.push($(".redirectorsTableColumnPurchasedCtr").index());
        hiddenColumns.push($(".redirectorsTableColumnPurchasedCpc").index());
        hiddenColumns.push($(".redirectorsTableColumnPurchasedRpi").index());
        hiddenColumns.push($(".redirectorsTableColumnPaidClicks").index());
        hiddenColumns.push($(".redirectorsTableColumnClickPercent").index());
    }
  

    $(hiddenColumns).each(function (index, item) {
        dataTable.columns(item).visible(false, false);
    });
    dataTable.columns.adjust().draw(false);
    
}

$(function () {
    $("#redirectorsTableViewTemplate").change(function () {

        handleView();

        //Force datatables draw to redo the totals
        dataTable.draw();

        //Also save it to the db, ignore the results
        $.post(RedirectorSaveViewSelectorUrl, { RedirectorView: $("#redirectorsTableViewTemplate").val() });

        var reloadCounter = 0;
        //Reload all open funnel terms
        $(".expando-trigger[toggle='[+]']").each(function (index, value) {
            reloadCounter++;
            if (reloadCounter == 1) {
                $('#loadingDialog').removeAttr('style');
                $('#loadingDialog').dialog('open');
            }
            loadKeywordsTable($(this).data('redirectid'), "loading");
        });

    });
});

$('#redirectorsTableFilterTemplate').change(function () {
    //Also save it to the db, ignore the results
    $.post(RedirectorEnabledFilterUrl, {
        redirectorsTableFilter: $("#redirectorsTableFilterTemplate").val()
    }, function () {
        window.location.reload();
    });
});

$('#ToggleSiteSummary').on('click', function () {
    //Also save it to the db, ignore the results
    $.post(ToggleSummaryUrl, {
    }, function () {
        window.location.reload();
    });
});


/*
 * Graphing Ajax 
 */

$(document).on('click','.enableGraphing', function(event){
    if ($(this).hasClass('enabled'))
        return true;
    var enable = confirm("Do you want to enable Graphing?");
    var item = $(this);
    if (enable) {
        $.post(enableGraphingUrl + "/" +  $(this).data('redirectid')
        , function (json) {
            $("#Notifications").load(DisplayUrl);

            if (json.Model) {
                item.addClass("enabled");
            }
        },
        "json");
    }
    return false;
});

$(document).on('click','.clone', function(event){
    var jsonParams = new Object();
    jsonParams.id = $(this).data('redirectid');
    $.post(CloneRedirectRulesUrl, jsonParams, function (data) {
            // reload window upon completing clone rule
            window.location.reload();
        }
    , "json");
});

$(".redirectorsTableRow .last-updated").live("click", function (event) {

    event.preventDefault();
    $("#redirect-history-beta table").DataTable().destroy();
    $("#redirect-history-beta").dialog('open');
    $("#loadingDialog").dialog("open");
    $.post(RetrieveRedirectHistory, { redirectId: $(this).data("redirectid"), campaign: $(this).data("campaignname") }, function (data) {
        $("#redirect-history-beta tbody").empty();

        for (var i = 0; i < data.length; i++) {
            var row = data[i];
            $("#redirect-history-beta tbody").append("<tr><td>" + row.Timestamp + "</td><td>" + row.User + "</td><td>" + row.RedirectId + "</td><td>" + row.Type + "</td><td>" + row.Note + "</td></tr>");
        }
        
        $("#redirect-history-beta table").DataTable({
            "pageLength": 20, "order": [[0, "desc"]], "columnDefs": [{
                "targets": 3,
                "data": "note",
                "render": function (data, type, full, meta) {
                    return data.split("\n").join("<br/>");
                }
            }]
        });

        $("#loadingDialog").dialog("close");
    }, "json");


    return false;
});