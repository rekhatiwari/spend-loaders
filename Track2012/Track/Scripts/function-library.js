﻿function encodeJsStringToJs(str) {
    return str.replace("\\", "\\\\").replace("\"", "\\\"").replace("\'", "\\\'");
}