﻿$(function () {

	$("#menu").buttonset();

	$("a.ui-state-active")
		.mouseover(function () { $(this).removeClass("ui-state-active"); })
		.mouseout(function () { $(this).addClass("ui-state-active"); });

	$("input:submit, button, a.ui-button").livequery(function () { $(this).button(); });
	$(".tabs").livequery(function() { $(this).easytabs({ uiTabs: true }); });
	$(".fancybox").livequery(function () { $(this).fancybox(); });
	$(".select-all").livequery(function () { $(this).click(function () { $(this).select(); }); });

	$(".editable").livequery(function () {

		var type = $(this).attr("type");
		var config = { type: type };

		var title = $(this).attr("title");
		if (title != null && title != "")
			config.tooltip = title;

		var submit = $(this).attr("submit");
		if (submit != null && submit != "")
			config.submit = submit;

		if (type == "select") {
			var loadUrl = $(this).attr("loadUrl");
			if (loadUrl != null && loadUrl != "")
				config.loadurl = loadUrl;
		}

		var saveUrl = $(this).attr("href");
		$(this).editable(saveUrl, config);
	});

	$(".ui-table").livequery(function () {

		if ($(this).attr("dt-no-init") == "true") return;

		var config = {
			"bJQueryUI": false,
			"bPaginate": false,
			"bFilter": true,
			"bSort": true,
			"iDisplayLength": 10
		};
		config.retrieve = true;

		var bJQueryUI = $(this).attr("dt-jqueryui");
		if (bJQueryUI != null && bJQueryUI != "") {
			config.bJQueryUI = bJQueryUI == "true";
		}

		var paginate = $(this).attr("dt-paginate");
		if (paginate != null && paginate != "") {
			config.bPaginate = paginate == "true";
		}

		var filter = $(this).attr("dt-filter");
		if (filter != null && filter != "") {
			config.bFilter = filter == "true";
		}

		var sort = $(this).attr("dt-sort");
		if (sort != null && sort != "") {
		    config.bSort = sort == "true";
		}

		var pagelength = $(this).attr("dt-pagelength");
		if (pagelength != null && pagelength != "") {
		    config.iDisplayLength = parseInt(pagelength);
		}

	    // Look for column sorting
		var aaSorting = new Array();
		$(this).find("th").each(function (index) {
			var obj = $(this).attr("dt-sort");

			if (obj != null) {
				// Expecting "n,asc|desc"
				var s = obj.split(",");

				aaSorting[s[0]] = [index, s[1]];
			}
		});
		if (aaSorting.length > 0) {
			config.aaSorting = aaSorting;
		}

		// Look for column sorting plugins (e.g. currency)
		var aoColumns = new Array();
		$(this).find("th").each(function (index) {
			var obj = $(this).attr("dt-type");

			if (obj != null) {
				aoColumns[index] = { "sType": obj };
			}

		});
		if (aoColumns.length > 0) {
			config.aoColumns = aoColumns;
		}

		var id = $(this).attr("id");
		var selector = id == null || id == ""
            ? ".ui-table"
            : "#" + id;

		configDataTable(selector, config, 0);

	});

	function configDataTable(selector, config, count) {
	    if ($(selector).is(":visible") == false && count < 12) {
	        setTimeout(configDataTable, 100, selector, config, count+1);
	    } else {
            $(selector).dataTable(config);
	    }
	}

	$('a.popup-dialog').livequery("click", function () {

		var title = $(this).attr("title") == null
			? $(this).text()
			: $(this).attr("title");

		var href = $(this).attr("href");
		var height = $(this).attr("dialogHeight");
		var width = $(this).attr("dialogWidth");

		var div = $("<div>Loading data... please wait. <img alt='please wait' src='/Content/themes/base/images/spinner.gif' /></div>").load(href);
		div.dialog({
			modal: true,
			title: title,
			width: width,
			height: height,
			close: function () { div.remove(); }
		});

		return false;
	});

	$('a.dialog').livequery("click", function () {

	    var title = $(this).attr("title") == null
			? $(this).text()
			: $(this).attr("title");

	    var href = $(this).attr("href");
	    var height = $(this).attr("dialogHeight");
	    var width = $(this).attr("dialogWidth");

	    $(href).dialog({
	        modal: true,
	        title: title,
	        width: width,
	        height: height
	    });

	    return false;
	});


	$("[toggleall]").livequery("click", function () {
		var state1 = $(this).text();
		var state2 = $(this).attr("toggleall");
		var href = $(this).attr("href");
		$(this).text(state2);
		$(this).attr("toggleall", state1);

		$(href).click();

		return false;
	});

	$("[toggle]").live("click", function () {
		var state1 = $(this).text();
		var state2 = $(this).attr("toggle");
		var href = $(this).attr("href");

		$(href).toggle("fast");
		$(this).text(state2).attr("toggle", state1);

		return false;
	});

	$(".notification[autohide]").livequery(function () {
	    var delay = parseInt($(this).attr("autohide")) * 1000;
		$(this).slideDown().delay(delay).slideUp(); 
	});

	$("[watermark]").livequery(function () {
		$(this).watermark($(this).attr("watermark"));
	});

	$("[confirm]").livequery(function () {
		$(this).click(function () {
			return confirm($(this).attr("confirm"));
		});
	});

	$(".datepicker").livequery(function() {
		$(this).datepicker({
			dateFormat: 'mm/dd/yy'	
			});
	});

	$(".timepicker").livequery(function () {
	    $(this).timepicker({
	    });
	});

	$("img[refresh]").livequery(function() {

		var img = $(this);
		var interval = parseInt(img.attr("refresh"));
		setInterval(function() {
			var src = img.attr('src');

			// check for existing ? and remove if found 
			var pos = src.indexOf('?');
			if (pos != -1) { src = src.substring(0, pos); }
			
			img.attr('src', src + '?' + new Date().getTime());
		   
		}, interval); 
	});

	$("pre.php").livequery(function() {
		$(this).snippet("php", { style: "acid" });
	});
});




/*

DataTables Sorting Currency

*/

jQuery.fn.dataTableExt.oSort['currency-asc'] = function (a, b) {
	'use strict';
	var x, y;
	/* Remove any commas (assumes that if present all strings will have a fixed number of d.p) */
	x = (a === "-" || a === "--" || a === '' || a.toLowerCase().replace('/', '') === 'na')
		? -1
		: a.replace(/,/g, "");
	y = (b === "-" || b === "--" || b === '' || b.toLowerCase().replace('/', '') === 'na')
		? -1
		: b.replace(/,/g, "");

	/* Remove the currency sign */
	if (typeof x === "string" && isNaN(x.substr(0, 1), 10)) {
		x = x.substring(1);
	}
	if (typeof y === "string" && isNaN(y.substr(0, 1), 10)) {
		y = y.substring(1);
	}
	/* Parse and return */
	x = parseFloat(x, 10);
	y = parseFloat(y, 10); 
	
	return x - y;
}; 

jQuery.fn.dataTableExt.oSort['currency-desc'] = function(a,b) {
	'use strict';

	var x, y;

	/* Remove any commas (assumes that if present all strings will have a fixed number of d.p) */
	x = (a === "-" || a === "--" || a === '' || a.toLowerCase().replace('/', '') === 'na') ? -1 : a.replace(/,/g, "");
	y = (b === "-" || b === "--" || b === '' || b.toLowerCase().replace('/', '') === 'na') ? -1 : b.replace(/,/g, "");

	/* Remove the currency sign */
	if (typeof x === "string" && isNaN(x.substr(0, 1), 10)) {
		x = x.substring(1);
	}
	if (typeof y === "string" && isNaN(y.substr(0, 1), 10)) {
		y = y.substring(1);
	}

	/* Parse and return */
	x = parseFloat(x, 10);
	y = parseFloat(y, 10);

	return y - x;

};

$.postify = function (value) {
	var result = {};

	var buildResult = function (object, prefix) {
		for (var key in object) {

			var postKey = isFinite(key)
					? (prefix != "" ? prefix : "") + "[" + key + "]"
					: (prefix != "" ? prefix + "." : "") + key;

			switch (typeof (object[key])) {
				case "number": case "string": case "boolean":
					result[postKey] = object[key];
					break;

				case "object":
					if (object[key].toUTCString)
						result[postKey] = object[key].toUTCString().replace("UTC", "GMT");
					else {
						buildResult(object[key], postKey != "" ? postKey : key);
					}
			}
		}
	};

	buildResult(value, "");

	return result;
};

function ChangeSessionContext(url, val) {
    $(".tabs").removeData("cache.tabs");
    ClearDataTablesFromStorage();
	$.post(url
			, { id: val }
			, function (json) {
				if (json.Model) window.location.reload();
			}
			, "json");
}

function ChangeSiteSessionContext(url, val, siteName) {
	$(".tabs").removeData("cache.tabs");
	ClearDataTablesFromStorage();

	siteName = encodeURIComponent(siteName);
	
	$.post(url
			, { id: val, siteName: siteName }
			, function (json) {
				if (json.Model) window.location.reload();
			}
			, "json");
}

function ChangeDomainSessionContext(url, val, domainName) {
	$(".tabs").removeData("cache.tabs");
	ClearDataTablesFromStorage();

	domainName = encodeURIComponent(domainName);

	$.post(url
			, { id: val, domainName: domainName }
			, function (json) {
				if (json.Model) window.location.reload();
			}
			, "json");
}

function ChangeDateSelectorSessionContext(url, dateSelectorVal, startDate, endDate, callbackFunc) {
    $(".tabs").removeData("cache.tabs");
    
    var sDate = null;
    var eDate = null;

    if (dateSelectorVal == "Custom") {
        sDate = startDate;
        eDate = endDate;
    }

    $.post(url
			, { dateSelectorVal: dateSelectorVal, startDate: sDate, endDate: eDate }
			, function (json) {
			    if (json.Model) window.location.reload();
			}
			, "json");
}

function ClearDataTablesFromStorage() {
    for (var i in localStorage) {
        if (i.indexOf("DataTables") >= 0) {
            //Clear the search parameter
            var dataTable = JSON.parse(localStorage[i]);
            dataTable.search.search = "";
            localStorage[i] = JSON.stringify(dataTable);
        }
    }
}

