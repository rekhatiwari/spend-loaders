﻿
(function () {

    /* jshint ignore:start */
    try { /* Raygun */
        (function (n) { function e(n, e) { return Object.prototype.hasOwnProperty.call(n, e) } function t(n) { return n === undefined } var r = {}, o = n.TraceKit, a = [].slice, i = "?"; r.noConflict = function () { return n.TraceKit = o, r }, r.wrap = function (n) { function e() { try { return n.apply(this, arguments) } catch (e) { throw r.report(e), e } } return e }, r.report = function () { function t(n) { c.push(n) } function o(n) { for (var e = c.length - 1; e >= 0; --e) c[e] === n && c.splice(e, 1) } function i(n, t) { var o = null; if (!t || r.collectWindowErrors) { for (var i in c) if (e(c, i)) try { c[i].apply(null, [n].concat(a.call(arguments, 2))) } catch (u) { o = u } if (o) throw o } } function u(e) { var t = a.call(arguments, 1); if (s) { if (l === e) return; var o = s; s = null, l = null, i.apply(null, [o, null].concat(t)) } var u = r.computeStackTrace(e); throw s = u, l = e, n.setTimeout(function () { l === e && (s = null, l = null, i.apply(null, [u, null].concat(t))) }, u.incomplete ? 2e3 : 0), e } var c = [], l = null, s = null, f = n.onerror; return n.onerror = function (n, e, t, o, a) { var u = null; if (a) u = r.computeStackTrace(a); else if (s) r.computeStackTrace.augmentStackTraceWithInitialElement(s, e, t, n), u = s, s = null, l = null; else { var c = { url: e, line: t, column: o }; c.func = r.computeStackTrace.guessFunctionName(c.url, c.line), c.context = r.computeStackTrace.gatherContext(c.url, c.line), u = { mode: "onerror", message: n, url: document.location.href, stack: [c], useragent: navigator.userAgent } } return i(u, "from window.onerror"), f ? f.apply(this, arguments) : !1 }, u.subscribe = t, u.unsubscribe = o, u }(), r.computeStackTrace = function () { function o(e) { function t() { try { return new n.XMLHttpRequest } catch (e) { return new n.ActiveXObject("Microsoft.XMLHTTP") } } if (!r.remoteFetching) return ""; try { var o = t(); return o.open("GET", e, !1), o.send(""), o.responseText } catch (a) { return "" } } function a(n) { if (!e(k, n)) { var t = ""; n = n || "", -1 !== n.indexOf(document.domain) && (t = o(n)), k[n] = t ? t.split("\n") : [] } return k[n] } function u(n, e) { var r, o = /function ([^(]*)\(([^)]*)\)/, u = /['"]?([0-9A-Za-z$_]+)['"]?\s*[:=]\s*(function|eval|new Function)/, c = "", l = 10, s = a(n); if (!s.length) return i; for (var f = 0; l > f; ++f) if (c = s[e - f] + c, !t(c)) { if (r = u.exec(c)) return r[1]; if (r = o.exec(c)) return r[1] } return i } function c(n, e) { var o = a(n); if (!o.length) return null; var i = [], u = Math.floor(r.linesOfContext / 2), c = u + r.linesOfContext % 2, l = Math.max(0, e - u - 1), s = Math.min(o.length, e + c - 1); e -= 1; for (var f = l; s > f; ++f) t(o[f]) || i.push(o[f]); return i.length > 0 ? i : null } function l(n) { return n.replace(/[\-\[\]{}()*+?.,\\\^$|#]/g, "\\$&") } function s(n) { return l(n).replace("<", "(?:<|&lt;)").replace(">", "(?:>|&gt;)").replace("&", "(?:&|&amp;)").replace('"', '(?:"|&quot;)').replace(/\s+/g, "\\s+") } function f(n, e) { for (var t, r, o = 0, i = e.length; i > o; ++o) if ((t = a(e[o])).length && (t = t.join("\n"), r = n.exec(t))) return { url: e[o], line: t.substring(0, r.index).split("\n").length, column: r.index - t.lastIndexOf("\n", r.index) - 1 }; return null } function g(n, e, t) { var r, o = a(e), i = RegExp("\\b" + l(n) + "\\b"); return t -= 1, o && o.length > t && (r = i.exec(o[t])) ? r.index : null } function p(e) { for (var t, r, o, a, i = [n.location.href], u = document.getElementsByTagName("script"), c = "" + e, g = /^function(?:\s+([\w$]+))?\s*\(([\w\s,]*)\)\s*\{\s*(\S[\s\S]*\S)\s*\}\s*$/, p = /^function on([\w$]+)\s*\(event\)\s*\{\s*(\S[\s\S]*\S)\s*\}\s*$/, h = 0; u.length > h; ++h) { var m = u[h]; m.src && i.push(m.src) } if (o = g.exec(c)) { var d = o[1] ? "\\s+" + o[1] : "", v = o[2].split(",").join("\\s*,\\s*"); t = l(o[3]).replace(/;$/, ";?"), r = RegExp("function" + d + "\\s*\\(\\s*" + v + "\\s*\\)\\s*{\\s*" + t + "\\s*}") } else r = RegExp(l(c).replace(/\s+/g, "\\s+")); if (a = f(r, i)) return a; if (o = p.exec(c)) { var y = o[1]; if (t = s(o[2]), r = RegExp("on" + y + "=[\\'\"]\\s*" + t + "\\s*[\\'\"]", "i"), a = f(r, i[0])) return a; if (r = RegExp(t), a = f(r, i)) return a } return null } function h(n) { if (!n.stack) return null; for (var e, t, r = /^\s*at (?:((?:\[object object\])?\S+) )?\(?((?:file|http|https):.*?):(\d+)(?::(\d+))?\)?\s*$/i, o = /^\s*(\S*)(?:\((.*?)\))?@((?:file|http|https).*?):(\d+)(?::(\d+))?\s*$/i, a = /^\s*at (?:((?:\[object object\])?.+) )?\(?((?:ms-appx|http|https):.*?):(\d+)(?::(\d+))?\)?\s*$/i, l = n.stack.split("\n"), s = [], f = /^(.*) is undefined$/.exec(n.message), p = 0, h = l.length; h > p; ++p) { if (e = o.exec(l[p])) t = { url: e[3], func: e[1] || i, args: e[2] ? e[2].split(",") : "", line: +e[4], column: e[5] ? +e[5] : null }; else if (e = r.exec(l[p])) t = { url: e[2], func: e[1] || i, line: +e[3], column: e[4] ? +e[4] : null }; else { if (!(e = a.exec(l[p]))) continue; t = { url: e[2], func: e[1] || i, line: +e[3], column: e[4] ? +e[4] : null } } !t.func && t.line && (t.func = u(t.url, t.line)), t.line && (t.context = c(t.url, t.line)), s.push(t) } return s[0] && s[0].line && !s[0].column && f && (s[0].column = g(f[1], s[0].url, s[0].line)), s.length ? { mode: "stack", name: n.name, message: n.message, url: document.location.href, stack: s, useragent: navigator.userAgent } : null } function m(n) { for (var e, t = n.stacktrace, r = / line (\d+), column (\d+) in (?:<anonymous function: ([^>]+)>|([^\)]+))\((.*)\) in (.*):\s*$/i, o = t.split("\n"), a = [], i = 0, l = o.length; l > i; i += 2) if (e = r.exec(o[i])) { var s = { line: +e[1], column: +e[2], func: e[3] || e[4], args: e[5] ? e[5].split(",") : [], url: e[6] }; if (!s.func && s.line && (s.func = u(s.url, s.line)), s.line) try { s.context = c(s.url, s.line) } catch (f) { } s.context || (s.context = [o[i + 1]]), a.push(s) } return a.length ? { mode: "stacktrace", name: n.name, message: n.message, url: document.location.href, stack: a, useragent: navigator.userAgent } : null } function d(t) { var r = t.message.split("\n"); if (4 > r.length) return null; var o, i, l, g, p = /^\s*Line (\d+) of linked script ((?:file|http|https)\S+)(?:: in function (\S+))?\s*$/i, h = /^\s*Line (\d+) of inline#(\d+) script in ((?:file|http|https)\S+)(?:: in function (\S+))?\s*$/i, m = /^\s*Line (\d+) of function script\s*$/i, d = [], v = document.getElementsByTagName("script"), y = []; for (i in v) e(v, i) && !v[i].src && y.push(v[i]); for (i = 2, l = r.length; l > i; i += 2) { var x = null; if (o = p.exec(r[i])) x = { url: o[2], func: o[3], line: +o[1] }; else if (o = h.exec(r[i])) { x = { url: o[3], func: o[4] }; var w = +o[1], S = y[o[2] - 1]; if (S && (g = a(x.url))) { g = g.join("\n"); var k = g.indexOf(S.innerText); k >= 0 && (x.line = w + g.substring(0, k).split("\n").length) } } else if (o = m.exec(r[i])) { var b = n.location.href.replace(/#.*$/, ""), T = o[1], R = RegExp(s(r[i + 1])); g = f(R, [b]), x = { url: b, line: g ? g.line : T, func: "" } } if (x) { x.func || (x.func = u(x.url, x.line)); var O = c(x.url, x.line), C = O ? O[Math.floor(O.length / 2)] : null; x.context = O && C.replace(/^\s*/, "") === r[i + 1].replace(/^\s*/, "") ? O : [r[i + 1]], d.push(x) } } return d.length ? { mode: "multiline", name: t.name, message: r[0], url: document.location.href, stack: d, useragent: navigator.userAgent } : null } function v(n, e, t, r) { var o = { url: e, line: t }; if (o.url && o.line) { n.incomplete = !1, o.func || (o.func = u(o.url, o.line)), o.context || (o.context = c(o.url, o.line)); var a = / '([^']+)' /.exec(r); if (a && (o.column = g(a[1], o.url, o.line)), n.stack.length > 0 && n.stack[0].url === o.url) { if (n.stack[0].line === o.line) return !1; if (!n.stack[0].line && n.stack[0].func === o.func) return n.stack[0].line = o.line, n.stack[0].context = o.context, !1 } return n.stack.unshift(o), n.partial = !0, !0 } return n.incomplete = !0, !1 } function y(n, e) { for (var t, o, a, c = /function\s+([_$a-zA-Z\xA0-\uFFFF][_$a-zA-Z0-9\xA0-\uFFFF]*)?\s*\(/i, l = [], s = {}, f = !1, h = y.caller; h && !f; h = h.caller) if (h !== x && h !== r.report) { if (o = { url: null, func: i, line: null, column: null }, h.name ? o.func = h.name : (t = c.exec("" + h)) && (o.func = t[1]), a = p(h)) { o.url = a.url, o.line = a.line, o.func === i && (o.func = u(o.url, o.line)); var m = / '([^']+)' /.exec(n.message || n.description); m && (o.column = g(m[1], a.url, a.line)) } s["" + h] ? f = !0 : s["" + h] = !0, l.push(o) } e && l.splice(0, e); var d = { mode: "callers", name: n.name, message: n.message, url: document.location.href, stack: l, useragent: navigator.userAgent }; return v(d, n.sourceURL || n.fileName, n.line || n.lineNumber, n.message || n.description), d } function x(n, e) { var t = null; e = null == e ? 0 : +e; try { if (t = m(n)) return t } catch (r) { if (S) throw r } try { if (t = h(n)) return t } catch (r) { if (S) throw r } try { if (t = d(n)) return t } catch (r) { if (S) throw r } try { if (t = y(n, e + 1)) return t } catch (r) { if (S) throw r } return { mode: "failed" } } function w(n) { n = (null == n ? 0 : +n) + 1; try { throw Error() } catch (e) { return x(e, n + 1) } return null } var S = !1, k = {}; return x.augmentStackTraceWithInitialElement = v, x.guessFunctionName = u, x.gatherContext = c, x.ofCaller = w, x }(), function () { var e = function e(e) { var t = n[e]; n[e] = function () { var n = a.call(arguments), e = n[0]; return "function" == typeof e && (n[0] = r.wrap(e)), t.apply ? t.apply(this, n) : t(n[0], n[1]) } }; e("setTimeout"), e("setInterval") }(), r.remoteFetching || (r.remoteFetching = !0), r.collectWindowErrors || (r.collectWindowErrors = !0), (!r.linesOfContext || 1 > r.linesOfContext) && (r.linesOfContext = 11), n.TraceKit = r })(window), function (n, e) { "use strict"; if (n) { var t = n.event.add; n.event.add = function (r, o, a, i, u) { var c; return a.handler ? (c = a.handler, a.handler = e.wrap(a.handler)) : (c = a, a = e.wrap(a)), a.guid = c.guid ? c.guid : c.guid = n.guid++, t.call(this, r, o, a, i, u) }; var r = n.fn.ready; n.fn.ready = function (n) { return r.call(this, e.wrap(n)) }; var o = n.ajax; n.ajax = function (t, r) { "object" == typeof t && (r = t, t = void 0), r = r || {}; for (var a, i = ["complete", "error", "success"]; a = i.pop() ;) n.isFunction(r[a]) && (r[a] = e.wrap(r[a])); try { return t ? o.call(this, t, r) : o.call(this, r) } catch (u) { throw e.report(u), u } } } }(window.jQuery, window.TraceKit), function (n, e, t) { function r(n) { var e = n, t = n.split("//")[1]; if (t) { var r = t.indexOf("?"), o = ("" + t).substring(0, r), a = o.split("/").slice(0, 4).join("/"), i = o.substring(0, 48); e = a.length < i.length ? a : i, e !== o && (e += "..") } return e } function o(n, e, o, a) { var i = "AJAX Error: " + (e.statusText || "unknown") + " " + (o.type || "unknown") + " " + (r(o.url) || "unknown"); (!j || e.getAllResponseHeaders()) && M.send(a || n.type, { status: e.status, statusText: e.statusText, type: o.type, url: o.url, ajaxErrorMessage: i, contentType: o.contentType, data: o.data ? o.data.slice(0, 10240) : t }) } function a(e, t) { n.console && n.console.log && C && (n.console.log(e), t && n.console.log(t)) } function i() { return w && "" !== w ? !0 : (a("Raygun API key has not been configured, make sure you call Raygun.init(yourApiKey)"), !1) } function u(n, e) { var t, r = {}; for (t in n) r[t] = n[t]; for (t in e) r[t] = e[t]; return r } function c(n, e) { return null != e ? n.concat(e) : n } function l(n, e) { for (var t = 0; n.length > t; t++) e.call(null, t, n[t]) } function s(n) { for (var e in n) if (n.hasOwnProperty(e)) return !1; return !0 } function f() { return Math.floor(9007199254740992 * Math.random()) } function g() { var e = document.documentElement, t = document.getElementsByTagName("body")[0], r = n.innerWidth || e.clientWidth || t.clientWidth, o = n.innerHeight || e.clientHeight || t.clientHeight; return { width: r, height: o } } function p(n) { var e = (new Date).toJSON(); try { var r = "raygunjs=" + e + "=" + f(); localStorage[r] === t && (localStorage[r] = n) } catch (o) { a("Raygun4JS: LocalStorage full, cannot save exception") } } function h() { try { return "localStorage" in n && null !== n.localStorage } catch (e) { return !1 } } function m() { if (h() && localStorage.length > 0) for (var n in localStorage) "raygunjs=" === n.substring(0, 9) && (v(JSON.parse(localStorage[n])), localStorage.removeItem(n)) } function d(e, r) { var o = [], i = {}; e.stack && e.stack.length && l(e.stack, function (n, e) { o.push({ LineNumber: e.line, ColumnNumber: e.column, ClassName: "line " + e.line + ", column " + e.column, FileName: e.url, MethodName: e.func || "[anonymous]" }) }), n.location.search && n.location.search.length > 1 && l(n.location.search.substring(1).split("&"), function (n, e) { var t = e.split("="); if (t && 2 === t.length) { var r = decodeURIComponent(t[0]), o = t[1]; if (b) if (Array.prototype.indexOf && b.indexOf === Array.prototype.indexOf) -1 === b.indexOf(r) && (i[r] = o); else for (n = 0; b.length > n; n++) b[n] === r && (i[r] = o); else i[r] = o } }), r === t && (r = {}), s(r.customData) && (r.customData = "function" == typeof N ? N() : N), s(r.tags) && (r.tags = D); var u = n.screen || { width: g().width, height: g().height, colorDepth: 8 }, c = r.customData && r.customData.ajaxErrorMessage, f = r.customData; try { JSON.stringify(f) } catch (p) { var h = "Cannot add custom data; may contain circular reference"; f = { error: h }, a("Raygun4JS: " + h) } var m = { OccurredOn: new Date, Details: { Error: { ClassName: e.name, Message: c || e.message || r.status || "Script error", StackTrace: o }, Environment: { UtcOffset: (new Date).getTimezoneOffset() / -60, "User-Language": navigator.userLanguage, "Document-Mode": document.documentMode, "Browser-Width": g().width, "Browser-Height": g().height, "Screen-Width": u.width, "Screen-Height": u.height, "Color-Depth": u.colorDepth, Browser: navigator.appCodeName, "Browser-Name": navigator.appName, "Browser-Version": navigator.appVersion, Platform: navigator.platform }, Client: { Name: "raygun-js", Version: "1.9.2" }, UserCustomData: f, Tags: r.tags, Request: { Url: document.location.href, QueryString: i, Headers: { "User-Agent": navigator.userAgent, Referer: document.referrer, Host: document.domain } }, Version: k || "Not supplied" } }; S && (m.Details.User = S), v(m) } function v(n) { if (i()) { a("Sending exception data to Raygun:", n); var e = $ + "/entries?apikey=" + encodeURIComponent(w); x(e, JSON.stringify(n)) } } function y(e, t) { var r; return r = new n.XMLHttpRequest, "withCredentials" in r ? r.open(e, t, !0) : n.XDomainRequest && (E && (t = t.slice(6)), r = new n.XDomainRequest, r.open(e, t)), r.timeout = 1e4, r } function x(e, r) { var o = y("POST", e, r); return "withCredentials" in o ? (o.onreadystatechange = function () { 4 === o.readyState && (202 === o.status ? m() : A && 403 !== o.status && 400 !== o.status && p(r)) }, o.onload = function () { a("logged error to Raygun") }) : n.XDomainRequest && (o.ontimeout = function () { A && (a("Raygun: saved error locally"), p(r)) }, o.onload = function () { a("logged error to Raygun"), m() }), o.onerror = function () { a("failed to log error to Raygun") }, o ? (o.send(r), t) : (a("CORS not supported"), t) } var w, S, k, b, T, R = TraceKit.noConflict(), O = n.Raygun, C = !1, E = !1, j = !1, A = !1, N = {}, D = [], $ = "https://api.raygun.io"; e && (T = e(document)); var M = { noConflict: function () { return n.Raygun = O, M }, init: function (n, e, t) { return w = n, R.remoteFetching = !1, N = t, e && (E = e.allowInsecureSubmissions || !1, j = e.ignoreAjaxAbort || !1, e.debugMode && (C = e.debugMode)), m(), M }, withCustomData: function (n) { return N = n, M }, withTags: function (n) { D = n }, attach: function () { return i() ? (R.report.subscribe(d), T && T.ajaxError(o), M) : t }, detach: function () { return R.report.unsubscribe(d), T && T.unbind("ajaxError", o), M }, send: function (n, e, t) { try { d(R.computeStackTrace(n), { customData: "function" == typeof N ? u(N(), e) : u(N, e), tags: c(D, t) }) } catch (r) { if (n !== r) throw r } return M }, setUser: function (n) { return S = { Identifier: n }, M }, setVersion: function (n) { return k = n, M }, saveIfOffline: function (n) { return n !== t && "boolean" == typeof n && (A = n), M }, filterSensitiveData: function (n) { return b = n, M } }; n.Raygun = M }(window, window.jQuery);
        Raygun.init('UzB+rWYMJHZ5CB+MVvtYBA==');
    }
    catch (e) { }
    /* jshint ignore:end */

    var getCORS = function () {
        if (window.XDomainRequest)
            return new window.XDomainRequest();
        if (window.XMLHttpRequest)
            return new window.XMLHttpRequest();
        if (window.ActiveXObject) {
            var activexmodes = ["Msxml2.XMLHTTP", "Microsoft.XMLHTTP"];
            for (var i = 0; i < activexmodes.length; i++) {
                try { return new ActiveXObject(activexmodes[i]); }
                catch (e) { }
            }
        }
        return null;
    };
    var postAsync = function (url, data) {
        var h = getCORS();
        h.open("POST", url, true);
        h.send(data);
    };
    var sendBeacon = function (params) {
        var url = ('https:' == document.location.protocol ? 'https://' : 'http://') + "b.awesomeom.com/jsi";
        var data = {};
        data.params = params;
        data.url = document.URL;
        data.referrer = document.referrer;
        if (navigator) {
            try {
                data.useragent = navigator.userAgent;
                data.platform = navigator.platform;
            } catch (e) { }
        }
        var j = JSON.stringify(data);
        postAsync(url, j);
    };

    var str_trim = function (s) {
        return s.replace(/^\s+|\s+$/g, '');
    };

    var pttJsi = function () {
        this.addevent();
    };

    function parse_bool(val) {
        var value = String(val).toLowerCase();
        return (value === '1' || value === 'true');
    };


    pttJsi.prototype.iframeWidth = 800;
    pttJsi.prototype.iframeHeight = 100;
    pttJsi.prototype.frameborder = 1;
    pttJsi.prototype.iframes = [];
    pttJsi.prototype.divs = [];
    pttJsi.prototype.userParams = null;
    pttJsi.prototype.iframe_idx = 0;
    pttJsi.prototype.query_string = function () {
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [query_string[pair[0]], pair[1]];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        }
        return query_string;
    }();

    pttJsi.prototype.is_small_screen = function () {
        if (window.self.screen.availWidth <= 800) {
            return true;
        } else {
            return false;
        }
    }();

    //event handler, parse messages.
    pttJsi.prototype.onmessage = function (e) {
        var d = e.data;
        var origin = e.origin;
        if (d.indexOf('origin=openmail') == -1) return;
        var arDim = d.split(';');
        if (arDim[0].split('=')[0] == 'callback') {
            var cbf = arDim[0].split('=')[1];
            if (typeof window[cbf] === 'function') {
                window[cbf]();
            }
        } else {
            var arW = arDim[0].split('=');
            var arH = arDim[1].split('=');
            var arID = arDim[2].split('=');
            var o = document.getElementById(arID[1]);
            if (o) {
                o.style.visibility = 'visible';
                o.style.height = o.height = arH[1] + "px";
            }
        }
    };

    pttJsi.prototype.addevent = function () {
        if (typeof window.addEventListener != 'undefined') {
            window.addEventListener('message', this.onmessage, false);
        } else if (typeof window.attachEvent != 'undefined') {
            window.attachEvent('onmessage', this.onmessage);
        }
    };

    pttJsi.prototype.loadIframe = function (arr) {

        var argu_i = arr;
        var i = arr.ifr_id;

        argu_i.SCTU = argu_i.SCTU ? argu_i.SCTU : argu_i.server_click_tracking_url;
        argu_i.CTU = argu_i.CTU ? argu_i.CTU : argu_i.click_tracking_url;
        argu_i.STU = argu_i.STU ? argu_i.STU : argu_i.search_tracking_url;
        argu_i.SBU = argu_i.SBU ? argu_i.SBU : argu_i.serp_base_url;
        argu_i.KW = argu_i.KW ? argu_i.KW : (argu_i.keyword ? argu_i.keyword : (this.query_string.jsikw ? this.query_string.jsikw : '')); //last resort : get it from query string in url
        ////
        this.divs[i] = argu_i.id;
        this.iframes[i] = document.createElement('iframe');
        this.iframes[i].id = 'ifr_' + argu_i.id;
        this.iframes[i].width = this.iframes[i].style.width = (!argu_i.width || this.is_small_screen) ? '100%' : argu_i.width; //argu_i.width?argu_i.width:'100%';
        this.iframes[i].frameBorder = 0;
        this.iframes[i].height = '0px';
        this.iframes[i].style.visibility = 'hidden';
        this.iframes[i].style.borderWidth = argu_i.border_width ? parseInt(argu_i.border_width) + 'px' : '0px';
        this.iframes[i].style.borderStyle = argu_i.border_style ? argu_i.border_style : 'solid';
        this.iframes[i].style.borderColor = argu_i.border_color ? argu_i.border_color : 'gray';
        this.iframes[i].scrolling = 'no';
        this.iframes[i].marginWidth = '0';
        //////////////////////////////////////////
        if (argu_i.request) {
            this.iframes[i].src = argu_i.request + '&ifr_id=' + this.iframes[i].id;
        } else {

            var PS = argu_i.PS ? argu_i.PS : '';
            var PX = argu_i.PX ? argu_i.PX : '';
            var S = argu_i.S ? argu_i.S : 'JSI';
            var L = argu_i.L ? argu_i.L : 'JSI';
            var V = argu_i.V ? argu_i.V : '';
            var KW = argu_i.KW ? argu_i.KW : '';
            var KK = argu_i.KK ? argu_i.KK : (this.query_string.KK ? this.query_string.KK : '');

            var LS = '';
            if (this.query_string.jsils) {
                LS = this.query_string.jsils;
            } else {
                LS = argu_i.linkset_id ? 'linkset:' + argu_i.linkset_id + ';' : '';
                LS += argu_i.sub_id ? 'sub:' + argu_i.sub_id + ';' : '';
                LS += argu_i.method_id ? 'method:' + argu_i.method_id + ';' : '';
                LS += argu_i.test_id ? 'test:' + argu_i.test_id + ';' : '';
                LS = argu_i.LS ? argu_i.LS : LS;//combined LS param will override individuals above, NOT DOCUMENTED
            }

            if (this.query_string.jsips) {
                PS = this.query_string.jsips;
                PX = '';
            }
            var https_enabled = argu_i.https_enabled ? argu_i.https_enabled : '';
            var KWRT = argu_i.KWRT ? argu_i.KWRT : '';
            var RP = argu_i.RP ? encodeURIComponent(argu_i.RP) : '';
            var IP = argu_i.IP ? argu_i.IP : '';
            var UA = argu_i.UA ? encodeURIComponent(argu_i.UA) : '';
            var TT = argu_i.TT ? TT = argu_i.TT : '';
            var TTT = argu_i.TTT ? argu_i.TTT : '';
            var RF = argu_i.RF ? encodeURIComponent(argu_i.RF) : '';
            var id = argu_i.id ? argu_i.id : '';
            var W = '';//argu_i.width?argu_i.width:'';
            var type = argu_i.type ? argu_i.type : 'text_ads';

            if (parse_bool(https_enabled)) {
                var domain = argu_i.domain ? argu_i.domain : 's.jsidomain.com';
                var protocol = 'https://'
            } else {
                var domain = argu_i.domain ? argu_i.domain : 'templatejsi.com';
                var protocol = 'http://'
            }


            var IC = '', TYP = '';
            if (type == 'text_ads' || type == 'combo' || !argu_i.SBU) IC = argu_i.n_ads ? '(Ads:' + argu_i.n_ads + ')' : '';
            if (type == 'keywords' || type == 'combo' || !argu_i.SBU) IC += argu_i.n_keywords ? '(Keywords:' + argu_i.n_keywords + ')' : '';
            if (type == 'search_box' || type == 'combo' || !argu_i.SBU) IC += argu_i.search_box ? '(SearchBox)' : '';
            IC = argu_i.IC ? argu_i.IC : IC; //combined IC param will override individuals above, NOT DOCUMENTED
            TYP = type ? type : 'text_ads';

            var ads_style = argu_i.ads_style ? argu_i.ads_style : '';
            ads_style = encodeURIComponent(str_trim(ads_style).replace(/\t/g, ' ').replace(/\n/g, ' ').replace(/\r/g, ' ').replace(/ *;/g, ';').replace(/; */g, ';').replace(/ *=/g, '=').replace(/= */g, '='));

            var keywords_style = argu_i.keywords_style ? argu_i.keywords_style : '';
            keywords_style = encodeURIComponent(str_trim(keywords_style).replace(/\t/g, ' ').replace(/\n/g, ' ').replace(/\r/g, ' ').replace(/ *;/g, ';').replace(/; */g, ';').replace(/ *=/g, '=').replace(/= */g, '='));

            var search_box_style = argu_i.search_box_style ? argu_i.search_box_style : '';
            search_box_style = encodeURIComponent(str_trim(search_box_style).replace(/\t/g, ' ').replace(/\n/g, ' ').replace(/\r/g, ' ').replace(/ *;/g, ';').replace(/; */g, ';').replace(/ *=/g, '=').replace(/= */g, '='));

            var STU = argu_i.STU ? encodeURIComponent(argu_i.STU) : '';
            var SBU = argu_i.SBU ? encodeURIComponent(argu_i.SBU) : '';
            var SOS = argu_i.serp_on_same || '';
            var SLN = argu_i.sitelinks || '';
            var CCBF = argu_i.click_callback || '';
            var DBAD = argu_i.double_ads || '';
            var SUBA = argu_i.subaff || '';
            var CTU = '';
            if (typeof argu_i.CTU == 'object') {
                for (var key in argu_i.CTU) {
                    CTU += (CTU ? ' AND ' : '') + argu_i.CTU[key];
                }
                CTU = encodeURIComponent(CTU);
            } else {
                CTU = argu_i.CTU ? encodeURIComponent(argu_i.CTU) : '';
            }

            var SCTU = '';
            if (typeof argu_i.SCTU == 'object') {
                for (var k in argu_i.SCTU) {
                    SCTU += (SCTU ? ' AND ' : '') + argu_i.SCTU[k];
                }
                SCTU = encodeURIComponent(SCTU);
            } else {
                SCTU = argu_i.SCTU ? encodeURIComponent(argu_i.SCTU) : '';
            }
            this.iframes[i].src = protocol + domain + '/d2s/search?' + 'PX=' + PX + '&PS=' + PS + '&RP=' + RP + '&IP=' + IP + '&UA=' + UA + '&KW=' + KW + '&KK=' + KK + '&LS=' + LS + '&KWRT=' + KWRT + '&TT=' + TT + '&TTT=' + TTT + '&RF=' + RF + '&IC=' + IC + '&S=' + S + '&L=' + L + '&V=' + V + '&W=' + W + '&TYP=' + TYP + '&ads_style=' + ads_style + '&keywords_style=' + keywords_style + '&search_box_style=' + search_box_style + '&SBU=' + SBU + '&SOS=' + SOS + '&DBAD=' + DBAD + '&SUBA=' + SUBA + '&SLN=' + SLN + '&SCTU=' + SCTU + '&CTU=' + CTU + '&STU=' + STU + '&CCBF=' + CCBF + '&ifr_id=' + this.iframes[i].id;

        }
        //////////////////////////////////////////
        var d = document.getElementById(this.divs[i]);
        if (d) {
            d.appendChild(this.iframes[i]);
        } else {
            throw ("JSI Error: element '" + this.divs[i] + "' not found.");
        }
    };

    pttJsi.prototype.load = function () {
        var readyStateCheckInterval;
        if (arguments) openmail.jsi.userParams = arguments;
        /////beacon
        if (openmail.jsi.userParams) {
            try {
                sendBeacon(openmail.jsi.userParams);
            } catch (e) {
                try { Raygun.send(e); } catch (ee) { }
            }
        }

        var thefunc = function () {
            if (document.readyState == "complete" || document.readyState == "loaded") {
                clearInterval(readyStateCheckInterval);
                for (var i = 0; i < openmail.jsi.userParams.length; i++) {
                    openmail.jsi.userParams[i].ifr_id = i;
                    try {
                        openmail.jsi.loadIframe.apply(openmail.jsi, [openmail.jsi.userParams[openmail.jsi.iframe_idx++]]);
                    } catch (e) {
                        try { Raygun.send(e); } catch (ee) { }
                    }
                }
                openmail.jsi.iframe_idx = 0; // after loading all iframes, reset this index in case use may reload other iframes on the same page.
            }
        };
        readyStateCheckInterval = setInterval(thefunc, 10);

    };


    ///////////////////////////////////////////
    var pttOpenmailWrapper = function () {
        this.jsi = new pttJsi();
    };

    window.openmail = new pttOpenmailWrapper();

})();