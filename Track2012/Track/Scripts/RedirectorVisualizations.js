﻿/*
    Visulization Refactor

*/

//https://github.com/highcharts/highcharts/issues/705
Highcharts.Axis.prototype.hasData = function () {
    return this.hasVisibleSeries;
};


var visualizationClickData = {};
var cachedGraphData = [];
var bindingAxis = {};


$(".display-redirector-rule-visualizations-graph").live("click", function (event) {

    
    event.preventDefault();

    visualizationClickData.request = {};
    visualizationClickData.request.url = RedirectorVisualizationsGraphsUrl;
    visualizationClickData.request.jsonParams = {};
    visualizationClickData.request.jsonParams.id = $(this).data('redirectid');
    visualizationClickData.request.jsonParams.campaign = $(this).data('campaignname');

    visualizationClickData.cacheKey = 'redirectId_' + $(this).data('redirectid') + "_" + $(this).data('campaignname');
    visualizationClickData.graphTitle = $(this).data('campaignname') + " - " + $(this).data('adgroup');
    if ($(this).data('redirectid') > 0) {
        visualizationClickData.graphTitle += " (" + $(this).data('redirectid') + ")";
    }

    resetBind();

    $("#dialog-keyword-visualizations-graph").dialog('open');

    return false;
});

$(".display-redirector-rule-graph").live("click", function (event) {
    event.preventDefault();
    $("#dialog-keyword-graph img").attr('src', "");
    $("#dialog-keyword-graph img").attr('src', $(this).attr('href'));
    $("#dialog-keyword-graph").dialog('open');
});


function attachVisualizationListener() {

    $('.display-keyword-visualizations-graph').unbind();
    $('.display-keyword-visualizations-graph').on('click', function (event) {

        event.preventDefault();

        visualizationClickData.request = {};
        visualizationClickData.request.url = RedirectorKeywordVisualizationsGraphsUrl;
        visualizationClickData.request.jsonParams = {};

        var funnelTerm = $(this).data('funnelterm');
        if (funnelTerm == null)
            funnelTerm = $(this).data('relatedterm');


        visualizationClickData.request.jsonParams.query = funnelTerm;
        visualizationClickData.request.jsonParams.redirectId = $(this).data('redirectid');

        if ($(this).data('keyword') != null)
            visualizationClickData.request.jsonParams.keyword = $(this).data('keyword');


        if ($(this).data('relatedterm') != null) {
            visualizationClickData.graphTitle = $(this).data('relatedterm') + '(' + $(this).data('redirectid') + ')';
        } else {
            visualizationClickData.graphTitle = $(this).data('keyword') + ' - ' + funnelTerm + ' (' + $(this).data('redirectid') + ')';
        }

        visualizationClickData.cacheKey = 'keyword_' + $(this).data('keywordid');

        resetBind();

        $("#dialog-keyword-visualizations-graph").dialog('open');


        return false;

    });
}

$("#dialog-keyword-visualizations-graph").dialog({
    title: 'Visualization Trends',
    autoOpen: false,
    width: 1600,
    weight: 20,
    resizable: false,
    modal: true,
    draggable: true,
    open: function () {
        loadVisualizationsGraph();
    }
});

$("#loadingVisualizationsGraphDialog").dialog({
    autoOpen: false,
    height: 180,
    width: 400,
    weight: 50,
    resizable: false,
    modal: true,
    draggable: false,
    open: function (event, ui) { $(".ui-dialog-titlebar-close", $(this).parent()).hide(); }
});

$("#dialog-keyword-graph").dialog({
    autoOpen: false,
    height: 300,
    width: 460,
    weight: 50,
    resizable: false,
    modal: true,
    draggable: false,
});




function loadVisualizationsGraph() {
    // show loading dialog until graphs are ready
    $('#loadingVisualizationsGraphDialog').removeAttr('style');
    $('#loadingVisualizationsGraphDialog').dialog('open');

    var currentState = getVisulizationState();

    var params = visualizationClickData.request.jsonParams;

    params.dateSelectorVal = currentState.dateSelector;
    params.startDate = currentState.startDate;
    params.endDate = currentState.endDate;
    params.hourlyDropDown = currentState.HourlyDropDown;

    $.post(visualizationClickData.request.url, params,
    function (data) {


        // must first initialize arrays
        cachedGraphData[visualizationClickData.cacheKey] = {};
        cachedGraphData[visualizationClickData.cacheKey].Sessions = [];
        cachedGraphData[visualizationClickData.cacheKey].Clicks = [];
        cachedGraphData[visualizationClickData.cacheKey].PaidClicks = [];
        cachedGraphData[visualizationClickData.cacheKey].CTR = [];
        cachedGraphData[visualizationClickData.cacheKey].RPC = [];
        cachedGraphData[visualizationClickData.cacheKey].Spend = [];
        cachedGraphData[visualizationClickData.cacheKey].Revenue = [];
        cachedGraphData[visualizationClickData.cacheKey].GP = [];
        cachedGraphData[visualizationClickData.cacheKey].ROAS = [];
        cachedGraphData[visualizationClickData.cacheKey].RPI = [];
        cachedGraphData[visualizationClickData.cacheKey].CPC = [];
        cachedGraphData[visualizationClickData.cacheKey].TQ = [];
        cachedGraphData[visualizationClickData.cacheKey].SourceTQ = [];
        cachedGraphData[visualizationClickData.cacheKey].Bid = [];
        cachedGraphData[visualizationClickData.cacheKey].Budget = [];

        for (var i = 0; i < data.length; i++) {
            var itemDate = new Date(data[i].Date);

            var sessionsItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].Sessions];
            cachedGraphData[visualizationClickData.cacheKey].Sessions.push(sessionsItem);

            var clicksItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].Clicks];
            cachedGraphData[visualizationClickData.cacheKey].Clicks.push(clicksItem);

            var paidClicksItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].PaidClicks];
            cachedGraphData[visualizationClickData.cacheKey].PaidClicks.push(paidClicksItem);

            var ctrItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].CTR * 100];
            cachedGraphData[visualizationClickData.cacheKey].CTR.push(ctrItem);

            var rpcItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].RPC];
            cachedGraphData[visualizationClickData.cacheKey].RPC.push(rpcItem);

            var spendItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].Spend];
            cachedGraphData[visualizationClickData.cacheKey].Spend.push(spendItem);

            var revenueItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].Revenue];
            cachedGraphData[visualizationClickData.cacheKey].Revenue.push(revenueItem);

            var gpItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].GP];
            cachedGraphData[visualizationClickData.cacheKey].GP.push(gpItem);

            var roasItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].ROAS * 100];
            cachedGraphData[visualizationClickData.cacheKey].ROAS.push(roasItem);

            var rpiItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].RPI];
            cachedGraphData[visualizationClickData.cacheKey].RPI.push(rpiItem);

            var cpcItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].CPC];
            cachedGraphData[visualizationClickData.cacheKey].CPC.push(cpcItem);

            var typeTqItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].TypeTQ];
            cachedGraphData[visualizationClickData.cacheKey].TQ.push(typeTqItem);

            var sourceTqItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].SourceTQ];
            cachedGraphData[visualizationClickData.cacheKey].SourceTQ.push(sourceTqItem);

            var BidItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].Bid];
            cachedGraphData[visualizationClickData.cacheKey].Bid.push(BidItem);

            var budgetItem = [Date.UTC(itemDate.getUTCFullYear(), itemDate.getUTCMonth(), itemDate.getUTCDate(), itemDate.getUTCHours()), data[i].Budget];
            cachedGraphData[visualizationClickData.cacheKey].Budget.push(budgetItem);
        }

        drawGenericChart();
    }, "json");
}



function drawGenericChart() {

    var currentState = getVisulizationState();

    if (!$('.highcharts-target').is(':empty')) {
        $('.highcharts-target').highcharts().destroy();
    }
    var title = visualizationClickData.graphTitle;
    var data = cachedGraphData[visualizationClickData.cacheKey];

    $('.highcharts-target').highcharts({
        chart: {
            type: 'spline'
        },
        plotOptions: {
            series: {
                events: {
                    show: saveVisulizationState,
                    hide: saveVisulizationState,
                }
            }
        },
        title: {
            text: title
        },
        yAxis: [{
            id: 'sessions-axis',
            labels: {
                format: '{value}',
                style: {
                    color: '#0000FF'
                }
            },
            title: {
                text: 'Sessions',
                style: {
                    color: '#0000FF'
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        }, {
            id: 'clicks-axis',
            labels: {
                format: '{value}',
                style: {
                    color: '#4AA02C'
                }
            },
            title: {
                text: 'Clicks',
                style: {
                    color: '#4AA02C'
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        }, {
            id: 'paid-clicks-axis',
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[9],
                }
                
            },
            title: {
                text: 'Paid Clicks',
                style: {
                    color: Highcharts.getOptions().colors[9],
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'ctr-axis',
            labels: {
                format: '{value} %',
                style: {
                    color: '#F6358A'
                }
            },
            title: {
                text: 'CTR',
                style: {
                    color: '#F6358A'
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'cpc-axis',
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            title: {
                text: 'CPC',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'rpc-axis',
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'RPC',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'rpi-axis',
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: 'RPI',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'spend-axis',
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[3]
                }
            },
            title: {
                text: 'Spend',
                style: {
                    color: Highcharts.getOptions().colors[3]
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'revenue-axis',
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[4]
                }
            },
            title: {
                text: 'Revenue',
                style: {
                    color: Highcharts.getOptions().colors[4]
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'gp-axis',
            labels: {
                format: '${value}',
                style: {
                    color: Highcharts.getOptions().colors[5]
                }
            },
            title: {
                text: 'GP',
                style: {
                    color: Highcharts.getOptions().colors[5]
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'roas-axis',
            labels: {
                format: '{value} %',
                style: {
                    color: Highcharts.getOptions().colors[6]
                }
            },
            title: {
                text: 'ROAS',
                style: {
                    color: Highcharts.getOptions().colors[6]
                }
            },
            max: bindingAxis.YMax,
            min: bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'tq-axis',
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[7]
                }
            },
            title: {
                text: 'TQ',
                style: {
                    color: Highcharts.getOptions().colors[7]
                }
            },
            max: bindingAxis.YMax == null ? 10 : bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
        {
            id: 'source-tq-axis',
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[8]
                }
            },
            title: {
                text: 'Source TQ',
                style: {
                    color: Highcharts.getOptions().colors[8]
                }
            },
            max: bindingAxis.YMax == null ? 10 : bindingAxis.YMax,
            min: bindingAxis.YMin == null ? 0 : bindingAxis.YMin,
            showEmpty: false,
        },
         {
             id: 'bid-axis',
             labels: {
                 format: '${value}',
                 style: {
                     color: Highcharts.getOptions().colors[9]
                 }
             },
             title: {
                 text: 'Bid',
                 style: {
                     color: Highcharts.getOptions().colors[9]
                 }
             },
             max: bindingAxis.YMax,
             min: bindingAxis.YMin,
             showEmpty: false,
         },
         {
             id: 'budget-axis',
             labels: {
                 format: '${value}',
                 style: {
                     color: Highcharts.getOptions().colors[10]
                 }
             },
             title: {
                 text: 'Budget',
                 style: {
                     color: Highcharts.getOptions().colors[10]
                 }
             },
             max: bindingAxis.YMax,
             min: bindingAxis.YMin,
             showEmpty: false,
         }],
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
                month: '%b %e',
                year: '%b'
            },
            title: {
                text: 'Date'
            }
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%b %e %H:%M}: {point.y:.2f}'
        },
        exporting: {
            sourceHeight: 500,
            sourceWidth: 1500,
            scale: 1
        },
        series: [{
            id: 'sessions-series',
            name: 'Sessions',
            data: data.Sessions,
            yAxis: 'sessions-axis',
            color: '#0000FF',
            visible: currentState.graph.Sessions,
        }, {
            id: 'clicks-series',
            name: 'Clicks',
            data: data.Clicks,
            yAxis: 'clicks-axis',
            color: '#4AA02C',
            visible: currentState.graph.Clicks,
        }, {
            id: 'paid-clicks-series',
            name: 'Paid Clicks',
            data: data.PaidClicks,
            yAxis: 'paid-clicks-axis',
            color: Highcharts.getOptions().colors[9],
            visible: currentState.graph.PaidClicks,
        },
        {
            id: 'ctr-series',
            name: 'CTR',
            data: data.CTR,
            yAxis: 'ctr-axis',
            color: '#F6358A',
            visible: currentState.graph.CTR,
        },
        {
            id: 'cpc-series',
            name: 'CPC',
            data: data.CPC,
            yAxis: 'cpc-axis',
            color: Highcharts.getOptions().colors[0],
            visible: currentState.graph.CPC,
        },
        {
            id: 'rpc-series',
            name: 'RPC',
            data: data.RPC,
            yAxis: 'rpc-axis',
            color: Highcharts.getOptions().colors[1],
            visible: currentState.graph.RPC,
        },
        {
            id: 'rpi-series',
            name: 'RPI',
            data: data.RPI,
            yAxis: 'rpi-axis',
            color: Highcharts.getOptions().colors[2],
            visible: currentState.graph.RPI
        },
        {
            id: 'spend-series',
            name: 'Spend',
            data: data.Spend,
            yAxis: 'spend-axis',
            color: Highcharts.getOptions().colors[3],
            visible: currentState.graph.Spend
        },
        {
            id: 'revenue-series',
            name: 'Revenue',
            data: data.Revenue,
            yAxis: 'revenue-axis',
            color: Highcharts.getOptions().colors[4],
            visible: currentState.graph.Revenue
        },
        {
            id: 'gp-series',
            name: 'GP',
            data: data.GP,
            yAxis: 'gp-axis',
            color: Highcharts.getOptions().colors[5],
            visible: currentState.graph.GP
        },
        {
            id: 'roas-series',
            name: 'ROAS',
            data: data.ROAS,
            yAxis: 'roas-axis',
            color: Highcharts.getOptions().colors[6],
            visible: currentState.graph.ROAS
        },
        {
            id: 'tq-series',
            name: 'TQ',
            data: data.TQ,
            yAxis: 'tq-axis',
            color: Highcharts.getOptions().colors[7],
            visible: currentState.graph.TQ
        },
        {
            id: 'source-tq-series',
            name: 'Source TQ',
            data: data.SourceTQ,
            yAxis: 'source-tq-axis',
            color: Highcharts.getOptions().colors[8],
            visible: currentState.graph.SourceTQ
        },
        {
            id: 'bid-series',
            name: 'Bid',
            data: data.Bid,
            yAxis: 'bid-axis',
            color: Highcharts.getOptions().colors[9],
            visible: currentState.graph.Bid
        },
        {
            id: 'budget-series',
            name: 'Budget',
            data: data.Budget,
            yAxis: 'budget-axis',
            color: Highcharts.getOptions().colors[10],
            visible: currentState.graph.Budget
        }
        ]
    });
    chart = $('.highcharts-target').highcharts();

    // can now remove loading dialog
    $('#loadingVisualizationsGraphDialog').dialog('close');

}


$(".endDateKeywordVisualizations, .startDateKeywordVisualizations").change(function () {
    saveVisulizationState();
});

$(".keywordVisualizationsGraphBind").change(function () {
    var tempData;
    var tempYmin = 0;
    var tempYmax = null;
    var data = cachedGraphData[visualizationClickData.cacheKey];

    var chart = $('.highcharts-target').highcharts();

    switch ($(this).val()) {
        case "Auto":
            bindingAxis.YMax = null;
            bindingAxis.YMin = null;
            saveVisulizationState();
            drawGenericChart();
            return;
        case "Sessions":
            tempData = data.Sessions;
            chart.series[0].setVisible(true);
            break;
        case "Clicks":
            tempData = data.Clicks;
            chart.series[1].setVisible(true);
            break;
        case "PaidClicks":
            tempData = data.PaidClicks;
            chart.series[2].setVisible(true);
            break;
        case "CTR":
            tempData = data.CTR;
            chart.series[3].setVisible(true);
            break;
        case "CPC":
            tempData = data.CPC;
            chart.series[4].setVisible(true);
            break;
        case "RPC":
            tempData = data.RPC;
            chart.series[5].setVisible(true);
            break;
        case "RPI":
            tempData = data.RPI;
            chart.series[6].setVisible(true);
            break;
        case "Spend":
            tempData = data.Spend;
            chart.series[7].setVisible(true);
            break;
        case "Revenue":
            tempData = data.Revenue;
            chart.series[8].setVisible(true);
            break;
        case "GP":
            tempData = data.GP;
            chart.series[9].setVisible(true);
            tempYmin = null;
            break;
        case "ROAS":
            tempData = data.ROAS;
            chart.series[10].setVisible(true);
            tempYmin = null;
            break;
        case "TQ":
            tempData = data.TQ;
            chart.series[11].setVisible(true);
            tempYmax = 10;
            break;
        case "Source TQ":
            tempData = data.SourceTQ;
            chart.series[12].setVisible(true);
            tempYmax = 10;
            break;
        case "Bid":
            tempData = data.Bid;
            chart.series[13].setVisible(true);
            break;
        case "Budget":
            tempData = data.Budget;
            chart.series[14].setVisible(true);
            break;
    }
    saveVisulizationState();

    //Remove existing 
    chart.get('sessions-axis').remove();
    chart.get('clicks-axis').remove();
    chart.get('paid-clicks-axis').remove();
    chart.get('ctr-axis').remove();
    chart.get('cpc-axis').remove();
    chart.get('rpc-axis').remove();
    chart.get('rpi-axis').remove();
    chart.get('spend-axis').remove();
    chart.get('revenue-axis').remove();
    chart.get('gp-axis').remove();
    chart.get('roas-axis').remove();
    chart.get('tq-axis').remove();
    chart.get('source-tq-axis').remove();
    chart.get('bid-axis').remove();
    chart.get('budget-axis').remove();

    //Add the new bound one
    chart.addAxis({
        id: 'temp-axis',
        min: tempYmin,
        max: tempYmax
    }, false, true, false);
    chart.addSeries({
        id: 'temp-series',
        data: tempData,
        yAxis: 'temp-axis',
    }, true, false);

    bindingAxis.YMax = chart.get('temp-axis').getExtremes().max;
    bindingAxis.YMin = chart.get('temp-axis').getExtremes().min;
    chart.get('temp-axis').remove();

    drawGenericChart();

});
$(".dateSelectorDropdownKeywordVisualizations").on("change", function (e) {
    saveVisulizationState();

    if ($(this).val() == "Custom") {
        //Forces open the date selectors
        getVisulizationState();
        return false;
    }

    loadVisualizationsGraph();
    return false;
});

$(".hourlyCheckbox").on("change", function (e) {
    saveVisulizationState();

    loadVisualizationsGraph();
    return false;
});

$(".KeywordVisualizationsDateSubmitButton").live("click", function (e) {
    var parent = $(this).parent();
    var dateSelectorDropdown = parent.children(".dateSelectorDropdownKeywordVisualizations").val();
    var startDate = parent.children(".startDateContainerKeywordVisualizations").children(".startDateKeywordVisualizations").val();
    var endDate = parent.children(".endDateContainerKeywordVisualizations").children(".endDateKeywordVisualizations").val();
    if (dateSelectorDropdown == "Custom") {
        var today = $.datepicker.formatDate('yy-mm-dd', new Date());
        if (startDate == "" && endDate == "") {
            alert("Please enter a Start Date and an End Date.");
            return;
        } else if (startDate == "") {
            alert("Please enter a Start Date.");
            return;
        } else if (endDate == "") {
            alert("Please enter an End Date.");
            return;
        } else if (startDate > endDate) {
            alert("Please enter a valid date range (Start Date is currently past the End Date).");
            return;
        } else if (endDate > today) {
            alert("Please re-enter the End Date (cannot enter an End Date that is in the future).");
            return;
        }
    }
    saveVisulizationState();
    loadVisualizationsGraph();
    return false;
});


function getVisulizationState() {
    var currentState = {};
    if ($.cookie("VisualizationState")) {
        currentState = $.parseJSON($.cookie("VisualizationState"));
    } else {
        currentState.dateSelector = "LastFourteenDays";
        currentState.startDate = "";
        currentState.endDate = "";
        currentState.bindTo = "Auto";

        currentState.graph = {};
        currentState.graph.Sessions = false;
        currentState.graph.Clicks = false;
        currentState.graph.PaidClicks = false;
        currentState.graph.CTR = false;
        currentState.graph.CTR = false;
        currentState.graph.CPC = false;
        currentState.graph.RPC = true;
        currentState.graph.RPI = true;
        currentState.graph.Spend = false;
        currentState.graph.Revenue = false;
        currentState.graph.GP = false;
        currentState.graph.ROAS = false;
        currentState.graph.TQ = false;
        currentState.graph.SourceTQ = false;
        currentState.graph.Bid = false;
        currentState.graph.Budget = false;

        currentState.HourlyDropDown = "None";
    }


    //Change the Selectors

    //Date Selector
    $(".dateSelectorDropdownKeywordVisualizations").val(currentState.dateSelector);

    //StartDate and endDate selector
    $(".startDateKeywordVisualizations").val(currentState.startDate);
    $(".endDateKeywordVisualizations").val(currentState.endDate);

    if ($(".dateSelectorDropdownKeywordVisualizations").val() == "Custom") {
        $(".startDateContainerKeywordVisualizations").show();
        $(".endDateContainerKeywordVisualizations").show();
    }
    else {
        $(".startDateContainerKeywordVisualizations").hide();
        $(".endDateContainerKeywordVisualizations").hide();
    }

    //Bind selector
    $('.keywordVisualizationsGraphBind').val(currentState.bindTo);

    $('.hourlyCheckbox').val(currentState.HourlyDropDown);

   

    return currentState;
}

function saveVisulizationState() {
    var currentState = {};
    currentState.dateSelector = $(".dateSelectorDropdownKeywordVisualizations").val();
    currentState.startDate = $(".startDateKeywordVisualizations").val();
    currentState.endDate = $(".endDateKeywordVisualizations").val();
    currentState.bindTo = $('.keywordVisualizationsGraphBind').val();

    currentState.HourlyDropDown = $('.hourlyCheckbox').val();

    var chart = $('.highcharts-target').highcharts();

   
    currentState.graph = {};
    if (chart) {
        currentState.graph.Sessions = chart.series[0].visible;
        currentState.graph.Clicks = chart.series[1].visible;
        currentState.graph.PaidClicks = chart.series[2].visible;
        currentState.graph.CTR = chart.series[3].visible;
        currentState.graph.CPC = chart.series[4].visible;
        currentState.graph.RPC = chart.series[5].visible;
        currentState.graph.RPI = chart.series[6].visible;
        currentState.graph.Spend = chart.series[7].visible;
        currentState.graph.Revenue = chart.series[8].visible;
        currentState.graph.GP = chart.series[9].visible;
        currentState.graph.ROAS = chart.series[10].visible;
        currentState.graph.TQ = chart.series[11].visible;
        currentState.graph.SourceTQ = chart.series[12].visible;
        currentState.graph.Bid = chart.series[13].visible;
        currentState.graph.Budget = chart.series[14].visible;
    } else {
        var oldState = getVisulizationState();
        currentState.graph = oldState.graph;
    }
   

    $.cookie("VisualizationState", JSON.stringify(currentState), { path: '/' });
}
function resetBind() {
    bindingAxis = {};
    getVisulizationState();
    $('.keywordVisualizationsGraphBind').val("Auto");
    saveVisulizationState();
}