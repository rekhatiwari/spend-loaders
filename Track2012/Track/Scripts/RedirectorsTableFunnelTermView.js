﻿
// Jquery function for handling Date Selector controls for keywords (see KeywordsInline.cshtml)
$(function () {
    $(".dateSelectorKeywords1Dropdown").live("change", function () {
        $(".dateSelectorKeywords1Dropdown").val($(this).val());
        if ($(this).val() == "Custom") {
            $(".startDateKeywords1Container").show();
            $(".endDateKeywords1Container").show();
        }
        else {
            $(".startDateKeywords1Container").hide();
            $(".endDateKeywords1Container").hide();
        }
        $('.displayedDate1Keywords').html("- Press Submit to see the effective dates -");
    });
});

$(function () {
    $(".dateSelectorKeywords2Dropdown").live("change", function () {
        $(".dateSelectorKeywords2Dropdown").val($(this).val());
        if ($(this).val() == "Custom") {
            $(".startDateKeywords2Container").show();
            $(".endDateKeywords2Container").show();
        }
        else {
            $(".startDateKeywords2Container").hide();
            $(".endDateKeywords2Container").hide();
        }
        $('.displayedDate2Keywords').html("- Press Submit to see the effective dates -");

    });
});

$(document).on("focus", "input.date:not(.hasDatepicker)", function () {
    $(this).datepicker({
        dateFormat: "yy-mm-dd",
        constrainInput: true,
    });
});


// also change all keyword date fields when a date field changes
$(function () {
    $(".startDateKeywords1").live("change", function () {
        $(".startDateKeywords1").val($(this).val());
    });
});

$(function () {
    $(".endDateKeywords1").live("change", function () {
        $(".endDateKeywords1").val($(this).val());
    });
});

$(function () {
    $(".startDateKeywords2").live("change", function () {
        $(".startDateKeywords2").val($(this).val());
    });
});

$(function () {
    $(".endDateKeywords2").live("change", function () {
        $(".endDateKeywords2").val($(this).val());
    });
});



// BEGIN RedirectRuleTableRow functionality
var previousTableData = new Object();
var previousAutoFunnel = new Object();
var previousAutoFunnelTemplateId = new Object();

var selectedRedirectId = 0;
// called when a Redirector row is expanded.  Makes a JSON request to fetch keyword data and populate into the table

var redirectRulesDetails = [];
$('.expando-trigger').live("click", function () {
    var tr = $(this).closest('tr');
    var row = dataTable.row(tr);

    var redirectId = $(this).data("redirectid");
    if (redirectId == 0) {
        //Its a grouping row
        var campaign = $(this).data("parentcampaignname");
        var childRows =  $('tr.childRow[data-campaignName="' + campaign + '"]');
        if (tr.hasClass('shown')) {
            //Close all the children
            childRows.each(function (index, item) {
                var itemRow = dataTable.row(item);
                if (itemRow.child.isShown()) {
                    // This row is already open - close it
                    itemRow.child.hide();
                    $(item).removeClass('shown');
                    var expando = $(item).find("a.expando-trigger");
                    var state1 = $(expando).text();
                    var state2 = $(expando).attr("toggle");
                    $(expando).text(state2).attr("toggle", state1);
                }
            });
            childRows.hide();
            tr.removeClass('shown');
        } else {
            tr.addClass('shown');
            childRows.show();

        }
        return;
    }

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    }
    else {
        // Open this row
        if ($(".inlineKeywordsDiv-" + redirectId).length == 0) {
            row.child($('.keywords-' + redirectId).clone().show().addClass("inlineKeywordsDiv-" + redirectId).addClass("enabled")).show();

            $('.keywords-' + redirectId).tabs({  });
            $('.keywords-' + redirectId).bind("tabsshow", function (event, ui) {
                if ($(ui.tab).html() == "Advertisers") {
                    RestoreAdvertiserDateSettings();
                    loadAdvertiserData($(ui.tab).data('redirectid'));
                }
            })
        }
        
        tr.addClass('shown');
    }

    redirectRulesDetails[redirectId] = {};
    redirectRulesDetails[redirectId].ImplementationType =  $(this).data("type");
    redirectRulesDetails[redirectId].isTwoClick =  $(this).data("twoclick");

    populateDateSelectorDropdown();

    if ($('#expand_tr_' + redirectId).attr("toggle").trim() == "[+]") {
        $('#loadingDialog').removeAttr('style');
        $('#loadingDialog').dialog('open');

        loadKeywordsTable(redirectId, "loading");
    }

});

var keywordTablesToLoadCount;

function loadKeywordsTable(redirectId, loadType) {



    var params = new Object();
    params.id = redirectId;

    var dateselectorValues = retrieveDateSelectorValue("1")

    params.dateSelectorValKeywords1 = dateselectorValues.dropdown;
    params.startDateKeywords1 = dateselectorValues.startDate;
    params.endDateKeywords1 = dateselectorValues.endDate;

    dateselectorValues = retrieveDateSelectorValue("2")

    params.dateSelectorValKeywords2 = dateselectorValues.dropdown;
    params.startDateKeywords2 = dateselectorValues.startDate;
    params.endDateKeywords2 = dateselectorValues.endDate;
    params.isTwoClick = redirectRulesDetails[redirectId].isTwoClick || redirectRulesDetails[redirectId].ImplementationType == "twoclick-widget-funnel" || redirectRulesDetails[redirectId].ImplementationType == "twoclick-widget";
    
    $.post(keywordsJsonUrl, params,
        function (data) {
            
            if (loadType == "loading") {
                $('#loadingDialog').dialog('close');
                $('#loadingDialog').css('display', 'none');
            }
            else if (loadType == "refreshing") {
                $('#refreshingDataDialog').dialog('close');
                $('#refreshingDataDialog').css('display', 'none');

                // displays notification bar at top of screen (notifying user that keywords have been saved)
                $("#Notifications").load(DisplayUrl);
            }
            else if (loadType == "dateSelectorSubmit") {
                // Decrement keywords counter (will close dialog in calling function when ready)
                keywordTablesToLoadCount--;
            }

            if (data.DateRange1) {
                $('.displayedDate1Keywords').html(data.DateRange1);
                $('.displayedDate2Keywords').html(data.DateRange2);
            }
            initKeywordsTable(redirectId, data.Keywords, data.AutoFunnel);


            // disable apply/cancel buttons
            $(".enabled.keywords-" +redirectId+ " .ApplyChanges").attr("disabled", "disabled");
            $(".enabled.keywords-" +redirectId+ " .ApplyAndClose").attr("disabled", "disabled");
            $(".enabled.keywords-" + redirectId + " .cancel").addClass("disabled");

            // populate autofunnel checkbox
            $(".enabled.keywords-" +redirectId+ " .AutoFunnelCheckbox").prop('checked', data.AutoFunnel);

            // populate AutoFunnelTemplate dropdown, and select the currently selected AutoFunnelTemplate
            // also, populate AutoFunnelTemplateDescriptions as well
            $(".enabled.keywords-" +redirectId+ " .AutoFunnelTemplateDropdown").empty();
            $(".enabled.keywords-" +redirectId+ " .AutoFunnelTemplateDescriptions").empty();
            for (var i = 0; i < data.AutoFunnelTemplates.length; i++) {
                if (data.AutoFunnelTemplates[i].Id == data.AutoFunnelTemplateId) {
                    $(".enabled.keywords-" + redirectId + " .AutoFunnelTemplateDropdown").append("<option value=\"" + data.AutoFunnelTemplates[i].Id + "\" selected=\"selected\">" + data.AutoFunnelTemplates[i].Name + "</option>");
                }
                else {
                    $(".enabled.keywords-" + redirectId + " .AutoFunnelTemplateDropdown").append("<option value=\"" + data.AutoFunnelTemplates[i].Id + "\">" + data.AutoFunnelTemplates[i].Name + "</option>");
                }
                $(".enabled.keywords-" + redirectId + " .AutoFunnelTemplateDescriptions").append("<h2>" + data.AutoFunnelTemplates[i].Name + "</h2>");
                $(".enabled.keywords-" + redirectId + " .AutoFunnelTemplateDescriptions").append("<p>" + data.AutoFunnelTemplates[i].Description + "</p>");
            }

            previousAutoFunnelTemplateId[redirectId] = data.AutoFunnelTemplateId;

            // hide autofunnel template dropdown & descriptions link if autofunnel template is disabled
            if (data.AutoFunnel) {
                $(".enabled.keywords-" +redirectId+ " .automatic-weighting-widgets").css("visibility", "visible");
            }

            if (loadType == "applyAndClose") {
                
                dataTable.row($('.enabled.keywords-' + redirectId).closest('tr').prev()).child.hide();

                // hide Saving Dialog
                $('#savingDialog').dialog('close');
                $('#savingDialog').css('display', 'none');

                // toggle the close keywords 'row' (done below)
                var elem = $("#expand_tr_" + redirectId);

                var state1 = elem.text();
                var state2 = elem.attr("toggle");
                var href = elem.attr("href");

                $(href).toggle("fast");

                elem.text(state2);
                elem.attr("toggle", state1);

                // display save notification at top
                $("#Notifications").load(DisplayUrl);
            }
        }
            , "json").fail(function () {
                alert("Error Loading Redirect Rule, please contact support");

                dataTable.row($('.enabled.keywords-' + redirectId).closest('tr').prev()).child.hide();

                // toggle the close keywords 'row' (done below)
                var elem = $("#expand_tr_" + redirectId);

                var state1 = elem.text();
                var state2 = elem.attr("toggle");
                var href = elem.attr("href");

                $(href).toggle("fast");

                elem.text(state2);
                elem.attr("toggle", state1);

                if (loadType == "loading") {
                    $('#loadingDialog').dialog('close');
                    $('#loadingDialog').css('display', 'none');
                }
                else if (loadType == "refreshing") {
                    $('#refreshingDataDialog').dialog('close');
                    $('#refreshingDataDialog').css('display', 'none');
                }
            });
}

var globalIsAutoFunnel = null;

// called to create the keywords table.  Also used when refreshing the table in some cases (table is deleted, then recreated with function below)
function initKeywordsTable(redirectId, data, isAutoFunnel) {

    var implementationType = redirectRulesDetails[redirectId].ImplementationType;
    var isTwoClick = redirectRulesDetails[redirectId].isTwoClick;

    globalIsAutoFunnel = isAutoFunnel;

    var sortCookie = undefined;

    if ($.cookie("RedirectorKeywordTableSorting")) {
        sortCookie = $.parseJSON($.cookie("RedirectorKeywordTableSorting"));
    }
    selectedRedirectId = redirectId;
    var allColumns = [{ data: 'KeywordId', header: "<span class='colHeader columnSorting' title='Action'>Action</span>", renderer: TableRenderer.actionRender, width: 60 },
                  { data: 'Keyword', header: "<span class='colHeader columnSorting' title='Keywords'>Keywords</span>", width: 128, renderer: TableRenderer.DuplicateDetector },
                  { data: 'RelatedTerm', header: "<span class='colHeader columnSorting' title='Related Term'>Related Term</span>", width: 248, renderer: TableRenderer.DuplicateDetector },
                  { data: 'DisplayTerm', header: "<span class='colHeader columnSorting' title='Display Term</span>'>Display Term</span></span>", width: 248, renderer: TableRenderer.DuplicateDetector },
                  { data: 'HtmlRelatedTerm', header: "<span class='colHeader columnSorting' title='Funnel Term'>Funnel Term</span>", width: 248, renderer: TableRenderer.DuplicateDetector },
                  { data: 'FunnelTerm', header: "<span class='colHeader columnSorting' title='Funnel Term'>Funnel Term</span>", width: 248, renderer: TableRenderer.DuplicateDetector },
                  { data: 'Weight', type: 'numeric', header: "<span class='colHeader columnSorting' title='Weight'>Weight</span>", width: 128, renderer: TableRenderer.Center },
                  { data: 'Notes', header: "<span class='colHeader columnSorting' title='Notes'>Notes</span>", width: 128 },
                  { data: 'UserWeight', type: 'numeric', header: "<span class='colHeader columnSorting' title='User Weight'>User Weight</span>", width: 128, renderer: TableRenderer.Center },
                  { data: 'TypeTag', type: 'numeric', header: "<span class='colHeader columnSorting' title='TypeTag'>TypeTag</span>", width: 128, renderer: TableRenderer.Center },
                  { data: 'YAdUrl', type: 'numeric', header: "<span class='colHeader columnSorting' title='Headline'>Headline</span>", width: 128, renderer: TableRenderer.Center },
                  { data: 'Url', header: "<span class='colHeader columnSorting' title='Url'>Url</span>", width: 128, renderer: TableRenderer.Center },
                  { data: 'WeightEffectivePercent', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Weight %'>Weight %</span>", width: 83, renderer: TableRenderer.weightPercentRender },
                  { data: 'UserWeightEffectivePercent', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='User Weight %'>User Weight %</span>", width: 128, renderer: TableRenderer.weightPercentRender },

                  //First Date
                  { data: 'EffectivePercent1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Effective %'>Effective %</span>", width: 88, renderer: TableRenderer.EffectivePercentDateOne },
                  { data: 'Sessions1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Sessions'>Sessions</span>", width: 80, renderer: TableRenderer.BareNumberDateOne },
                  { data: 'PurchasedClicks1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Purchased Clicks'>Purchased Clicks</span>", width: 140, renderer: TableRenderer.BareNumberDateOne },
                  { data: 'LanderVisitsTotal1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Landers'>Landers</span>", width: 80, renderer: TableRenderer.BareNumberDateOne },
                  { data: 'OutboundClicks1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Clicks'>Clicks</span>", width: 68, renderer: TableRenderer.BareNumberDateOne },
                  { data: 'ResultsVisits1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Results'>Results</span>", width: 65, renderer: TableRenderer.BareNumberDateOne },
                  { data: 'PaidClicks1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Clicks'>Clicks</span>", width: 68, renderer: TableRenderer.BareNumberDateOne },
                  { data: 'ClickDiscount1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Click Discount'>Click Discount</span>", width: 130, renderer: TableRenderer.PercentDateOne },

                  { data: 'Ctr1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Ctr'>Ctr</span>", width: 55, renderer: TableRenderer.PercentDateOne },
                  { data: 'PurchasedCtr1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Purchased Clicks Ctr'>Ctr</span>", width: 55, renderer: TableRenderer.PercentDateOne },

                  { data: 'Cpc1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Cpc'>Cpc</span>", width: 55, renderer: TableRenderer.MoneyDateOne },
                  { data: 'PurchasedCpc1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Purchased Clicks Cpc'>Cpc</span>", width: 55, renderer: TableRenderer.MoneyDateOne },

                  { data: 'Rpc1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Rpc'>Rpc</span>", width: 55, renderer: TableRenderer.MoneyDateOne },

                  { data: 'Rpi1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Rpi'>Rpi</span>", width: 55, renderer: TableRenderer.MoneyDateOne },
                  { data: 'PurchasedRpi1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Purchased Clicks Rpi'>Rpi</span>", width: 55, renderer: TableRenderer.MoneyDateOne },

                  { data: 'Spend1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Spend'>Spend</span>", width: 70, renderer: TableRenderer.MoneyDateOne },
                  { data: 'Revenue1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Rev'>Rev</span>", width: 70, renderer: TableRenderer.MoneyDateOne },
                  { data: 'GP1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='GP'>GP</span>", width: 80, renderer: TableRenderer.MoneyDateOne },
                  { data: 'TypeTQ1Display', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='TQ'>TQ</span>", width: 105, renderer: TableRenderer.BareNumberDateOne },
                  { data: 'TypeTQ1', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='TQ'>TQ</span>", width: 105, renderer: TableRenderer.BareNumberDateOne },


                    //Second Date
                  { data: 'EffectivePercent2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Effective %'>Effective %</span>", width: 88, renderer: TableRenderer.EffectivePercentDateTwo },
                  { data: 'Sessions2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Sessions'>Sessions</span>", width: 80, renderer: TableRenderer.BareNumberDateTwo },
                  { data: 'PurchasedClicks2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Purchased Clicks'>Purchased Clicks</span>", width: 140, renderer: TableRenderer.BareNumberDateTwo },
                  { data: 'LanderVisitsTotal2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Landers'>Landers</span>", width: 80, renderer: TableRenderer.BareNumberDateTwo },
                  { data: 'OutboundClicks2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Clicks'>Clicks</span>", width: 68, renderer: TableRenderer.BareNumberDateTwo },
                  { data: 'ResultsVisits2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Results'>Results</span>", width: 65, renderer: TableRenderer.BareNumberDateTwo },
                  { data: 'PaidClicks2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Clicks'>Clicks</span>", width: 68, renderer: TableRenderer.BareNumberDateTwo },
                  { data: 'ClickDiscount2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Click Discount'>Click Discount</span>", width: 130, renderer: TableRenderer.PercentDateTwo },

                  { data: 'Ctr2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Ctr'>Ctr</span>", width: 55, renderer: TableRenderer.PercentDateTwo },
                  { data: 'PurchasedCtr2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Purchased Clicks Ctr'>Ctr</span>", width: 55, renderer: TableRenderer.PercentDateTwo },

                  { data: 'Cpc2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Cpc'>Cpc</span>", width: 55, renderer: TableRenderer.MoneyDateTwo },
                  { data: 'PurchasedCpc2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Purchased Clicks Cpc'>Cpc</span>", width: 55, renderer: TableRenderer.MoneyDateTwo },

                  { data: 'Rpc2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Rpc'>Rpc</span>", width: 55, renderer: TableRenderer.MoneyDateTwo },

                  { data: 'Rpi2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Rpi'>Rpi</span>", width: 55, renderer: TableRenderer.MoneyDateTwo },
                  { data: 'PurchasedRpi2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Purchased Clicks Rpi'>Rpi</span>", width: 55, renderer: TableRenderer.MoneyDateTwo },

                  { data: 'Spend2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Spend'>Spend</span>", width: 70, renderer: TableRenderer.MoneyDateTwo },
                  { data: 'Revenue2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='Rev'>Rev</span>", width: 70, renderer: TableRenderer.MoneyDateTwo },
                  { data: 'GP2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='GP'>GP</span>", width: 80, renderer: TableRenderer.MoneyDateTwo },
                  { data: 'TypeTQ2Display', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='TQ'>TQ</span>", width: 105, renderer: TableRenderer.BareNumberDateTwo },
                  { data: 'TypeTQ2', type: 'numeric', readOnly: true, header: "<span class='colHeader columnSorting' title='TQ'>TQ</span>", width: 105, renderer: TableRenderer.BareNumberDateTwo }];


    var selectedColumns = [];
    var includedColumns = [];
    
    if (isTwoClick || implementationType == "twoclick-widget-funnel" || implementationType == "twoclick-widget") {
        
        includedColumns = ['KeywordId', 'Weight', 'Notes', 'Url', 'WeightEffectivePercent',
            'LanderVisitsTotal1', 'ResultsVisits1', 'PaidClicks1', 'Ctr1', 'Revenue1', 'Rpc1',
            'LanderVisitsTotal2', 'ResultsVisits2', 'PaidClicks2', 'Ctr2', 'Revenue2', 'Rpc2'];

        if (implementationType == "twoclick-widget-funnel") {
            includedColumns.push("DisplayTerm", "HtmlRelatedTerm" );
        }else {
            includedColumns.push("RelatedTerm");
        }

        if ($('#redirectorsTableViewTemplate').val() == "Purchased Clicks") {
            //Use  the purchasedClicks instead
            includedColumns.push("PurchasedClicks1");
            includedColumns[includedColumns.indexOf("Ctr1")] = 'PurchasedCtr1';
            includedColumns[includedColumns.indexOf("Rpi1")] = 'PurchasedRpi1';

            includedColumns.push("PurchasedClicks2");
            includedColumns[includedColumns.indexOf("Ctr2")] = 'PurchasedCtr2';
            includedColumns[includedColumns.indexOf("Rpi2")] = 'PurchasedRpi2';
        }
    } else {
        includedColumns = ['KeywordId', "Keyword", 'FunnelTerm', 'Weight', 'Notes', 'Url', 'WeightEffectivePercent',
                'EffectivePercent1', 'Sessions1', 'OutboundClicks1', 'ClickDiscount1','Ctr1', 'Cpc1', 'Rpc1', 'Rpi1', 'Spend1', 'Revenue1', 'GP1', 'TypeTQ1Display',
                'EffectivePercent2', 'Sessions2', 'OutboundClicks2', 'ClickDiscount2', 'Ctr2', 'Cpc2', 'Rpc2', 'Rpi2', 'Spend2', 'Revenue2', 'GP2', 'TypeTQ2Display'];

            if ($('#redirectorsTableViewTemplate').val() == "Purchased Clicks") {
                //Use  the purchasedClicks, purchasedCTR, PurchasedCPC, and PurchasedPRI instead of sessions, CTR, CPC and RPI
                includedColumns[includedColumns.indexOf("Sessions1")] = 'PurchasedClicks1';
                includedColumns[includedColumns.indexOf("Ctr1")] = 'PurchasedCtr1';
                includedColumns[includedColumns.indexOf("Cpc1")] = 'PurchasedCpc1';
                includedColumns[includedColumns.indexOf("Rpi1")] = 'PurchasedRpi1';

                includedColumns[includedColumns.indexOf("Sessions2")] = 'PurchasedClicks2';
                includedColumns[includedColumns.indexOf("Ctr2")] = 'PurchasedCtr2';
                includedColumns[includedColumns.indexOf("Cpc2")] = 'PurchasedCpc2';
                includedColumns[includedColumns.indexOf("Rpi2")] = 'PurchasedRpi2';
            }
    }

    

    if (isAutoFunnel) {
        includedColumns.push("UserWeight", "UserWeightEffectivePercent");
    }

    selectedColumns = allColumns.filter(function (element, index, array) {
        if (includedColumns.indexOf(element.data) > -1)
            return true;
        return false;
    });
    var columnSort = {
        sortOrder: sortCookie != undefined ? sortCookie.order : false // default to descending (RPI values)
    };
    if (data.length > 0){
        columnSort.column = sortCookie != undefined ? sortCookie.column : 15;
    }
    var hot = $(".enabled.keywords-" + redirectId + " .keywords-grid").handsontable({
        startRows: data.length == 0 ? 1 : data.length,
        startCols: selectedColumns.length,
        colHeaders: selectedColumns.map(function (a) { return a.header; }),
        colWidths: selectedColumns.map(function (a) { return a.width; }),
        data: data.length > 0 ? data : null,
        minSpareCols: 0,
        minSpareRows: 1,
        formulas: true,
        columnSorting: columnSort,
        sortIndicator: true,
        fillHandle: "vertical",
        beforeChange: function (changes, source) {
            if ($(".enabled.keywords-" + redirectId + " .AutoFunnelCheckbox").is(":checked")) {
                globalIsAutoFunnel = true;
            }
            else {
                globalIsAutoFunnel = false;
            }
        },
        afterChange: function (data) {
            // enable apply/cancel buttons
            $(".enabled.keywords-" + redirectId + " .ApplyChanges").removeAttr("disabled");
            $(".enabled.keywords-" + redirectId + " .ApplyAndClose").removeAttr("disabled");
            $(".enabled.keywords-" + redirectId + " .cancel").removeClass("disabled");
        },
        afterColumnSort: function (column, order) {
            // save sorting in a cookie for future retrieval
            var sortCookie = new Object();
            sortCookie.column = column;
            sortCookie.order = order;
            $.cookie("RedirectorKeywordTableSorting", JSON.stringify(sortCookie), { path: '/' });
        },
        cells: function (row, col, prop) {
            var cellProperties = {};
            if (col >= "7" || col == "0") {
                cellProperties.readOnly = true;
            }
            return cellProperties;
        },
        columns: selectedColumns,
        afterSelection: function (r, c, r2, c2) {

            // User has clicked on a TQ column if col = 19 or col = 30 (automatic weighting is on)
            if ((c == 19 && c2 == 19) || (c == 30 && c2 == 30)) {
                if (r == r2) {
                    // NOTE: Only display TQ graphs if a TQ value is defined for the selected cell
                    var dataMapTQVal = $(".enabled.keywords-" + redirectId + " .keywords-grid").handsontable("getSourceData", r, c, r2, c2);
                    if (!(dataMapTQVal[0][0] == "" || dataMapTQVal[0][0] == null)) {
                        // NOTE: Column 30 is storing the Keyword Id (since automatic weighting is on)
                        var dataMapKeyword = $("#keywords-grid-" +redirectId).handsontable("getSourceData", r, 31, r2, 31);
                        $("#dialog-keyword-tq-graph-" + dataMapKeyword[0][0]).dialog('open');
                    }
                }
            }
        },
        afterRender: attachVisualizationListener
    });
}
function TableRenderer() { };

TableRenderer.actionRender = function (instance, td, row, col, prop, value, cellProperties) {

    if (value) {
        td.innerHTML = '<a data-keywordid=' + value + ' data-relatedTerm="' + cellProperties.instance.getDataAtRowProp(row, 'RelatedTerm') + '" data-funnelTerm="' + cellProperties.instance.getDataAtRowProp(row, 'FunnelTerm') + '" data-redirectId=' + selectedRedirectId + ' data-keyword="' + cellProperties.instance.getDataAtRowProp(row, 'Keyword') + '" href="#" class="display-keyword-visualizations-graph"><img style="border:none; outline:none;" title="Show Visualizations" src="' + visualizationsUrl + '" /></a>';
    }
    return td;
}

TableRenderer.moneyRender = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    if (value) {
        value = numeral(value).format('$0.00');
    }
    else if (value == 0) {
        value = numeral(value).format('$0');
    }
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.percentRender = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    if (value) 
        value = numeral(value).format('0%');
    else if (value == 0) 
        value = numeral(value).format('0%');
    
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.weightPercentRender = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#E3F0C9";
    td.style.color = 'black';

    var weightCalculationSource = "Weight";
    if (prop === "UserWeightEffectivePercent")
        weightCalculationSource = "UserWeight"

    var total = 0;
    
    var allData = cellProperties.instance.getSourceData();
    
    for (i = 0; i < allData.length ; i++) {
        if (allData[i][weightCalculationSource]) {
            total += allData[i][weightCalculationSource];
        }
    }
    var rowWeight = cellProperties.instance.getDataAtRowProp(row, weightCalculationSource);

    
    if (rowWeight !== null && total > 0) {
        value = rowWeight / total;
        value = numeral(value).format((total > 100) ? "0.00%" : "0%");
    }
    else if (rowWeight !== null) {
        value = numeral(0).format('0.0%');
    } else {
        value = "";
    }
        

    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.EffectivePercentDateOne = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#FFFFEE";
    td.style.color = 'black';
    if (value)
        value = numeral(value).format('0%');
    else if (value == 0)
        value = numeral(value).format('0%');
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.EffectivePercentWithDecimalDateOne = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#FFFFEE";
    td.style.color = 'black';
    if (value)
        value = numeral(value).format('0.0%');
    else if (value == 0)
        value = numeral(value).format('0.0%');
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.BareNumberDateOne = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#FFFFEE";
    td.style.color = 'black';
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.PercentDateOne = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#FFFFEE";
    td.style.color = 'black';
    if (value)
        value = numeral(value).format('0%');
    else if (value == 0)
        value = numeral(value).format('0%');
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.MoneyDateOne = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#FFFFEE";
    td.style.color = 'black';

    var isNegative = false;
    if (value < 0) {
        value = value * -1;
        isNegative = true;
    }
    if (value) {
        value = numeral(value).format('$0.00');
    }
    else if (value == 0) {
        value = numeral(value).format('$0');
    }
    if (isNegative) {
        value = "(" + value + ")";
    }
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.EffectivePercentDateTwo = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#EEFFFF";
    td.style.color = 'black';
    if (value)
        value = numeral(value).format('0%');
    else if (value == 0)
        value = numeral(value).format('0%');
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}
TableRenderer.EffectivePercentWithDecimalDateTwo = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#EEFFFF";
    td.style.color = 'black';
    if (value)
        value = numeral(value).format('0.0%');
    else if (value == 0)
        value = numeral(value).format('0.0%');
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.BareNumberDateTwo = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#EEFFFF";
    td.style.color = 'black';
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.PercentDateTwo = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#EEFFFF";
    td.style.color = 'black';
    if (value)
        value = numeral(value).format('0%');
    else if (value == 0)
        value = numeral(value).format('0%');
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.MoneyDateTwo = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    td.style.backgroundColor = "#EEFFFF";
    td.style.color = 'black';

    var isNegative = false;
    if (value < 0) {
        value = value * -1;
        isNegative = true;
    }
    if (value) {
        value = numeral(value).format('$0.00');
    }
    else if (value == 0) {
        value = numeral(value).format('$0');
    }
    if (isNegative) {
        value = "(" + value + ")";
    }
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}

TableRenderer.Center = function (instance, td, row, col, prop, value, cellProperties) {
    td.style.textAlign = "center";
    Handsontable.renderers.TextRenderer.apply(this, arguments);
}
TableRenderer.DuplicateDetector = function (instance, td, row, col, prop, value, cellProperties) {
    var allData = cellProperties.instance.getSourceData();
    var allTerms = [];
    for (i = 0; i < allData.length ; i++) {
        var item = allData[i];

        var funnelTerm = item.FunnelTerm == null || item.FunnelTerm.length == 0 ? "*" : item.FunnelTerm;
        var keyword = item.Keyword == null || item.Keyword.length == 0 ? "*" : item.Keyword;
        var RelatedTerm = item.RelatedTerm == null || item.RelatedTerm.length == 0 ? "*" : item.RelatedTerm;
        if (RelatedTerm == "*" && item.HtmlRelatedTerm != null && item.HtmlRelatedTerm.length > 0) {
            RelatedTerm = item.HtmlRelatedTerm;
        }
        var DisplayTerm = item.DisplayTerm == null || item.DisplayTerm.length == 0 ? "*" : item.DisplayTerm;
        allTerms.push("ft:" + funnelTerm + ":::" + keyword + ":::" + RelatedTerm + ":::" + DisplayTerm);
    }
   
    var thisRow = cellProperties.instance.getSourceDataAtRow(row);

    var funnelTerm = thisRow.FunnelTerm == null || thisRow.FunnelTerm.length == 0 ? "*" : thisRow.FunnelTerm;
    var keyword = thisRow.Keyword == null || thisRow.Keyword.length == 0 ? "*" : thisRow.Keyword;
    var RelatedTerm = thisRow.RelatedTerm == null || thisRow.RelatedTerm.length == 0 ? "*" : thisRow.RelatedTerm;
    if (RelatedTerm == "*" && thisRow.HtmlRelatedTerm != null && thisRow.HtmlRelatedTerm.length > 0) {
        RelatedTerm = thisRow.HtmlRelatedTerm;
    }
    var DisplayTerm = thisRow.DisplayTerm == null || thisRow.DisplayTerm.length == 0 ? "*" : thisRow.DisplayTerm;
    var item = "ft:" + funnelTerm + ":::" + keyword + ":::" + RelatedTerm + ":::" + DisplayTerm;
    if (allTerms.indexOf(item) != allTerms.lastIndexOf(item) && item != "ft:*:::*:::*:::*")
        td.style.backgroundColor = "#ff8f6c";
    else
        td.style.backgroundColor = "#FFF";
    Handsontable.renderers.TextRenderer.apply(this, arguments);    
}



// Event triggers when cancel button is clicked.  Refreshes keywords table to previous state (last saved if previously saved, or original data when loaded)
function cancel(redirectId, campaignName, adGroup) {

    // alert user that the changes have been reverted
    alert("Changes for \"" + campaignName + " - " + adGroup + "\" (id = " + redirectId + ") will be reverted");

    loadKeywordsTable(redirectId, "");

    $(".enabled.keywords-" + redirectId + " .AutoFunnelCheckbox").prop('checked', previousAutoFunnel[redirectId]);

    $(".enabled.keywords-" + redirectId + " .AutoFunnelTemplateDropdown").val(previousAutoFunnelTemplateId[redirectId]);

    // also show/hide autofunnel 'widgets' depending on whether the auto funnel checkbox is checked/unchecked
    if ($(".enabled.keywords-" + redirectId + " .AutoFunnelCheckbox").is(":checked")) {
        $(".enabled.keywords-" + redirectId + " .automatic-weighting-widgets").css("visibility", "visible");
    }
    else {
        $(".enabled.keywords-" + redirectId + " .automatic-weighting-widgets").css("visibility", "hidden");
    }

    // disable the apply/cancel buttons
    $(".enabled.keywords-" + redirectId + " .ApplyChanges").attr("disabled", "disabled");
    $(".enabled.keywords-" + redirectId + " .ApplyAndClose").attr("disabled", "disabled");
    $(".enabled.keywords-" + redirectId + " .cancel").addClass("disabled");

   
}

function SaveKeywords(redirectId, isClose) {
    // display Saving Dialog screen to user.  Will close once save operation is done.
    $('#savingDialog').removeAttr('style');
    $('#savingDialog').dialog('open');

    // get data to be saved
    var items = $(".enabled.keywords-" + redirectId + " .keywords-grid").handsontable("getSourceData");
    var data = {
        Id: redirectId,
        AutoFunnel: $(".enabled.keywords-" + redirectId + " .AutoFunnelCheckbox").is(":checked"),
        AutoFunnelTemplateId: $(".enabled.keywords-" + redirectId + " .AutoFunnelTemplateDropdown").val()
    };

    var duplicateRows = [];

    if (redirectRulesDetails[redirectId].ImplementationType == "twoclick-widget-funnel") {
        items = items.map(function(item){
            if ((item.DisplayTerm == null || item.DisplayTerm == "") && (item.HtmlRelatedTerm == null || item.HtmlRelatedTerm === ""))
                return null;

            var newItem = {};

            newItem.DisplayTerm = item.DisplayTerm.replace("amp;", "");
            newItem.HtmlRelatedTerm = item.HtmlRelatedTerm.replace("amp;", "");
            newItem.Weight = item.Weight;
            newItem.YAdUrl = "";
            newItem.Notes = item.Notes;
            newItem.Url = item.Url;

            if (data.AutoFunnel)
                newItem.UserWeight = item.UserWeight;

            if (!(newItem.HtmlRelatedTerm))
                newItem.HtmlRelatedTerm = "";

            if (newItem.Notes === null)
                newItem.Notes = "";
            if (newItem.Url === null)
                newItem.Url = "";

            if (newItem.Weight == null || newItem.Weight === "")
                newItem.Weight = 1;
            return newItem;
            
        });

    } else if (redirectRulesDetails[redirectId].isTwoClick || redirectRulesDetails[redirectId].ImplementationType == "twoclick-widget") {

        items = items.map(function(item){
            if (item.RelatedTerm == null || item.RelatedTerm === "")
                return null;

            var newItem = {};

            newItem.HtmlRelatedTerm = item.RelatedTerm.replace("amp;", "");
            newItem.Weight = item.Weight;
            newItem.YAdUrl = "";
            newItem.Url = item.Url;
            newItem.Notes = item.Notes;

            if (data.AutoFunnel)
                newItem.UserWeight = item.UserWeight;

            if (!(newItem.HtmlRelatedTerm))
                newItem.HtmlRelatedTerm = "";

            if (newItem.Notes === null)
                newItem.Notes = "";
            if (newItem.Url === null)
                newItem.Url = "";
            if (newItem.Weight == null || newItem.Weight === "")
                newItem.Weight = 1;
            return newItem;
        });
       
    }else{
        items = items.map(function(item){
            if ((item.FunnelTerm == null || item.FunnelTerm == "") && (item.Keyword == null || item.Keyword == ""))
                return null;
            var newItem = {};

            if (item.Keyword)
                newItem.Keyword = item.Keyword.replace("amp;", "");

            if (item.FunnelTerm)
                newItem.FunnelTerm = item.FunnelTerm.replace("amp;", "");

            newItem.Weight = item.Weight;
            newItem.Url = item.Url;
            newItem.Notes = item.Notes;

            if (data.AutoFunnel)
                newItem.UserWeight = item.UserWeight;

            if (!(newItem.Keyword)) 
                newItem.Keyword = "*";
            
            if (newItem.Weight == null || newItem.Weight === "")
                newItem.Weight = 1;

            if (newItem.Notes === null)
                newItem.Notes = "";
            if (newItem.Url === null)
                newItem.Url = "";
            if (!(newItem.UserWeight)) 
                newItem.UserWeight = "";
            
            if (newItem.FunnelTerm == null) 
                newItem.FunnelTerm = "";
            return newItem;
        });
    }
    
    items = items.filter(function (item) {
        if (item == null)
            return false;
        return true;
    });
    
    var uniqueItems = items.filter(function (item, index, array) {
        if (item == null)
            return false;
        for (var i = 0; i < items.length ; i++) {
            if (i == index)
                continue;

            if (typeof (items[i].FunnelTerm) === "undefined" && typeof (item.FunnelTerm) === "undefined" && typeof (items[i].DisplayTerm) === "undefined" && typeof (item.DisplayTerm) === "undefined" &&  (item.HtmlRelatedTerm) && items[i].HtmlRelatedTerm && item.HtmlRelatedTerm == items[i].HtmlRelatedTerm)
                return false;
            if ((item.DisplayTerm) && items[i].DisplayTerm && item.DisplayTerm == items[i].DisplayTerm && (item.HtmlRelatedTerm) && items[i].HtmlRelatedTerm && item.HtmlRelatedTerm == items[i].HtmlRelatedTerm)
                return false;

            if ((item.Keyword) && items[i].Keyword && item.Keyword == items[i].Keyword && (item.FunnelTerm)  && items[i].FunnelTerm && item.FunnelTerm == items[i].FunnelTerm)
                return false;
        }
       return true;
        
    });

    if (uniqueItems.length != items.length) {
        alert("Please remove all duplicate entries before saving");
        $('#savingDialog').dialog('close');
        return;
    }

    data.Keywords = uniqueItems;
    var isTwoClick = redirectRulesDetails[redirectId].isTwoClick || redirectRulesDetails[redirectId].ImplementationType == "twoclick-widget" || redirectRulesDetails[redirectId].ImplementationType == "twoclick-widget-funnel";
    $.post((isTwoClick ? saveKeywordsTwoClickUrl : saveKeywordsUrl)
        , data
        , function (foundSingleOrDoubleQuotes) {

            // reset temporary stored data (now that we are refreshing the table)
            previousTableData[redirectId] = null;
            previousAutoFunnel[redirectId] = null;
            previousAutoFunnelTemplateId[redirectId] = null;

            // alert user if a single or double quote has been removed from any funnel terms
            if (foundSingleOrDoubleQuotes) {
                alert("NOTE: Some of the funnel terms with single or double quotes have been altered.  Quotes have been removed from these funnel terms for compatibility with the system.");
            }

            if (isClose) {
                // if isClose == true, then Apply & Close has been clicked and the Keyword table show minimize itself (ie should 'hide').  Done within the 'loadKeywordsTable' function
                loadKeywordsTable(redirectId, "applyAndClose");
            }
            else {
                // apply button clicked.  Simply close Saving Dialog, display Refreshing Data Dialog, and load data
                $('#savingDialog').dialog('close');
                $('#savingDialog').css('display', 'none');

                $('#refreshingDataDialog').removeAttr('style');
                $('#refreshingDataDialog').dialog('open');

                loadKeywordsTable(redirectId, "refreshing");
            }
        }
        , "json");
}


// Triggers when the AutoFunnel Checkbox is clicked.  Causes the keywords inline table to expand/collapse Automatic Weighting only columns
function AutoFunnelCheckbox(redirectId) {

    // destroy ffffand recreate table to encompass changes to the user weights columns being removed (or added)
    var dataMap = $(".enabled.keywords-" + redirectId + " .keywords-grid").handsontable("getSourceData");

    initKeywordsTable(redirectId, dataMap, $(".enabled.keywords-" + redirectId + " .AutoFunnelCheckbox").is(":checked"));
    

    // also show/hide autofunnel 'widgets' depending on whether the auto funnel checkbox is checked/unchecked
    if ($(".enabled.keywords-" + redirectId + " .AutoFunnelCheckbox").is(":checked")) {
        $(".enabled.keywords-" + redirectId + " .automatic-weighting-widgets").css("visibility", "visible");
    }
    else {
        $(".enabled.keywords-" + redirectId + " .automatic-weighting-widgets").css("visibility", "hidden");
    }

    $(".enabled.keywords-" + redirectId + " .ApplyChanges").removeAttr("disabled");
    $(".enabled.keywords-" + redirectId + " .ApplyAndClose").removeAttr("disabled");
    $(".enabled.keywords-" + redirectId + " .cancel").removeClass("disabled");
}

// Triggered when the Automatic Funnel Template dropdown changes value.  Only need to enable apply/cancel buttons in this case.
function AutoFunnelTemplateDropdown(redirectId) {
    $(".enabled.keywords-" + redirectId + " .ApplyChanges").removeAttr("disabled");
    $(".enabled.keywords-" + redirectId + " .ApplyAndClose").removeAttr("disabled");
    $(".enabled.keywords-" + redirectId + " .cancel").removeClass("disabled");
}

// Triggered when the Apply button is clicked.  Saves changes in keywords table to DB.
function ApplyChanges(redirectId) {
    SaveKeywords(redirectId, false);
}

function ApplyAndClose(redirectId) {
    // Triggered when the Apply & Close button is clicked.  Saves changes in keywords table to DB, and also collapses the current keywords table.
    SaveKeywords(redirectId, true);
    return false;
}



function KeywordsDateSubmitButton(redirectId) {

    //Validate the first 'Custom Selector' 
    if ($(".dateSelectorKeywords1Dropdown").val() == "Custom") {
        if (validateCustomDateType($(".startDateKeywords1").val(), $(".endDateKeywords1").val(), 1) == false)
            return;
    }

    //Validate the second 'Custom Selector'
    if ($(".dateSelectorKeywords2Dropdown").val() == "Custom") {
        if (validateCustomDateType($(".startDateKeywords2").val(), $(".endDateKeywords2").val(), 2) == false)
            return;
    }


    // save keyword date values to two cookies (keyword1DateSelectorCookie and keyword2DateSelectorCookie)
    storeDateSelectorValue("1", $(".dateSelectorKeywords1Dropdown").val(), $(".startDateKeywords1").val(), $(".endDateKeywords1").val())
    storeDateSelectorValue("2", $(".dateSelectorKeywords2Dropdown").val(), $(".startDateKeywords2").val(), $(".endDateKeywords2").val())

    // now show and load data
    $('#loadingDialog').removeAttr('style');
    $('#loadingDialog').dialog('open');

    keywordTablesToLoadCount = 0;

    // want to reload data for all open keywords tables (ie all 'toggled' open keywords)
    $('.redirectorsTableRow').each(function (i) {
        $(this).children("td:eq(0)").each(function (i) {
            if ($(this).children("a:eq(0)").attr("toggle") == "[+]") {
                var redirectId = $(this).children("a:eq(0)").attr("id").split("_")[2];
                keywordTablesToLoadCount++;
                loadKeywordsTable(redirectId, "dateSelectorSubmit");

               
            }
        });
    });

    // wait for all keywords tables to load before closing loadingDialog
    setTimeout(waitForKeywordTablesToLoad, 500);
}

function validateCustomDateType(startDate, endDate, selectorNumber) {
    var today = $.datepicker.formatDate('yy-mm-dd', new Date());
    if (startDate == "" && endDate == "") {
        alert("Please enter a Start Date and an End Date for Date Selector " + selectorNumber);
        return false;
    }
    else if (startDate == "") {
        alert("Please enter a Start Date for Date Selector " + selectorNumber);
        return false;
    }
    else if (endDate == "") {
        alert("Please enter an End Date for Date Selector " + selectorNumber);
        return false;
    }
    else if (startDate > endDate) {
        alert("Please enter a valid date range for Date Selector " + selectorNumber + " (Start Date is currently past the End Date).");
        return false;
    }
    else if (endDate > today) {
        alert("Please re-enter the End Date for Date Selector " + selectorNumber + " (cannot enter an End Date that is in the future).");
        return false;
    }
    return true;
}


function storeDateSelectorValue(dateSelector, selectorOption, customDateStart, customDateEnd) {

    var keyword1DateSelectorCookie = new Object();
    keyword1DateSelectorCookie.dropdown = selectorOption;
    keyword1DateSelectorCookie.startDate = customDateStart;
    keyword1DateSelectorCookie.endDate = customDateEnd;
    $.cookie(dateSelector, JSON.stringify(keyword1DateSelectorCookie), { path: '/' });
}


function retrieveDateSelectorValue(dateSelector) {
    if ($.cookie(dateSelector)) {
        return $.parseJSON($.cookie(dateSelector));
    }
    else {
        var defaultValue = {};
        switch (dateSelector) {
            case "2":
                defaultValue.dropdown = "LastThreeDays";
                defaultValue.startDate = "";
                defaultValue.endDate = "";
                break;
            default:
                defaultValue.dropdown = "Yesterday";
                defaultValue.startDate = "";
                defaultValue.endDate = "";
        }
        return defaultValue;
    }
    
}

function populateDateSelectorDropdown() {

    var dateSelectorValues = retrieveDateSelectorValue("1");

    $(".dateSelectorKeywords1Dropdown").val(dateSelectorValues.dropdown);
    $(".startDateKeywords1").val(dateSelectorValues.startDate);
    $(".endDateKeywords1").val(dateSelectorValues.endDate);

    dateSelectorValues = retrieveDateSelectorValue("2");

    $(".dateSelectorKeywords2Dropdown").val(dateSelectorValues.dropdown);
    $(".startDateKeywords2").val(dateSelectorValues.startDate);
    $(".endDateKeywords2").val(dateSelectorValues.endDate);


    if ($(".dateSelectorKeywords1Dropdown").val() == "Custom") {
        $(".startDateKeywords1Container").show();
        $(".endDateKeywords1Container").show();
    }
    else {
        $(".startDateKeywords1Container").hide();
        $(".endDateKeywords1Container").hide();
    }

    if ($(".dateSelectorKeywords2Dropdown").val() == "Custom") {
        $(".startDateKeywords2Container").show();
        $(".endDateKeywords2Container").show();
    }
    else {
        $(".startDateKeywords2Container").hide();
        $(".endDateKeywords2Container").hide();
    }
}

function waitForKeywordTablesToLoad() {
    if (keywordTablesToLoadCount > 0) {
        setTimeout(waitForKeywordTablesToLoad, 500);
    }
    else {
        $('#loadingDialog').dialog('close');
        $('#loadingDialog').css('display', 'none');
    }
}
/* Advertiser Table View */

function loadAdvertiserData(redirectId){
    //http://nicolas.kruchten.com/pivottable/examples/simple_ui.html

    var parentDiv =  $('.Advertiser-' + redirectId + ':visible');

    var dateSelector = parentDiv.find(".AdvertiserDateSelector").val();
    var customStartDate = parentDiv.find(".startDateAdvertiser").val();
    var customEndDate = parentDiv.find(".endDateAdvertiser").val();

    parentDiv.find('.startDateAdvertiser').datepicker({ dateFormat: "yy-mm-dd" })
    parentDiv.find('.endDateAdvertiser').datepicker({ dateFormat: "yy-mm-dd" })

    if (dateSelector == "Custom") {
        if (customStartDate.length == 0 || customEndDate.length == 0) {
            alert("Please input valid start and end dates");
            return;
        }
    }
    $('#loadingDialog').removeAttr('style');
    $('#loadingDialog').dialog('open');

    //Ajax get Data
    $.post(
        AdvertiserJsonUrl, 
        {
            redirectId : redirectId,
            dateSelector : dateSelector,
            startDate : customStartDate,
            endDate : customEndDate
        },
        function (data) {

            $('#loadingDialog').dialog('close');
            $('#loadingDialog').css('display', 'none');


            var nrecoPivotExt = new NRecoPivotTableExtensions({
                drillDownHandler: function (dataFilter) {
                    console.log(dataFilter);

                    var filterParts = [];
                    for (var k in dataFilter) {
                        filterParts.push(k + "=" + dataFilter[k]);
                    }
                    alert(filterParts.join(", "));

                }
            });

            var stdRendererNames = ["Table", "Table Barchart", "Heatmap", "Row Heatmap", "Col Heatmap"];
            var wrappedRenderers = $.extend({}, $.pivotUtilities.renderers);
            $.each(stdRendererNames, function () {
                var rName = this;
                wrappedRenderers[rName] = nrecoPivotExt.wrapTableRenderer(wrappedRenderers[rName]);
            });            for (var rendererName in wrappedRenderers) {
                // add data export api for renderer
                var renderer = wrappedRenderers[rendererName];
                wrappedRenderers[rendererName] = nrecoPivotExt.wrapPivotExportRenderer(renderer);
            }            var refreshFunction = function (config) {
                var config_copy = JSON.parse(JSON.stringify(config));
                //delete some values which are functions
                delete config_copy["aggregators"];
                delete config_copy["renderers"];
                if (config_copy.cols.length > 0 && config_copy.rows.length > 0)
                    localStorage.setItem("pivotUISettings", JSON.stringify(config_copy));

            };            var settings = {
                rows: ["AdvertiserDomain"],
                cols: ["Rank"],
                aggregatorName: "Integer Sum",
                vals: ["ClickCount"],
                renderers: wrappedRenderers,
                rendererOptions: { sort: { direction: "desc", col_totals: true } },
                onRefresh: refreshFunction
            };
            
            if (localStorage.getItem("pivotUISettings")) {
                settings = JSON.parse(localStorage.pivotUISettings);
                settings.renderers = wrappedRenderers;
                settings.onRefresh = refreshFunction;
            }

            parentDiv.find('.advertiser-table').pivotUI(data, settings, false);

        }, "json");
}
function RestoreAdvertiserDateSettings() {
    if (localStorage.getItem("AdvertiserDateSelection")) {
        var advertiserDateSelector = JSON.parse(localStorage.AdvertiserDateSelection);
        $(".AdvertiserDateSelector").val(advertiserDateSelector.selector);
        $(".startDateAdvertiser").val(advertiserDateSelector.startDate);
        $(".endDateAdvertiser").val(advertiserDateSelector.endDate);

        if (advertiserDateSelector.selector == "Custom") {
            $(".startDateAdvertiserContainer").show();
            $(".endDateAdvertiserContainer").show();
        }
        else {
            $(".startDateAdvertiserContainer").hide();
            $(".endDateAdvertiserContainer").hide();
        }
    }
}

$(function () {
    $(".AdvertiserDateSelector").live("change", function () {

        $(".AdvertiserDateSelector").val($(this).val());
        if ($(this).val() == "Custom") {
            $(".startDateAdvertiserContainer").show();
            $(".endDateAdvertiserContainer").show();
        }
        else {
            $(".startDateAdvertiserContainer").hide();
            $(".endDateAdvertiserContainer").hide();
        }
    });

    $(".AdvertiserDateSelector, .startDateAdvertiser, .endDateAdvertiser").live("change", function () {
        var advertiserDateSelector = {};
        advertiserDateSelector.selector = $(this).parent().parent().find(".AdvertiserDateSelector").val();
        advertiserDateSelector.startDate = $(this).parent().parent().find(".startDateAdvertiser").val();
        advertiserDateSelector.endDate = $(this).parent().parent().find(".endDateAdvertiser").val();

        localStorage.setItem("AdvertiserDateSelection", JSON.stringify(advertiserDateSelector));

    });

    // export demo
    var fillExportJsonForm = function ($f, parentDiv, redirectId) {
        // getPivotExportData method is provided by nreco pivottable renderer extensions 
        var reportData = parentDiv.find('.advertiser-table .pivotExportData').data('getPivotExportData')();
        console.log(reportData);
        $f.find('input[name="reportJson"]').val(JSON.stringify(reportData));
        $f.find('input[name="redirectId"]').val(redirectId);
        $f.find('input[name="dateSelector"]').val(parentDiv.find(".AdvertiserDateSelector").val());
        $f.find('input[name="startDate"]').val(parentDiv.find(".startDateAdvertiser").val());
        $f.find('input[name="endDate"]').val(parentDiv.find(".endDateAdvertiser").val());


       /* var reportState = nrecoWebPivot.getState();
        var c = nrecoWebPivot.dataSchema.findClassById(reportState.relex_builder.class_id);
        $f.find('input[name="class_name"]').val(c != null ? c.name : "Data");
        */
    };

    $('.AdvertiserExport').live("click", function (event) {
        var redirectId = $(this).data("redirectid");
        var parentDiv = $('.Advertiser-' + redirectId + ':visible');
        var $f = parentDiv.find('.exportCsvForm');
        fillExportJsonForm($f, parentDiv, redirectId);
        $f.submit();
        event.preventDefault();
    });
   
});