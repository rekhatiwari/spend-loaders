﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;

namespace Track.Extensions
{
	public static class EnumerableExtensions
	{
		public static string ToCsv<T>(this IEnumerable<T> data) where T : class
		{
			var sb = new StringBuilder();
			using (var sw = new StringWriter(sb))
			{
				var helper = new CsvWriter(sw);
				helper.WriteRecords(data);
			}

			return sb.ToString();

		} 
	}
}
