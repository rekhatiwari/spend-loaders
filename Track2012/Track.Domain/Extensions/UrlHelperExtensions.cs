﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Track.Extensions
{
	public static class UrlHelperExtensions
	{
		public static Uri MakeAbsolute(this UrlHelper helper, string url)
		{
			if (string.IsNullOrWhiteSpace(url)) return null;

			if (!(url.StartsWith("http://") || url.StartsWith("https://")))
			{
				url = "http://" + url;
			}

			Uri uri;
			return Uri.TryCreate(url, UriKind.Absolute, out uri)
				? uri
				: null;
		}

		public static MvcHtmlString HostName(this UrlHelper helper, string url)
		{
			var uri = helper.MakeAbsolute(url);
			return new MvcHtmlString(uri == null
				? ""
				: uri.Host);
		}

	}
}