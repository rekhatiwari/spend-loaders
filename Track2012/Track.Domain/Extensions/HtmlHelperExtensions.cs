﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Track.Data;

namespace Track.Extensions
{
	public static class HtmlHelperExtensions
	{

		public static MvcHtmlString MenuActionLink(this HtmlHelper html, string linkText, string actionName, string controllerName)
		{
			var className = html.ViewContext.RouteData.Values["controller"].ToString().Equals(controllerName, StringComparison.InvariantCultureIgnoreCase)
				? "ui-state-active" 
				: "";

			var returnVal = html.ActionLink(linkText, actionName, new { controller = controllerName }, new { @class = className });
			return returnVal;
		}

		public static MvcHtmlString TruncateWithMore(this HtmlHelper html, string text, int maxLength)
		{
			if (string.IsNullOrEmpty(text) || (text ?? "").Length < maxLength) return new MvcHtmlString(text);

			var teaser = string.Format("<span>{0}</span><span id=\"a{2}\" class=\"ui-helper-hidden\">{1}</span> <a href=\"#a{2}\" toggle=\"[show less]\">[show more]</a>"
				,text.Substring(0, maxLength)
				,text.Substring(maxLength)
				,Guid.NewGuid());

			return new MvcHtmlString(teaser);
		}

        public static MvcHtmlString Image(this HtmlHelper html, byte[] image)
        {
            var img = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(image));
            return new MvcHtmlString("<img src='" + img + "' />");
        }

        public static MvcHtmlString Percent(this HtmlHelper html, decimal? percent)
        {
            if (percent == null)
                new MvcHtmlString("");

            return new MvcHtmlString(String.Format("{0:P1}", percent));
        }
    }
}