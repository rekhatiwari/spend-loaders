﻿using System;

namespace Track.Domain.Exceptions
{
    public class GhostException : Exception
    {
        public GhostException(string message) : base(message) { }
    }
}
