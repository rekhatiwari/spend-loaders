﻿using System;

namespace Track.Domain.Exceptions
{
    public class BidToolException : Exception
    {
        public BidToolException(string message) : base(message) { }
    }
}
