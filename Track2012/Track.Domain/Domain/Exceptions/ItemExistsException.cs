﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.Domain.Exceptions
{
	public class ItemExistsException : Exception
	{
		public ItemExistsException(string itemName, string format = "Item {0} already exists.") : base(string.Format(format, itemName)) { }
	}
}
