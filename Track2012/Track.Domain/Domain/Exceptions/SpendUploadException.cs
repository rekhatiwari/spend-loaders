﻿using System;

namespace Track.Domain.Exceptions
{
	public class SpendUploadException : Exception
	{
		public SpendUploadException(string message) : base(message) { }
	}
}
