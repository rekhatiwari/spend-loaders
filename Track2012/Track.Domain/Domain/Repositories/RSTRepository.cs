﻿using System;
using System.Linq;
using Track.Domain.Interfaces;
using Track.Data;
using Track.Domain.ViewModels;
using Track.Domain.Models;
using Track.Domain.Services;
using System.Collections.Generic;
using System.Data.Entity;

namespace Track.Domain.Repositories
{
    public class RSTRepository : RepositoryBase<RSTEntities>, IRSTRepository
    {
        IFileService FileService { get; set; }
        ILogService LogService { get; set; }

        public RSTRepository(IFileService file, ILogService logService)
		{
			FileService = file;
            LogService = logService;
		}

        #region Login

        //public User GetUserByUserName(string userName)
        //{
        //    //return Execute(db => db.Users.Single(o => o.userName == userName));
        //    return null;
        //}

        #endregion

        #region Ghost
        public List<int> GetAllGhostEntries()
        {
            return Execute(db =>
            {
                return db.GhostQueues.Where(s => s.Id >= 0).Select(s => s.Id).ToList();
            });
        }

        public GhostQueue GetGhostEntry(int id)
        {
            return Execute(db =>
            {
                return db.GhostQueues.Where(q => q.Id == id).FirstOrDefault();
            });
        }

        public GhostQueueViewModel GetGhostQueueForSpendAccount(int spendAccountId)
        {
            GhostQueueViewModel model = new GhostQueueViewModel()
            {
                SpendAccountId = spendAccountId
            };

            Execute(db =>
            {
                model.GhostQueue = db.GhostQueues.Where(s => s.SpendAccountId == spendAccountId).Take(7).OrderByDescending(s => s.AddedToQueue).ToList();
            });
            return model;
        }

        public void UpdateGhostRequest(int id, bool? requestSent, bool? requestFulfilled, bool? requestError)
        {
            if (requestSent.HasValue || requestFulfilled.HasValue || requestError.HasValue)
            {
                Execute(db =>
                {
                    var entry = db.GhostQueues.Where(q => q.Id == id).FirstOrDefault();
                    if (entry != null)
                    {
                        if (requestSent.HasValue)
                            entry.RequestSent = requestSent.Value;
                        if (requestFulfilled.HasValue)
                            entry.RequestFulfilled = requestFulfilled.Value;
                        if (requestError.HasValue)
                            entry.RequestError = requestError.Value;

                        db.Entry(entry).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                });
            }
        }
        #endregion

        #region SpendAccount

        public SpendAccount GetSpendAccountById(int id)
		{
            return Execute(db => db.SpendAccounts.Include("SpendProvider").Include("SpendReportType").Include("SpendStatusType").Include("SpendAccountInteractions").Include("SpendAccountMappingAliases").SingleOrDefault(o => o.Id == id));
		}

        public ASAUploadStatu CreateUploadStatus(SpendAccount account, string user, byte[] file)
        {
            var status = new ASAUploadStatu()
            {
                Id = 0,
                UserName = user,
                DateStarted = DateTime.UtcNow,
                Status = "Started",
                SpendAccountId = account.Id
            };

            Execute(db =>
            {
                db.ASAUploadStatus.Add(status);
                db.SaveChanges();

                status.FilePath = FileService.SaveFile(file, status.Id, account.Id.ToString());
                db.Entry(status).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            });

            return status;
        }

        public void SaveSpendAccount(SpendAccount model)
        {
            if (model.Id == 0) return;

            // If status is anything other than active, then we set the account to inactive
            bool isActive = true;
            if (model.SpendStatusTypeId > 1)
                isActive = false;

            Execute(db =>
            {
                var item = db.SpendAccounts.Single(o => o.Id == model.Id);

                // Test to see if the status has changed, if so add as an interaction
                if (item.SpendStatusTypeId != model.SpendStatusTypeId)
                {
                    var spendStatuses = db.SpendStatusTypes.ToList();
                    var detail = string.Format("Changed status from '{0}' to '{1}'",
                        spendStatuses.Where(i => i.Id == item.SpendStatusTypeId).Select(i => i.Name).First(),
                        spendStatuses.Where(i => i.Id == model.SpendStatusTypeId).Select(i => i.Name).First());
                    var interaction = new SpendAccountInteraction()
                    {
                        SpendAccountId = item.Id,
                        TimeStamp = DateTime.UtcNow,
                        Details = detail,
                        UserName = "System"
                    };
                    db.SpendAccountInteractions.Add(interaction);
                }

                item.SiteId = model.SiteId;
                item.ProviderId = model.ProviderId;
                item.ReportTypeId = model.ReportTypeId;
                item.Label = model.Label;
                item.Username = model.Username;
                item.Password = model.Password;
                item.Identifier = model.Identifier;
                item.ReportingEmail = model.ReportingEmail;
                item.vmID = model.vmID;
                item.TimeZone = model.TimeZone;
                item.StartedOn = model.StartedOn;
                item.Address = model.Address;
                item.City = model.City;
                item.CountryCode = model.CountryCode;
                item.Region = model.Region;
                item.PostalCode = model.PostalCode;
                item.Phone = model.Phone;
                item.NameOnAccount = model.NameOnAccount;
                item.LaunchLocation = model.LaunchLocation;
                item.LaunchIp = model.LaunchIp;
                item.CurrencyCode = model.CurrencyCode;
                item.CountryCode = model.CountryCode;
                item.CreditCardNumber = model.CreditCardNumber;
                item.CreditCardCountry = model.CreditCardCountry;
                item.CreditCardIssuingBank = model.CreditCardIssuingBank;
                item.LaunchNotes = model.LaunchNotes;
                item.OptInGoogleGhost = model.OptInGoogleGhost;
                item.AgentString = model.AgentString;
                item.GhostStartTime = model.GhostStartTime;
                item.GhostEndTime = model.GhostEndTime;
                item.ReportingEmail = model.ReportingEmail;
                item.AdditionalNotes = model.AdditionalNotes;

                item.IsActive = isActive;
                item.SpendStatusTypeId = model.SpendStatusTypeId;
                item.AlertWhenMissingSpend = model.AlertWhenMissingSpend;
                if (!String.IsNullOrEmpty(model.Token))
                    item.Token = model.Token;
                item.ObservesDST = model.ObservesDST;

                db.SaveChanges();
            });
        }

        public void UpdateUploadStatus(ASAUploadStatu asa, string message)
        {
            Execute(db =>
            {
                db.ASAUploadStatus.Attach(asa);
                asa.Status = asa.Status + "\n-----\n" + message;
                db.Entry(asa).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            });
        }

        #endregion

        public Site GetSiteById(int id)
        {
            return Execute(db => db.Sites.SingleOrDefault(o => o.SiteId == id));
        }

        public int LoadAds(List<SpendReport> ads, SpendAccount account, TimeSpan? ts)
        {

            //Set the ads UTCOffset:

            ads = ads.Select(r =>
            {
                if (r.DateOffset == null)
                {
                    DateTime.SpecifyKind(r.Date, DateTimeKind.Utc);
                    r.DateOffset = new DateTimeOffset(r.Date);
                }
                return r;
            }).ToList();

            // Handle deletion of date range we are loading for
            var min = ads.Min(a => a.DateOffset).Value.UtcDateTime;
            var max = ads.Max(a => a.DateOffset).Value.UtcDateTime;

            // Adjust for wrongful overlapped deletes
            if (ts.HasValue)
            {
                if (ts.Value.TotalMilliseconds < 0)
                    min = min.AddDays(1);
                else if (ts.Value.TotalMilliseconds > 0)
                    max = max.AddDays(-1);
            }

            Execute(db =>
            {
                db.Database.CommandTimeout = 300;  // allow five minutes

                // Delete previous data using data range being loaded for
                db.SpendReportsDeleteOld(min, max, account.Id);

                // Refresh data (or add new)
                var spendReportsAdded = false;
                foreach (var ad in ads.Where(a => a.DateOffset.Value.UtcDateTime >= min && a.DateOffset.Value.UtcDateTime <= max))
                {
                    if (account.AgencyPercent.HasValue && ad.Cost > 0)
                        ad.Cost = ad.Cost * (1 + account.AgencyPercent.Value);

                    // Stores less data, only care about reports with actual clicks/cost
                    if (ad.Cost > 0)
                    {
                        db.SpendReports.Add(ad);

                        spendReportsAdded = true;
                    }
                }

                if (spendReportsAdded)
                {
                    // Update spendaccount to be active if it is inactive and has cost
                    var accountToUpdate = db.SpendAccounts.FirstOrDefault(o => o.Id == account.Id && !o.IsActive);
                    if (accountToUpdate != null)
                    {
                        accountToUpdate.IsActive = true;
                    }
                }

                db.SaveChanges();
            });

            return ads.Count;
        }

        public void SpendReportsCurrencyConversion(int spendAccountId, List<SpendReport> adsSummarized)
        {
            if (adsSummarized == null || adsSummarized.Count == 0)
                return;

            var min = adsSummarized.Min(a => a.DateOffset).Value.UtcDateTime;
            var max = adsSummarized.Max(a => a.DateOffset).Value.UtcDateTime;
            Execute(db =>
            {
                db.Database.CommandTimeout = 500;
                db.SpendReportsCurrencyConversion(spendAccountId, min, max);
            });
        }

        public void PopulateSpendInSCMSPS(DateTime? date, int? accountId)
        {
            Execute(db =>
            {
                db.Database.CommandTimeout = 300;
                db.SpendReports_Populate_SCM_SPS(date, accountId);
            });
        }

        public void CompleteUploadStatus(ASAUploadStatu status, SpendAccount account, SimpleMailMessage message, System.Text.StringBuilder dc, bool updateStatus)
        {
            Execute(db =>
            {
                if (updateStatus && status != null)
                {
                    db.ASAUploadStatus.Attach(status);
                    status.DateCompleted = DateTime.UtcNow;
                    status.Status = message.Body.ToString() + "\n-----\n" + status.Status;
                    db.Entry(status).State = System.Data.Entity.EntityState.Modified;

                    db.SaveChanges();
                }

                List<string> emails = new List<string> { "apotabatti@genius-minds.com" };
                foreach (var email in emails)
                {
                    if (string.IsNullOrEmpty(email))
                        continue;

                    // Hack to ensure admins (apotabatti@genius-minds.com) get all the debugging output
                    if (email.Equals("apotabatti@genius-minds.com"))
                    {
                        LogService.SendEmail(email, message.Subject, message.Body.ToString() + Environment.NewLine + dc.ToString());
                    }
                    else
                    {
                        LogService.SendEmail(email, message.Subject, message.Body.ToString());
                    }
                }

            });
        }

        #region DailyJobStatus

        public void AddToDailyJobStatus(DailyJobStatu record) //Add records into dailyjobstatus
        {
            Execute(db =>
            {
                var job = db.DailyJobStatus.Where(o => o.Date == record.Date && o.AccountId == record.AccountId).FirstOrDefault();
                if (job == null)
                {
                    db.DailyJobStatus.Add(record);
                }
                else
                {
                    db.DailyJobStatus.Remove(job);
                    db.SaveChanges();
                    db.DailyJobStatus.Add(record);
                }
                db.SaveChanges();
            });
        }

        public void SendDailyStatusEmail()
        {
            Execute(db =>
            {
                string emailBody = "";
                DateTime yDate = DateTime.Now.AddDays(-1);
                var completedJobs = db.DailyJobStatus.Where(d => d.Date == yDate.Date && d.LoaderType == "Spend" && d.Completed == 1);
                var failedJobs = db.DailyJobStatus.Where(d => d.Date == yDate.Date && d.LoaderType == "Spend" && d.Completed == 0);

                emailBody += "List of Jobs Completed : " + Environment.NewLine + Environment.NewLine;
                int count = 1;
                foreach (var jobs in completedJobs)
                {
                    emailBody += count++ + "." + Environment.NewLine + "Date : " + jobs.Date.ToString("yyyy-MM-dd") + Environment.NewLine + "Spend AccountID : " + jobs.AccountId + Environment.NewLine + "Spend Provider : " + jobs.Source + Environment.NewLine + Environment.NewLine;
                }
                count = 1;
                emailBody += "List of Jobs Failed : " + Environment.NewLine + Environment.NewLine;
                foreach (var jobs in failedJobs)
                {
                    emailBody += count++ + "." + Environment.NewLine + "Date : " + jobs.Date.ToString("yyyy-MM-dd") + Environment.NewLine + "Spend AccountID : " + jobs.AccountId + Environment.NewLine + "Spend Provider : " + jobs.Source + Environment.NewLine + "Comments : " + jobs.Comments + Environment.NewLine + Environment.NewLine;
                }

                LogService.SendEmail("apotabatti@genius-minds.com", "Spend Loader Jobs Status :" + yDate.Date.ToString("yyyy-MM-dd"), emailBody);
            });
        }

        #endregion

        public List<FinstatsSummaryModel> FinstatsSummary(DateTime? fromDate, DateTime? toDate, string userId, string divisionName, string groupName, string siteName)
        {
            return Execute(db =>
            {
                return new List<FinstatsSummaryModel>();
            });
        }

    }
}
