﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using Track.Data;

namespace Track.Domain.Repositories
{
    public abstract class RepositoryBase<TObjContext> where TObjContext : DbContext, new()
	{
		public TResult Execute<TResult>(Func<TObjContext, TResult> exec)
		{
            using (var db = new TObjContext())
			{
				return exec(db);
			}
		}

		public void Execute(Action<TObjContext> exec)
		{
            using (var db = new TObjContext())
			{
				exec(db);
			}
		}

	}
}
