﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using HtmlAgilityPack;
using OpenPop.Mime.Header;
using OpenPop.Pop3;
using Track.Domain.Exceptions;
using Track.Domain.Helpers;
using Track.Domain.Interfaces;
using Track.Data;
using Track.Extensions;
using Track.Domain.Models;
using S22.Imap;
using System.Net.Mail;
using System.Net.Mime;
using System.Data;
using Excel;

namespace Track.Domain.Services
{
    public class GhostService : IGhostService
    {
        public ISpendService SpendService { get; set; }
        public IFileService FileService { get; set; }
        public ICacheService CacheService { get; set; }
        public ILogService LogService { get; set; }
        public IRSTRepository RSTRepository { get; set; }

        public static string comments = "";
        public static bool isErrorOccured = false;

        private string imapHost;
        private int imapPort;
        private bool imapSsl;
        private string imapUser;
        private string imapPassword;
        private bool imapEnableVerboseLogging;
        private int minutesBetweenGhostIterations;
        public GhostService()
        {
  
            this.imapHost = ConfigurationManager.AppSettings["Ghost.ImapHost"];
            this.imapPort = ConfigurationManager.AppSettings["Ghost.ImapPort"].ParseAs<int>();
            this.imapSsl = ConfigurationManager.AppSettings["Ghost.ImapSsl"].ParseAs<bool>();
            this.imapUser = ConfigurationManager.AppSettings["Ghost.ImapUser"];
            this.imapPassword = ConfigurationManager.AppSettings["Ghost.ImapPassword"];
            this.imapEnableVerboseLogging = ConfigurationManager.AppSettings["Ghost.ImapEnableVerboseLogging"].ParseAs<bool>();
            this.minutesBetweenGhostIterations = ConfigurationManager.AppSettings["Ghost.MinutesBetweenGhostIterations"].ParseAs<int>();

        }

        public void ProcessGhostQueueItem(GhostQueue entry)
        {
            bool serviceEntry = false;
            if (entry != null || entry.SpendAccountId.HasValue)
            {
                var ghostCounter = CacheService.GetGhostCounter();
                if (ghostCounter.ContainsKey(entry.SpendAccountId.Value))
                {
                    var item = ghostCounter[entry.SpendAccountId.Value];

                    TimeSpan customTime = new TimeSpan(11,45,0); //time frame just greater than first iteration of loaders
                    TimeSpan curTime = DateTime.UtcNow.TimeOfDay;
                    if (curTime < customTime)
                    {//invalidate cache in first iteration of day(if found)
                        item.Iterations = 0;
                        item.RequestFulfilled = null;
                    }

                    if (!(item.LastServiced.AddMinutes(this.minutesBetweenGhostIterations) <= DateTime.UtcNow))
                        return;

                    item.Iterations++;
                    item.LastServiced = DateTime.UtcNow;

                    if (item.Iterations > 10)
                    {
                        var message = string.Format("{0} has {1} processed iterations. Fullfilled: {2}", entry.SpendAccountId.Value, item.Iterations, item.RequestFulfilled);
                        LogService.WriteGhostLog(entry.SpendAccountId.Value, message);
                        LogService.SendEmail("apotabatti@genius-minds.com", "Track - Ghost.NET", message);
                    }

                    if (item.Iterations > 15)
                    {
                        //Stop Processing after 15 iterations
                        var message = string.Format("Stopped the processing of {0} after {1} iterations. Fullfilled: {2}", entry.SpendAccountId.Value, item.Iterations, item.RequestFulfilled);
                        LogService.WriteGhostLog(entry.SpendAccountId.Value, message);
                        LogService.SendEmail("apotabatti@genius-minds.com", "Track - Ghost.NET", message);

                        //Set the request to error out
                        RSTRepository.UpdateGhostRequest(entry.Id, null, null, true);
                        item.Iterations = 0;

                        return;

                    }
                    if (!item.RequestFulfilled.HasValue)
                        serviceEntry = true;
                }
                else
                {
                    GhostCounterItem item = new GhostCounterItem()
                    {
                        Iterations = 1,
                        LastServiced = DateTime.UtcNow
                    };
                    ghostCounter.Add(entry.SpendAccountId.Value, item);

                    serviceEntry = true;
                }

                if (serviceEntry)
                {
                    //populate yesterdays data
                    var date = DateTime.UtcNow.Date.AddDays(-1);
                    try
                    {
                        ProcessItem(entry, date);
                        if (isErrorOccured == false)
                        {
                            JobStatusUpdate(entry, date, null);//loader runs successfully
                        }
                        else
                        {
                            JobStatusUpdate(entry, date, comments);//loader fails
                        }
                    }
                    catch (Exception ex)
                    {
                        RSTRepository.UpdateGhostRequest(entry.Id, null, null, true);
                        LogService.WriteGhostLog(entry.SpendAccountId.Value, ex.ToString());
                        comments += ex.ToString();
                        JobStatusUpdate(entry, date, comments);//loader fails
                    }
                }
            }
        }

        public void ProcessItem(GhostQueue entry, DateTime date)
        {
            RSTRepository.UpdateGhostRequest(entry.Id, true, null, null);

            var validatedFile = false;
            byte[] reportAsBytes = null;
            isErrorOccured = false;
            comments = "";
            if (entry.ProviderId == 11)  //Yahoo Gemini
            { 
                //reportAsBytes = SpendService.RetrieveYahooGeminiSpend(entry.SpendAccountId.Value, date);
                reportAsBytes = RetrieveHexagramSpend(entry, date);
                if (reportAsBytes != null && reportAsBytes.Length > 0)
                {
                    validatedFile = true;
                }
            }
            else if (entry.ProviderId == 9)  // Outbrain
            {
                if (entry.ReportTypeId == 20)
                {
                    reportAsBytes = RetrieveHexagramSpend(entry, date);
                }
                if (reportAsBytes != null && reportAsBytes.Length > 100)
                {
                    validatedFile = true;
                }
            }
            else if (entry.ProviderId == 16)  // Taboola
            {
                reportAsBytes = SpendService.RetrieveTaboolaSpend(entry.SpendAccountId.Value, date);

                if (reportAsBytes != null && reportAsBytes.Length > 100)
                {
                    validatedFile = true;
                }
            }
            else if (entry.ProviderId == 13)  // Adblade
            {
                reportAsBytes = SpendService.RetrieveAdbladeSpend(entry.SpendAccountId.Value, date);

                if (reportAsBytes != null && reportAsBytes.Length > 100)
                {
                    validatedFile = true;
                }
            }
           
           else if (entry.ProviderId == 15)  // Advertise-Adgroup
            {
                reportAsBytes = SpendService.RetrieveAdvertiseAdgroupSpend(entry.SpendAccountId.Value, date);

                if (reportAsBytes != null && reportAsBytes.Length > 100)
                {
                    validatedFile = true;
                }
            }
           
            else if(entry.ProviderId == 17)  // RevContent
            {
                reportAsBytes = SpendService.RetrieveRevcontentSpend(entry.SpendAccountId.Value, date);

                if (reportAsBytes != null && reportAsBytes.Length > 100)
                {
                    validatedFile = true;
                }
            }
            if (validatedFile)
            {
                SpendService.UploadSpend(entry, reportAsBytes);

                // Mark account as loaded in cache
                var ghostCounter = CacheService.GetGhostCounter();
                GhostCounterItem gcEntry;
                if (ghostCounter.TryGetValue(entry.SpendAccountId.Value, out gcEntry))
                {
                    gcEntry.RequestFulfilled = DateTime.UtcNow;
                }
                else
                {
                        ghostCounter.Add(
                        entry.SpendAccountId.Value,
                        new GhostCounterItem() { Iterations = 1, LastServiced = DateTime.UtcNow, RequestFulfilled = DateTime.UtcNow });
                }
            }
        }

   
        private byte[] RetrieveHexagramSpend(GhostQueue entry, DateTime date)
        {
            var attachments = new List<Attachment>();

            //Retrieve all the attachments
            using (ImapClient client = new ImapClient(this.imapHost, this.imapPort, this.imapSsl))
            {
                if (this.imapEnableVerboseLogging)
                {
                    LogService.WriteGhostLog(entry.SpendAccountId.Value, string.Format("Attempting to login to mailbox ({0}).", this.imapUser));
                }

                try
                {
                    // Connect to the server (login)
                    client.Login(this.imapUser, this.imapPassword, AuthMethod.Auto);
                }
                catch (Exception e)
                {
                    LogService.WriteGhostLog(entry.SpendAccountId.Value, string.Format("Exception thrown when attempting to login to mailbox ({0}). Exception: {1}", this.imapUser, e.Message));
                    isErrorOccured = true;
                    comments += e.ToString();
                }

                if (this.imapEnableVerboseLogging)
                {
                    LogService.WriteGhostLog(entry.SpendAccountId.Value, "Login Successful.");
                }


                var messages = new List<uint>();

                try
                {
                    // now search for any messages sent today that were sent belonging to the current spend account
                    messages = client.Search(SearchCondition.SentSince(date.AddDays(-2)).And(SearchCondition.From("kamil.kieliszek@hexagram.com").Or(SearchCondition.From("michael@hexagram.com")))).ToList();
                }
                catch (Exception e)
                {
                    LogService.WriteGhostLog(entry.SpendAccountId.Value, string.Format("Exception thrown when retrieving messages from mailbox ({0}), from email address {1}, on {2}. Exception: {3}", this.imapUser, entry.Username, date.AddDays(1).ToShortDateString(), e.Message));
                    isErrorOccured = true;
                    comments += e.ToString();
                    return new byte[0];
                }

                foreach (var message in messages)
                {
                    MailMessage msg = new MailMessage();
                    AlternateViewCollection alternateViews = null;

                    // get alternate views to retrieve plain text emails later on
                    try
                    {
                        msg = client.GetMessage(message);
                        alternateViews = msg.AlternateViews;
                    }
                    catch (Exception e)
                    {
                        LogService.WriteGhostLog(entry.SpendAccountId.Value, string.Format("Exception thrown when retrieving message from mailbox ({0}) (looking for Outbrain msg), on {1}. Exception: {2}", this.imapUser, date.AddDays(1).ToShortDateString(), e.Message));
                        isErrorOccured = true;
                        comments += e.ToString();
                        return new byte[0];
                    }

                    var messageAttachments = msg.Attachments.Where(o => (!attachments.Select(r => r.Name).Contains(o.Name))).ToList();
                    if (messageAttachments.Count > 0)
                    {
                        attachments.AddRange(messageAttachments);
                    }
                }
            }

            //Take all the attachments, and convert them to CSV, using the filename as a date
            var stream = new MemoryStream();
            var textWriter = new StreamWriter(stream);

            //Load Outbrain-Hexagram data from email attachment
            if (entry.ReportingLabel.Contains("Outbrain - Hexagram"))
            {
                bool hasPreviousDay = attachments.Select(q => q.Name.Substring(6, 2)).Contains(DateTime.UtcNow.AddDays(-1).Day.ToString("00"));
                if (!hasPreviousDay && date.ToString("yyyy-MM-dd").Equals(DateTime.UtcNow.AddDays(-1).ToString("yyyy-MM-dd")))
                {
                    return stream.ToArray();
                }
                using (var csvWriter = new CsvHelper.CsvWriter(textWriter))
                {
                    // write header record
                    csvWriter.WriteField("Date");
                    csvWriter.WriteField("Campaign");
                    csvWriter.WriteField("Clicks");
                    csvWriter.WriteField("Cost");
                    csvWriter.WriteField("Impressions");
                    csvWriter.WriteField("CTR(%)");
                    csvWriter.NextRecord();


                    //Convert attachements to csv
                    foreach (var attachment in attachments)
                    {
                        if ((!attachment.Name.EndsWith(".xls") && !attachment.Name.EndsWith(".csv")) || attachment.Name.EndsWith("TI2.xls") || attachment.Name.EndsWith("TI1.xls"))
                            continue;


                        IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(attachment.ContentStream);
                        excelReader.IsFirstRowAsColumnNames = true;

                        DataSet dataset = excelReader.AsDataSet();

                        if (dataset == null)
                        {
                            continue;
                        }

                        foreach (DataTable table in dataset.Tables)
                        {
                            foreach (DataRow row in table.Rows)
                            {
                                if (table.Columns.Contains("Campaign"))
                                {
                                    if ((string)row["Campaign"] == "Total")
                                        continue;
                                    csvWriter.WriteField(attachment.Name.Replace(".xls", "")); //Date is the filename
                                    csvWriter.WriteField((string)row["Campaign"]); //Campaign
                                    csvWriter.WriteField((double)row["Clicks"]); //Clicks
                                    csvWriter.WriteField((double)row["Cost"]); // Result Type
                                    csvWriter.WriteField((double)row["Impressions"]); // Impressions
                                    csvWriter.WriteField((double)row["CTR(%)"]); // CTR                                
                                }
                                else if (table.Columns.Contains("Campaign Name"))
                                {
                                    if ((row["Campaign Name"].ToString().Equals("")))
                                        continue;
                                    csvWriter.WriteField(attachment.Name.Replace(".xls", "")); //Date is the filename
                                    csvWriter.WriteField(row["Campaign Name"].ToString()); //Campaign
                                    csvWriter.WriteField(Convert.ToDouble(row["Clicks"].ToString())); //Clicks
                                    csvWriter.WriteField(Convert.ToDouble(row["Amount Spent"].ToString())); // Result Type
                                    csvWriter.WriteField(Convert.ToDouble(row["Impressions"].ToString())); // Impressions
                                    csvWriter.WriteField(Convert.ToDouble(row["CTR"].ToString().Replace("%", ""))); // CTR         
                                }

                                csvWriter.NextRecord();
                            }
                        }
                    }
                }

                return stream.ToArray();
            }

            //Load Yahoogemini hexagram data from email attachment
            else if (entry.ReportingLabel.Contains("YahooGemini - Hexagram"))
            {
                using (var csvWriter = new CsvHelper.CsvWriter(textWriter))
                {
                    // Get spend account details (need the identifier [FB account id] & country of account for parsing)
                    csvWriter.WriteField("Date");
                    csvWriter.WriteField("Campaign Name");
                    csvWriter.WriteField("Clicks");
                    csvWriter.WriteField("Spend");
                    csvWriter.WriteField("Impressions");
                    csvWriter.WriteField("CTR");
                    csvWriter.NextRecord();
                    //Convert attachements to csv
                    foreach (var attachment in attachments)
                    {
                        if (attachment.Name.EndsWith(".csv"))
                        // continue;
                        {
                            try
                            {
                                using (var csv = new CsvHelper.CsvReader(new StreamReader(attachment.ContentStream)))
                                {
                                    csv.Configuration.SkipEmptyRecords = true;
                                    csv.Configuration.IsHeaderCaseSensitive = false;
                                    var datetime1 = attachment.Name.ToString().Replace("G.csv", "");
                                    var datetime2 = datetime1.Insert(4, "/");
                                    var datetime3 = datetime2.Insert(7, "/");
                                    var Date = datetime3.ToString();
                                    while (csv.Read())
                                    {
                                        csvWriter.WriteField(Date);
                                        csvWriter.WriteField(csv.GetField<string>("Campaign Name"));
                                        csvWriter.WriteField(int.Parse(csv.GetField<string>("Clicks")));
                                        csvWriter.WriteField(decimal.Parse(csv.GetField<string>("Spend")));
                                        csvWriter.WriteField(csv.GetField<string>("Impressions"));
                                        csvWriter.WriteField(String.IsNullOrWhiteSpace(csv.GetField<string>("CTR")) ? 0 : decimal.Parse(csv.GetField<string>("CTR")));

                                        csvWriter.NextRecord();

                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                e.ToString();
                            }

                        }
                    }
                }

                return stream.ToArray();
            }
            return null;
        }
          
        public void JobStatusUpdate(GhostQueue entry, DateTime date, string comments)
        {
            if (comments == null)
            { 
                RSTRepository.AddToDailyJobStatus(new DailyJobStatu()
                    {
                        Date = date,
                        AccountId = entry.SpendAccountId.Value,
                        Source = entry.ReportingLabel.ToString(),
                        LoaderType = "Spend",
                        Completed = 1,
                        Comments = null
                   });
            }
            else
            {
                RSTRepository.AddToDailyJobStatus(new DailyJobStatu()
                   {
                        Date = date,
                        AccountId =  entry.SpendAccountId.Value,
                        Source =  entry.ReportingLabel.ToString(),
                        LoaderType = "Spend",
                        Completed = 0,
                        Comments = comments
                  });
           }
      }
    }
}
