﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Track.Data;
using Track.Domain.Enums;
using Track.Domain.Exceptions;
using Track.Domain.Interfaces;
using Track.Domain.Models;
using Track.Domain.ViewModels;
using Track.Extensions;

namespace Track.Domain.Services
{
    public class ParsingService : IParsingService
    {
        public IHttpService HttpService { get; set; }
        public IFileService FileService { get; set; }


        public ParsingService(IHttpService httpService, IFileService fileService)
        {
            HttpService = httpService;
            FileService = fileService;
        }


        #region Outbrain
        
        public List<SpendReport> ParseOutbrainHexagramFile(byte[] fileBytes, SpendAccount spendAccount, out string dateRange)
        {
            var ads = new List<SpendReport>();
            dateRange = "";

            using (var sr = new StreamReader(new MemoryStream(fileBytes)))
            {
                List<OutbrainSpendReport> outbrainSpend = new List<OutbrainSpendReport>();

                var config = new CsvConfiguration()
                {
                    WillThrowOnMissingField = true
                };
                using (var csvReader = new CsvReader(sr, config))
                {
                    csvReader.Configuration.RegisterClassMap<OutbrainSpendReportMap>();
                    outbrainSpend = csvReader.GetRecords<OutbrainSpendReport>().ToList();

                    foreach (var record in outbrainSpend)
                    {
                        if (!record.Campaign.Equals("Total"))
                        {
                            var ad = new SpendReport()
                            {
                                Date = DateTime.ParseExact(record.Date, "yyyyMMdd", CultureInfo.InvariantCulture),
                                AdwordsAccount = spendAccount.Identifier,
                                AdwordsId = spendAccount.Id,
                                DomainName = "todaysinfo.net",
                                DomainId = 0,
                                Source = spendAccount.SpendProvider.ProviderName,
                                Campaign = record.Campaign.Trim(),
                                Clicks = record.Clicks.ParseAs<int>(0),
                                Impressions = record.Impressions.ParseAs<int>(0),
                                Cost = record.Cost.Replace("$", "").Replace(",", "").ParseAs<decimal>(0),
                                SiteId = spendAccount.SiteId
                            };

                            ads.Add(ad);
                        }
                    }
                }
            }

            dateRange = string.Format("{0} - {1}", ads.Select(d => d.Date).Min().ToShortDateString(), ads.Select(d => d.Date).Max().ToShortDateString());

            return ads;
        }

        #endregion


        #region Yahoo Gemini

        public List<SpendReport> ParseYahooGeminiCampaignPerformanceReport(byte[] fileBytes, SpendAccount spendAccount, out string dateRange)
        {
            var ads = new List<SpendReport>();
            dateRange = "";

            using (var sr = new StreamReader(new MemoryStream(fileBytes)))
            {
                List<YahooGeminiCampaignReport> geminiSpend = new List<YahooGeminiCampaignReport>();

                using (var csvReader = new CsvReader(sr))
                {
                    csvReader.Configuration.RegisterClassMap<YahooGeminiCampaignReportMap>();
                    geminiSpend = csvReader.GetRecords<YahooGeminiCampaignReport>().ToList();

                    foreach (var record in geminiSpend.Where(s => int.Parse(s.Impressions) > 0))
                    {
                        if (spendAccount.Id == 18471 && !record.CampaignName.Trim().Contains("HV-")) { continue; }
                        else if (spendAccount.Id == 18512 && !record.CampaignName.Trim().Contains("TV-")) { continue; }
                        else if (spendAccount.Id == 18525 && !record.CampaignName.Trim().Contains("HEX")) { continue; } //18525 - YahooGemini-Hexagram
                        var ad = new SpendReport()
                        {
                            Date = DateTime.Parse(record.Date),
                            AdwordsAccount = spendAccount.Identifier,
                            AdwordsId = spendAccount.Id,
                            DomainName = record.CampaignName.Trim().Contains("HEX") ? "todaysinfo.net" : "",
                            DomainId = 0,
                            Source = "YahooGemini",
                            Campaign = record.CampaignName.Trim(),
                            Clicks = record.Clicks.ParseAs<int>(),
                            Impressions = record.Impressions.ParseAs<int>(),
                            Cost = record.Spend.ParseAs<decimal>(),
                            SiteId = spendAccount.SiteId
                        };

                        ads.Add(ad);
                    }
                }
            }
            if (ads.Count == 0)
            {
                return new List<SpendReport>();
            }
            dateRange = string.Format("{0} - {1}", ads.Select(d => d.Date).Min().ToShortDateString(), ads.Select(d => d.Date).Max().ToShortDateString());

            return ads;
        }

        #endregion


        #region Mobile

        public List<SpendReport> ParseMobileSpend(SpendAccount account, string fileContents, out string dateRange)
        {
            if (string.IsNullOrEmpty(fileContents))
            {
                dateRange = "";
                return null;
            }

            List<MobileSpendModel> entityList = new List<MobileSpendModel>();
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(fileContents)))
            {
                using (var reader = new StreamReader(ms))
                {
                    using (var csvReader = new CsvReader(reader))
                    {
                        entityList = csvReader.GetRecords<MobileSpendModel>().ToList();
                    }
                }
            }

            if (entityList.Count > 0)
            {
                CultureInfo culture;
                switch (account.CountryCode)
                {
                    case "US":
                        // Ensure date is parsed using explicit culture (MM/dd/YYYY)
                        culture = new CultureInfo("en-US", true);
                        break;
                    case "CA":
                    case "UK":
                        // Ensure date is parsed using explicit culture (dd/MM/YYYY)
                        culture = new CultureInfo("en-GB", true);
                        break;
                    default:
                        throw new Exception(string.Format("No DateTime formatting currently exists for {0}, please contact an administrator", account.CountryCode));
                }

                List<SpendReport> report = new List<SpendReport>();
                foreach (var entity in entityList)
                {
                    SpendReport reportItem = new SpendReport()
                    {
                        Date = DateTime.Parse(entity.Day, culture),
                        SiteId = account.SiteId,
                        AdwordsAccount = account.Identifier,
                        AdwordsId = account.Id,
                        Source = "",
                        Campaign = entity.Campaign.Trim(),
                        AdGroup = "",
                        Impressions = entity.Impressions.Replace(",","").ParseAs<int>(),
                        Clicks = entity.Clicks.Replace(",","").ParseAs<int>(),
                        Cost = entity.Cost.Replace("$","").ParseAs<decimal>()
                    };
                    report.Add(reportItem);
                }

                dateRange = string.Format("{0} - {1}", report.Min(d => d.Date).ToShortDateString(), report.Max(d => d.Date).ToShortDateString());
                return report;
            }
            else
            {
                dateRange = "";
                return null;
            }
        }

        public List<SpendReport> ParseRawSpend(SpendAccount account, string fileContents, out string dateRange)
        {
            dateRange = "";

            if (string.IsNullOrEmpty(fileContents))
            {                
                return null;
            }

            var spendReports = new List<SpendReport>();
            var csv = new CsvHelper.CsvReader(new StringReader(fileContents));
            csv.Configuration.IsHeaderCaseSensitive = false;
            csv.Configuration.Delimiter = "\t";
            try
            {
                while (csv.Read())
                {
                    var spendReport = new SpendReport()
                    {
                        Date = csv.GetField<DateTime>("date"),
                        Cost = csv.GetField<decimal>("cost"),
                        Clicks = csv.GetField<int>("clicks"),
                        Source = account.SpendProvider.ProviderName,
                        SpendAccount = account, 
                        SiteId = account.SiteId,
                        AdwordsId = account.Id,
                        AdwordsAccount = account.Identifier
                    };

                    int RedirectId = 0;
                    if (csv.TryGetField<int>("redirectid", out RedirectId)){
                        spendReport.RedirectId = RedirectId;
                    }
                    spendReports.Add(spendReport);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Raw Spend - Error parsing data: " + e.Message);
            }

            if (spendReports.Count == 0)
            {
                return new List<SpendReport>();
            }

            dateRange = string.Format("{0} - {1}", spendReports.Select(d => d.Date).Min().ToShortDateString(), spendReports.Select(d => d.Date).Max().ToShortDateString());

            //return new List<SpendReport>();
            return spendReports;
        }
        

        #endregion
        
        #region Taboola
        public List<SpendReport> ParseTaboolaFile(byte[] fileAsBytes, SpendAccount account, out string dateRange)
        {
            var ads = new List<SpendReport>();
            dateRange = "";

            using (var sr = new StreamReader(new MemoryStream(fileAsBytes)))
            {
                var taboolaSpend = new List<TaboolaSpendReport>();

                using (var csvReader = new CsvReader(sr))
                {
                    csvReader.Configuration.RegisterClassMap<TaboolaSpendReportMap>();
                    taboolaSpend = csvReader.GetRecords<TaboolaSpendReport>().ToList();

                    foreach (var record in taboolaSpend.Where(s => s.Clicks > 0))
                    {
                        var ad = new SpendReport()
                        {
                            Date = record.Date,
                            AdwordsAccount = account.Identifier,
                            AdwordsId = account.Id,
                            DomainName = "",
                            DomainId = 0,
                            Source = "Taboola",
                            Clicks = record.Clicks,
                            Impressions = record.Impressions,
                            Cost = record.Spend,
                            SiteId = account.SiteId,
                            Campaign = record.Campaign.Trim(),
                        };
                        ads.Add(ad);
                    }
                }
            }
           
            if (ads.Count == 0)
            {
                return new List<SpendReport>();
            }


            dateRange = string.Format("{0} - {1}", ads.Select(d => d.Date).Min().ToShortDateString(), ads.Select(d => d.Date).Max().ToShortDateString());

            return ads;
        }
        #endregion

        #region Adblade
        public List<SpendReport> ParseAdbladeFile(byte[] fileAsBytes, SpendAccount account, out string dateRange)
        {
            var ads = new List<SpendReport>();
            dateRange = "";

            using (var sr = new StreamReader(new MemoryStream(fileAsBytes)))
            {
                var adbladespend = new List<AdbladeSpendReport>();

                using (var csvReader = new CsvReader(sr))
                {
                    csvReader.Configuration.RegisterClassMap<AdbladeSpendReportMap>();
                    //csvReader.Configuration.WillThrowOnMissingField = false;
                    adbladespend = csvReader.GetRecords<AdbladeSpendReport>().ToList();

                    foreach (var record in adbladespend.Where(s => s.Clicks > 0))
                    {
                        string domain = record.Campaign.IndexOf("TI") >= 0 ? "todaysinfo.net" : "";
                        if (string.IsNullOrEmpty(record.Campaign.ToString())) { continue; }

                        var ad = new SpendReport()
                        {
                            Date = record.Date,
                            AdwordsAccount = account.Identifier,
                            AdGroup = "",
                            AdwordsId = account.Id,
                            DomainName = domain,
                            DomainId = 0,
                            Source = "Adblade",
                            Clicks = record.Clicks,
                            Impressions = record.Impressions,
                            Cost = record.Spend,
                            SiteId = account.SiteId,
                            Campaign = record.Campaign.Trim(),
                        };
                        ads.Add(ad);
                    }
                }
            }
            if (ads.Count == 0)
            {
                return new List<SpendReport>();
            }


            dateRange = string.Format("{0} - {1}", ads.Select(d => d.Date).Min().ToShortDateString(), ads.Select(d => d.Date).Max().ToShortDateString());

            return ads;
        }
        #endregion

        #region Revcontent
        public List<SpendReport> ParseRevcontentFile(byte[] fileAsBytes, SpendAccount account, out string dateRange)
        {
            var ads = new List<SpendReport>();
            dateRange = "";

            using (var sr = new StreamReader(new MemoryStream(fileAsBytes)))
            {
                var revcontentSpend = new List<RevcontentSpendReport>();

                using (var csvReader = new CsvReader(sr))
                {
                    csvReader.Configuration.RegisterClassMap<RevcontentSpendReportMap>();
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    revcontentSpend = csvReader.GetRecords<RevcontentSpendReport>().ToList();

                    foreach (var record in revcontentSpend.Where(s => s.Clicks > 0))
                    {
                        string domain = "todaysinfo.net";
                        if (string.IsNullOrEmpty(record.Campaign.ToString())) { continue; }

                        var ad = new SpendReport()
                        {
                            Date = record.Date,
                            AdwordsAccount = account.Identifier,
                            AdGroup = "",
                            AdwordsId = account.Id,
                            DomainName = domain,
                            DomainId = 0,
                            Source = "Revcontent",
                            Clicks = record.Clicks,
                            Impressions = record.Impressions,
                            Cost = record.Spend,
                            SiteId = account.SiteId,
                            Campaign = record.Campaign.Trim(),
                        };
                        ads.Add(ad);
                    }
                }
            }
            if (ads.Count == 0)
            {
                return new List<SpendReport>();
            }

            dateRange = string.Format("{0} - {1}", ads.Select(d => d.Date).Min().ToShortDateString(), ads.Select(d => d.Date).Max().ToShortDateString());

            return ads;
        }
        #endregion

        #region AdvertiseAdgroup
        public List<SpendReport> ParseAdvertiseAdgroupFile(byte[] fileAsBytes, SpendAccount account, out string dateRange)
        {
            var ads = new List<SpendReport>();
            dateRange = "";

            using (var sr = new StreamReader(new MemoryStream(fileAsBytes)))
            {
                var advertise = new List<AdvertiseAdgroupSpendReport>();

                using (var csvReader = new CsvReader(sr))
                {
                    csvReader.Configuration.RegisterClassMap<AdvertiseAdgroupSpendReportMap>();
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    advertise = csvReader.GetRecords<AdvertiseAdgroupSpendReport>().ToList();

                    foreach (var record in advertise.Where(s => s.Clicks > 0))
                    {
                        string domain = "todaysinfo.net";
                        if (record.Campaign.Contains("Summaries")) { continue; }
                    
                        var ad = new SpendReport()
                        {
                            Date = record.Date,
                            AdwordsAccount = account.Identifier,
                            AdGroup = "",
                            AdwordsId = account.Id,
                            DomainName = domain,
                            DomainId = 0,
                            Source = "Advertise",
                            Clicks = record.Clicks,
                            Cost = record.Spend,
                            SiteId = account.SiteId,
                            Campaign = record.Campaign.Trim(),
                        };
                        ads.Add(ad);
                    }
                }
            }
            if (ads.Count == 0)
            {
                return new List<SpendReport>();
            }


            dateRange = string.Format("{0} - {1}", ads.Select(d => d.Date).Min().ToShortDateString(), ads.Select(d => d.Date).Max().ToShortDateString());

            return ads;
        }
        #endregion



    }
}
