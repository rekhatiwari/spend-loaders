﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Track.Domain.Enums;
using Track.Domain.Exceptions;
using Track.Domain.Interfaces;
using Track.Data;
using Track.Domain.ViewModels;
using Track.Domain.Models;
using System.Data.Entity.Core;
using Track.Data.Common;
using Newtonsoft.Json;
using Track.Extensions;
using System.Net;
using Track.Domain.Helpers;
using System.Web;
using System.Collections.Specialized;

namespace Track.Domain.Services
{
	public class SpendService : ISpendService
	{
        public IRSTRepository RSTRepository { get; set; }

        public IFileService FileService { get; set; }
		public IHttpService HttpService { get; set; }
		public ILogService LogService { get; set; }
        public IParsingService ParsingService { get; set; }
        public IYahooGeminiService YahooGeminiService { get; set; }
        
        public void UploadSpend(UploadSpendModel model)
        {
            if (model.File == null) return;
            else if (model.SpendAccountId == 0)
            {
                throw new SpendUploadException("SpendAccountId must be greater than 0.");
            }
           
            var spendAccount = RSTRepository.GetSpendAccountById(model.SpendAccountId);
            if (spendAccount == null)
            {
                throw new SpendUploadException("Invalid SpendAccountId.");
            }

           if (spendAccount.SpendProvider.ProviderName.Equals("Outbrain", StringComparison.InvariantCultureIgnoreCase))
            {
                uploadSpend(spendAccount, ReportProviders.Outbrain, model);
            }
            else if (spendAccount.SpendProvider.ProviderName.Equals("Yahoo Gemini", StringComparison.InvariantCultureIgnoreCase))
            {
                uploadSpend(spendAccount, ReportProviders.YahooGemini, model);
            }
         
            else
            {
                throw new SpendUploadException("The system does not currently support loading spend for " + spendAccount.SpendProvider.ProviderName);
            }
        }

        public void UploadSpend(GhostQueue entry, byte[] fileAsBytes)
        {
            if (!entry.SpendAccountId.HasValue || entry.SpendAccountId <= 0)
                throw new SpendUploadException("SpendAccountId must be greater than 0.");
            else if (fileAsBytes == null || fileAsBytes.Length == 0)
                throw new SpendUploadException("Must specify a file to upload");

            var spendAccount = RSTRepository.GetSpendAccountById(entry.SpendAccountId.Value);

            string fileContents;
            ReportProviders provider;
            
            if (spendAccount.SpendProvider.ProviderName.Equals("Outbrain", StringComparison.InvariantCultureIgnoreCase))
            {
                provider = ReportProviders.Outbrain;
                fileContents = System.Text.Encoding.UTF8.GetString(fileAsBytes);
            }
            else if (spendAccount.SpendProvider.ProviderName.Equals("Taboola", StringComparison.InvariantCultureIgnoreCase))
            {
                provider = ReportProviders.Taboola;
                fileContents = System.Text.Encoding.UTF8.GetString(fileAsBytes);
            }
            else if (spendAccount.SpendProvider.ProviderName.Equals("Adblade", StringComparison.InvariantCultureIgnoreCase))
            {
                provider = ReportProviders.Adblade;
                fileContents = System.Text.Encoding.UTF8.GetString(fileAsBytes);
            }
            else if (spendAccount.SpendProvider.ProviderName.Equals("Yahoo Gemini", StringComparison.InvariantCultureIgnoreCase))
            {
                provider = ReportProviders.YahooGemini;
                fileContents = System.Text.Encoding.UTF8.GetString(fileAsBytes);
            }
            else if (spendAccount.SpendProvider.ProviderName.Equals("Advertise", StringComparison.InvariantCultureIgnoreCase))
            {
                provider = ReportProviders.AdvertiseAdgroup;
                fileContents = System.Text.Encoding.UTF8.GetString(fileAsBytes);
            }

            else if (spendAccount.SpendProvider.ProviderName.Equals("Revcontent", StringComparison.InvariantCultureIgnoreCase))
            {
                provider = ReportProviders.Revcontent;
                fileContents = System.Text.Encoding.UTF8.GetString(fileAsBytes);
            }
            else
                throw new SpendUploadException("The system does not currently support loading spend for " + spendAccount.SpendProvider.ProviderName);

            ProcessUpload(fileContents,
                            provider,
                            spendAccount,
                            HttpService.Context.User.Identity.Name,
                            fileAsBytes,
                            entry.Id);
        }

        public List<Exception> uploadRawSpend(string contents, int spendAccountId)
        {
            var spendAccount = RSTRepository.GetSpendAccountById(spendAccountId);

            return LockAndLoad(contents, ReportProviders.Raw, spendAccount, HttpService.Context.User.Identity.Name, System.Text.Encoding.UTF8.GetBytes(contents), null);
            
        }

        private void uploadSpend(SpendAccount spendAccount, ReportProviders provider, UploadSpendModel model)
		{
			string fileContents;
            byte[] fileAsBytes = null;

			switch (provider)
			{
				case ReportProviders.Google:
                    if (model.File.FileName.EndsWith(".zip"))
                        fileContents = FileService.Unzip(model.File.InputStream, 1, null);
                    else if (model.File.FileName.EndsWith(".gz"))
                        fileContents = FileService.GUnzip(model.File.InputStream);
                    else
                        throw new SpendUploadException("Google spend file must be zipped or gzipped");
                    break;
				
                case ReportProviders.Outbrain:
                    if (!model.File.FileName.EndsWith(".csv"))
                        throw new SpendUploadException("Outbrain spend file must be in csv format");
                    fileAsBytes = FileService.ReadAllBytes(model.File.InputStream);
                    fileContents = "";
                    break;
                case ReportProviders.YahooGemini:
                    if (!model.File.FileName.EndsWith(".csv"))
                        throw new SpendUploadException("Yahoo Gemini spend file must be in csv format");
                    else
                        fileAsBytes = FileService.ReadAllBytes(model.File.InputStream);
                        fileContents = "";  // only need bytes for Facebook
                    break;
                case ReportProviders.Taboola:
                    fileAsBytes = FileService.ReadAllBytes(model.File.InputStream);
                    fileContents = FileService.ConvertBytesToString(fileAsBytes, System.Text.Encoding.UTF8);
                    break;
                case ReportProviders.Adblade:
                    fileAsBytes = FileService.ReadAllBytes(model.File.InputStream);
                    fileContents = FileService.ConvertBytesToString(fileAsBytes, System.Text.Encoding.UTF8);
                    break;
                case ReportProviders.AdvertiseAdgroup:
                    fileAsBytes = FileService.ReadAllBytes(model.File.InputStream);
                    fileContents = FileService.ConvertBytesToString(fileAsBytes, System.Text.Encoding.UTF8);
                    break;
                default:
					throw new SpendUploadException(string.Format("Loading spend for '{0}' is not supported.", provider));
			}

			if (fileContents.StartsWith("Error", StringComparison.InvariantCultureIgnoreCase))
			{
				throw new SpendUploadException(fileContents);
			}

            if (fileAsBytes == null)
                fileAsBytes = FileService.ReadAllBytes(model.File.InputStream);

			ProcessUpload(fileContents,
				provider,
				spendAccount,
				HttpService.Context.User.Identity.Name,
                fileAsBytes,
                model.GhostQueueId);

			//NotificationService.Add(NotificationLevel.Info, "File is being processed, an email will be sent when it completes");
		}
        
        protected void ProcessUpload(string fileContents, ReportProviders provider, SpendAccount spendAccount, string user, byte[] fileAsBytes, int? ghostQueueId = null)
		{
#if DEBUG
            LockAndLoad(fileContents, provider, spendAccount, user, fileAsBytes, ghostQueueId);
#else
            System.Threading.Tasks.Task.Factory.StartNew(() => LockAndLoad(fileContents, provider, spendAccount, user, fileAsBytes, ghostQueueId), System.Threading.Tasks.TaskCreationOptions.LongRunning);
#endif
        }

        protected List<Exception> LockAndLoad(string fileContents, ReportProviders provider, SpendAccount account, string user, byte[] fileAsBytes, int? ghostQueueId = null)
		{
			var errors = new List<Exception>();
            var warnings = new List<Exception>();
			ASAUploadStatu status = null;
			TimeSpan? accountsUTCOffset = null;
			var dateRange = string.Empty;
			var adsTotalCount = 0;

            var debugInfo = new StringBuilder();
			var stopwatchJob = new Stopwatch();
			stopwatchJob.Start();
            try
            {
                // Create new status entry (indicates job has started)
                status = RSTRepository.CreateUploadStatus(account, user, fileAsBytes);

                // Sanity check
                if (account == null)
                    throw new SpendUploadException("Spend account does not exist in our system, therefore cannot map up accountid, siteid and related redirect domains, load aborted");

                // Handle timezone apportioning
                if (account.TimezoneApportioningIsEnabled.HasValue && account.TimezoneApportioningIsEnabled.Value && provider != ReportProviders.Google)
                    throw new SpendUploadException("Advanced timezone apportioning is currently only supported on Google (Adwords) spend accounts, load aborted");
             
                else
                {
                    // Normal flow -> Parse file
                    var ads = new List<SpendReport>();
                   if (provider == ReportProviders.Outbrain)
                    {
                        if (account.SpendReportType.Type.Equals("Outbrain - Campaign Report"))
                        {
                            //ads = ParsingService.ParseOutbrainCampaignFile(fileAsBytes, account, out dateRange);
                        }
                        if (account.SpendReportType.Type.Equals("Outbrain - Hexagram"))
                        {
                            ads = ParsingService.ParseOutbrainHexagramFile(fileAsBytes, account, out dateRange);
                        }
                    }
               
                    else if (provider == ReportProviders.YahooGemini)
                    {
                        ads = ParsingService.ParseYahooGeminiCampaignPerformanceReport(fileAsBytes, account, out dateRange);
                    }
              
                    else if (provider == ReportProviders.Taboola)
                    {
                        ads = ParsingService.ParseTaboolaFile(fileAsBytes, account, out dateRange);
                    }
                    else if (provider == ReportProviders.Adblade)
                    {
                        ads = ParsingService.ParseAdbladeFile(fileAsBytes, account, out dateRange);
                    }
                    else if (provider == ReportProviders.Revcontent)
                    {
                        ads = ParsingService.ParseRevcontentFile(fileAsBytes, account, out dateRange);
                    }
                    else if (provider == ReportProviders.AdvertiseAdgroup)
                    {
                        ads = ParsingService.ParseAdvertiseAdgroupFile(fileAsBytes, account, out dateRange);
                    }
                   
                    else
                        throw new SpendUploadException(string.Format("{0} is not currently supported for spend integration", provider.ToString()));

                    RSTRepository.UpdateUploadStatus(status, "Additional tracing: Parse*() completed");

                    //Set the ads UTCOffset if the loader didnt
                    ads = ads.Select(r => {
                        if (r.DateOffset == null)
                        {
                            DateTime.SpecifyKind(r.Date, DateTimeKind.Utc);
                            r.DateOffset = new DateTimeOffset(r.Date, new TimeSpan());
                        }
                        return r;
                    }).ToList();


                   ads = UpdateCastleDestinationDomain(ads, account);

                    // Get spend assigned to rev domains (using redirect rules), required for SPS
                    List<SpendReport> masterSpend = new List<SpendReport>();

                 
                        masterSpend = ads;
                 

                    RSTRepository.UpdateUploadStatus(status, "Additional tracing: GetSpend() completed");

                    // Sum hourly data into a daily total, per account/domain/campaign/adgroup
                    List<SpendReport> adsSummarized;
                    if (account.SpendReportType.Type.ToLower().Contains("hourly"))
                        adsSummarized = masterSpend;
                    else
                        adsSummarized = AggregateSpend(masterSpend);
                    RSTRepository.UpdateUploadStatus(status, "Additional tracing: AggregateSpend() completed");

                    // Remove spend with dates in the future (Qool fail-safe)
                    if (adsSummarized.Where(x => x.DateOffset >= DateTime.UtcNow).Count() >= 1)
                    {
                        warnings.Add(new Exception("Spend file had entries whose dates landed on today or in the future. These have been removed and not loaded."));
                        adsSummarized.RemoveAll(x => x.Date >= DateTime.UtcNow.Date);
                    }

                    // Load ads to db
                    if (adsSummarized.Count > 0)
                    {
                        adsTotalCount = RSTRepository.LoadAds(adsSummarized, account, accountsUTCOffset);
                    }
                    RSTRepository.UpdateUploadStatus(status, "Additional tracing: LoadAds() completed");

                    // Apply currency conversion as needed
                    //RSTRepository.SpendReportsCurrencyConversion(account.Id, adsSummarized);
                    RSTRepository.UpdateUploadStatus(status, "Additional tracing: SpendReportsCurrencyConversion() completed");

             
                    //Populate SCM_SPS
                    List<int> SCMSiteids = new List<int>{ 11271 };
                    if (SCMSiteids.Contains(account.SiteId))
                    {
                        if (ads.Count > 0)
                        {
                            var date = ads.Max(a => a.DateOffset).Value.Date;
                            RSTRepository.PopulateSpendInSCMSPS(date, account.Id);
                        }                       
                    }
                    stopwatchJob.Stop();
               }
            }
            catch (Exception ex)
            {
                errors.Add(ex.InnerException != null ? ex.InnerException : ex);
                errors.Add(ex);
                GhostService.isErrorOccured = true;
                GhostService.comments += ex.ToString();
            }

			// Update status entry (include message) and mark as completed
            var message = constructEmailMessage(errors, warnings, user, stopwatchJob, dateRange, adsTotalCount, account);
            
            RSTRepository.CompleteUploadStatus(status, account, message, debugInfo, true);

            if (ghostQueueId.HasValue)
                RSTRepository.UpdateGhostRequest(ghostQueueId.Value, null, true, null);

            return errors;
          

        }

    

        #region Email

        protected static SimpleMailMessage constructEmailMessage(List<Exception> errors, List<Exception> warnings, string user, Stopwatch stopwatch, string dateRange, int adsTotalCount, SpendAccount account)
		{
            var subject = "Track - ASA Upload [{0}] - {1} ({2})";
			var body = new StringBuilder();
            if (errors.Count > 0)
            {
                subject = string.Format(subject, "Error", account.ReportingLabel, account.Id);

                body.AppendLine("Failure");
                body.AppendLine("User: " + user);
                body.AppendLine("Running time: " + Math.Round(stopwatch.Elapsed.TotalSeconds, 1).ToString() + " seconds");
                body.AppendLine("Spend account: " + account.ReportingLabel + " (" + account.Id.ToString() + ")");
                body.AppendLine();

                foreach (var e in errors.Select(s => (s.Message + "::" + s.StackTrace)).Distinct())
                {
                    body.AppendLine(e);
                }
                foreach (var e in warnings.Select(s => s.Message).Distinct())
                {
                    body.AppendLine(e);
                }
            }
            else if (warnings.Count > 0)
            {
                subject = string.Format(subject, "Warning", account.ReportingLabel, account.Id);

                body.AppendLine("Warning");
                body.AppendLine("User: " + user);
                body.AppendLine("Running time: " + Math.Round(stopwatch.Elapsed.TotalSeconds, 1).ToString() + " seconds");
                body.AppendLine("Spend account: " + account.ReportingLabel + " (" + account.Id.ToString() + ")");
                body.AppendLine();

                foreach (var e in warnings.Select(s => s.Message).Distinct())
                {
                    body.AppendLine(e);
                }
            }
            else
            {
                subject = string.Format(subject, "Success", account.ReportingLabel, account.Id);

                body.AppendLine("Success");
                body.AppendLine("User: " + user);
                body.AppendLine("Running time: " + Math.Round(stopwatch.Elapsed.TotalSeconds, 1).ToString() + " seconds");
                body.AppendLine("Date(s): " + dateRange);
                body.AppendLine("Total added: " + adsTotalCount);
                body.AppendLine("Spend account: " + account.ReportingLabel + " (" + account.Id.ToString() + ")");
            }

            var mailMessage = new SimpleMailMessage()
            {
                Subject = subject,
                Body = body
            };
            return mailMessage;
		}

        #endregion

        #region Logic Decisions

   

        protected List<SpendReport> UpdateCastleDestinationDomain(List<SpendReport> ads, SpendAccount spendAccount)
        {
            if (spendAccount.SiteId > 0 && ads != null && ads.Count > 0 )
            {
                var site = RSTRepository.GetSiteById(spendAccount.SiteId).Name.ToLower();
                if (site.Contains(" "))
                {
                    site = site.Split(new char[] { ' ' })[0];
                }
                return ads.Select(e => {
                    if (e.DestinationDomain == null || e.DestinationDomain.ToLower() == "castle.com" || e.DestinationDomain.ToLower() == "briefly.com")
                        e.DestinationDomain = site;
                    return e;
                }).ToList();
            }
            return ads;
        }

        protected List<SpendReport> AggregateSpend(List<SpendReport> masterSpend)
		{
			var ads = masterSpend
					.GroupBy(a => new
					{
						a.Date.Date,
						a.SiteId,
						a.AdwordsAccount,
						a.AdwordsId,
						a.DomainName,
						a.DomainId,
						a.Source,
						a.SourceType,
						a.Campaign,
                        a.NativeCampaignName,
						a.AdGroup,
                        a.NativeAdgroupName,
						a.Keyword,
						a.MatchTypeId,
                        a.DestinationUrl,
                        a.DestinationDomain,
                        a.RedirectId
					})
					.Select(group => new SpendReport
					{
						Date = group.Key.Date,
						SiteId = group.Key.SiteId,
						AdwordsAccount = group.Key.AdwordsAccount,
						AdwordsId = group.Key.AdwordsId,
						DomainName = group.Key.DomainName,
						DomainId = group.Key.DomainId,
						Source = group.Key.Source,
						SourceType = group.Key.SourceType,
						Campaign = group.Key.Campaign,
                        NativeCampaignName = group.Key.NativeCampaignName,
						AdGroup = group.Key.AdGroup,
                        NativeAdgroupName = group.Key.NativeAdgroupName,
						Keyword = group.Key.Keyword,
						MatchTypeId = group.Key.MatchTypeId,
                        DestinationUrl = group.Key.DestinationUrl,
                        DestinationDomain = group.Key.DestinationDomain,
						Clicks = group.Sum(a => a.Clicks),
						Impressions = group.Sum(a => a.Impressions),
						Cost = group.Sum(a => a.Cost),
						AverageCPC = group.Average(a => a.AverageCPC),
						AveragePosition = group.Average(a => a.AveragePosition),
						AverageCPM = group.Average(a => a.AverageCPM),
                        RedirectId = group.Key.RedirectId,
                        Bid = group.Average(a=>a.Bid),
                        Budget = group.Sum(a=>a.Budget)
					}).ToList();

            //Set the ads UTCOffset if they haven't yet
            ads = ads.Select(r => {
                if (r.DateOffset == null)
                {
                    DateTime.SpecifyKind(r.Date, DateTimeKind.Utc);
                    r.DateOffset = new DateTimeOffset(r.Date, new TimeSpan());
                }
                return r;
            }).ToList();

            return ads;
        }

		#endregion


        #region Facebook
        
        public byte[] RetrieveYahooGeminiSpend(int spendAccountId, DateTime date)
        {
            var spendReportCSV = YahooGeminiService.RetrieveSpend(date, spendAccountId);
            return System.Text.Encoding.UTF8.GetBytes(spendReportCSV);
        }

        public byte[] RetrieveTaboolaSpend(int spendAccountId, DateTime date)
        {
            var stream = new MemoryStream();
            var textWriter = new StreamWriter(stream);
            using (var csvWriter = new CsvHelper.CsvWriter(textWriter))
            {
                // Get spend account details (need the identifier [FB account id] & country of account for parsing)
                var spendAcct = RSTRepository.GetSpendAccountById(spendAccountId);

                // write header record
                csvWriter.WriteField("Date");
                csvWriter.WriteField("Campaign");
                csvWriter.WriteField("Impressions");
                csvWriter.WriteField("Clicks");
                csvWriter.WriteField("Actions");
                csvWriter.WriteField("Spend");
                csvWriter.NextRecord();

                using (WebClient client = new CGWebClient())
                {
                    var response = client.DownloadString("https://backstage.taboola.com/backstage/");

                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.Load(new MemoryStream(client.DownloadData("https://backstage.taboola.com/backstage/")));
                

                    var csrf = doc.DocumentNode.SelectSingleNode("//input[@type='hidden' and @name='_csrf']").Attributes["value"].Value;
                    var sig = doc.DocumentNode.SelectSingleNode("//input[@type='hidden' and @name='sig']").Attributes["value"].Value;

                    //Log in
                    var parameters = new NameValueCollection();
                    parameters.Add("serverTime", Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds).ToString());
                    parameters.Add("j_username", spendAcct.Username);
                    parameters.Add("j_password", spendAcct.Password);
                    parameters.Add("_spring_security_remember_me", "true");
                    parameters.Add("_csrf", csrf);
                    parameters.Add("sig", sig);
                    string identifier = spendAcct.Identifier;


                    client.UploadValues("https://backstage.taboola.com/backstage/j_spring_security_check", parameters);

                    var campaignReport = client.DownloadString(String.Format("https://backstage.taboola.com/backstage/transform/" + identifier +"/campaigns/campaign-summary/csv?currentDimension=campaign_breakdown&marker&id=wq4bx01fhoal&groupId=campaigns&reportId=campaign-summary&dateStart={0}&dateEnd{0}&dateRangeValue=0&term={0}%2000%3A00%3A00%20to%20{0}%2023%3A59%3A59&queryFilter=%5B%7B%22id%22%3A%22campaign_param%22%2C%22operator%22%3A%22equal%22%2C%22value%22%3A%22-1%22%7D%5D", date.ToString("yyyy-MM-dd")));
                    String csv = campaignReport.ToString();                   
                    if (String.IsNullOrEmpty(csv))
                    {                      
                        return null;                      
                    }
                    var csvReader = new CsvHelper.CsvReader(new StringReader(campaignReport));
                    csvReader.Configuration.IsHeaderCaseSensitive = false;
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    csvReader.Configuration.RegisterClassMap<TaboolaSpendReportMap>();
                                   
                    var items = csvReader.GetRecords<TaboolaSpendReport>().ToList();
                    foreach (var item in items)
                    {
                        if (spendAcct.Identifier == "1021754" && item.Campaign.ToUpper().Contains("HEX"))
                        {
                            item.Spend = item.Spend + (item.Spend * (Convert.ToDecimal(12.5) / 100));
                        }
                        item.Date = date;
                        csvWriter.WriteRecord(item);
                    }                       
                }
            }

            return stream.ToArray();

            }
        public byte[] RetrieveAdbladeSpend(int spendAccountId, DateTime date)
        {
            var stream = new MemoryStream();
            var textWriter = new StreamWriter(stream);
            using (var csvWriter = new CsvHelper.CsvWriter(textWriter))
            {
                // write header record
                csvWriter.WriteField("Date");
                csvWriter.WriteField("Campaign Name");
                csvWriter.WriteField("Views");
                csvWriter.WriteField("Clicks");
                csvWriter.WriteField("Total Spend");
                csvWriter.NextRecord();
                // Get spend account details (need the identifier [FB account id] & country of account for parsing)
                var spendAcct = RSTRepository.GetSpendAccountById(spendAccountId);
                using (WebClient client = new CGWebClient())
                {
                    //Log in
                    var parameters = new NameValueCollection();
                    parameters.Add("email", spendAcct.Username);
                    parameters.Add("password", "riverrich1");
                    parameters.Add("remember", "true");


                    client.UploadValues("https://www.adblade.com/control/login", parameters);
                    var campaignReport = client.DownloadString(String.Format("https://www.adblade.com/ads/getcsv?status=&start={0}&end={0}&adWithImps=&adGroup=&totals=1", date.ToString("yyyy-MM-dd")));

                    String csv = campaignReport.ToString();
                    if (String.IsNullOrEmpty(csv))
                    {
                        return null;
                    }
                    var csvReader = new CsvHelper.CsvReader(new StringReader(campaignReport));
                    csvReader.Configuration.IsHeaderCaseSensitive = false;
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    csvReader.Configuration.RegisterClassMap<AdbladeSpendReportMap>();

                    var items = csvReader.GetRecords<AdbladeSpendReport>().ToList();
                    foreach (var item in items)
                    {

                        item.Date = date;
                        csvWriter.WriteRecord(item);
                    }
                }
            }


            return stream.ToArray();

        }

        public byte[] RetrieveAdvertiseAdgroupSpend(int spendAccountId, DateTime date)
        {
            var stream = new MemoryStream();
            var textWriter = new StreamWriter(stream);
            string Date = date.ToString("MM-dd-yyyy");


            using (var csvWriter = new CsvHelper.CsvWriter(textWriter))
            {
                // write header record
                csvWriter.WriteField("Date");
                csvWriter.WriteField("Campaign Name");
                csvWriter.WriteField("Total Clicks");
                csvWriter.WriteField("Spent");
                csvWriter.NextRecord();

                // Get spend account details (need the identifier [FB account id] & country of account for parsing)
                var spendAcct = RSTRepository.GetSpendAccountById(spendAccountId);

                CGWebClient client = new CGWebClient();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://admin.advertise.com/ads-webapp/login/login.json");
                httpWebRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"userName\":\"todaysinfo \"," +
                                     "\"password\":\"riverrich\"}";
                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string[] cookie = httpResponse.Headers["Set-Cookie"].Split(';');
                string JSESSIONIDCookie = "";
                foreach (string c in cookie)
                {
                    if (c.Contains("JSESSIONID"))
                    {
                        JSESSIONIDCookie = c;
                        break;
                    }
                }

                httpWebRequest = (HttpWebRequest)WebRequest.Create("https://admin.advertise.com/ads-webapp/run_report_exl.json");
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Cookie:" + JSESSIONIDCookie + ";");
                var boundary = "------WebKitFormBoundaryTMIHcDkoyl6Xt3SK";
                var newLine = Environment.NewLine;
                var propFormat = boundary + newLine +
                                "Content-Disposition: form-data; name=\"i\"" + newLine + newLine;
                httpWebRequest.ContentType = "multipart/form-data; boundary=----WebKitFormBoundaryTMIHcDkoyl6Xt3SK";
                httpWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
               
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    string json = propFormat + "{\"start\":0,\"pageSize\":99999,\"sortColumn\":\"\",\"sortOrder\":\"ASC\",\"filters\":[],\"reportId\":11,\"startDate\":\"" + Date + " 00:00:00\",\"endDate\":\"" + Date + " 23:59:59\",\"mode\":\"excel\"}" + newLine + boundary + "--";
                    streamWriter.Write(json);

                }
                var csv = "";
                try
                {
                    var httpResponse1 = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader1 = new StreamReader(httpResponse1.GetResponseStream()))
                    {
                        csv = streamReader1.ReadToEnd();
                      
                        String data = csv.ToString();
                        if (String.IsNullOrEmpty(csv) || data.Contains("No report exists with the given inputs"))
                        {
                            return null;
                        }
                        var csvReader = new CsvHelper.CsvReader(new StringReader(csv));
                        csvReader.Configuration.IsHeaderCaseSensitive = false;
                        csvReader.Configuration.WillThrowOnMissingField = false;
                        csvReader.Configuration.RegisterClassMap<AdvertiseAdgroupSpendReportMap>();
                        var items = csvReader.GetRecords<AdvertiseAdgroupSpendReport>().ToList();
                        foreach (var item in items)
                        {

                            item.Date = date;
                            csvWriter.WriteRecord(item);
                        }


                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    GhostService.isErrorOccured = true;
                    GhostService.comments += ex.ToString();
                }

            }

            return stream.ToArray();

        }

        public byte[] RetrieveRevcontentSpend(int spendAccountId, DateTime date)
        {
            string dateString = date.ToString("yyyy-MM-dd");
            var stream = new MemoryStream();
            var textWriter = new StreamWriter(stream);
            using (var csvWriter = new CsvHelper.CsvWriter(textWriter))
            {
                // Get spend account details
                var spendAcct = RSTRepository.GetSpendAccountById(spendAccountId);

                // write header record
                csvWriter.WriteField("Date");
                csvWriter.WriteField("Name");
                csvWriter.WriteField("Impressions");
                csvWriter.WriteField("Clicks");
                csvWriter.WriteField("Cost");
                csvWriter.NextRecord();

                using (WebClient client = new CGWebClient())
                {
                    //catch the initial cookies
                    client.DownloadString("http://www.revcontent.com/login");

                    //Log in
                    var parameters = new NameValueCollection();
                    parameters.Add("name", spendAcct.Username);
                    parameters.Add("password", spendAcct.Password);
                    parameters.Add("login", "Sign+In");
                    client.UploadValues("http://www.revcontent.com/login", parameters);

                    var parameters1 = new NameValueCollection();
                    parameters1.Add("csv_all", "1");
                    parameters1.Add("csv", "1");
                    var campaignReport = client.UploadValues(string.Format("https://www.revcontent.com/boosts?tab=all&enabled=both&system_status=all&start_date="+dateString+"&end_date="+dateString), parameters1);
                    var rTxt = Encoding.ASCII.GetString(campaignReport);
                    if (String.IsNullOrEmpty(rTxt))
                    {
                        return null;
                    }
                    var csvReader = new CsvHelper.CsvReader(new StringReader(rTxt));
                    csvReader.Configuration.IsHeaderCaseSensitive = false;
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    csvReader.Configuration.RegisterClassMap<RevcontentSpendReportMap>();

                    var items = csvReader.GetRecords<RevcontentSpendReport>().ToList();
                    foreach (var item in items)
                    {
                        item.Date = date;
                        csvWriter.WriteRecord(item);
                    }
                }
            }

            return stream.ToArray();

        }
        
        #endregion

    }
}
