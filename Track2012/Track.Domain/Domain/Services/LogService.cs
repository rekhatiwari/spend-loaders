﻿using System;
using System.Diagnostics;
using Track.Domain.Interfaces;
using Track.Data;
using Track.Domain.Repositories;
using log4net;
using Track.Domain.Models;
using System.Net.Mail;
using System.Linq;

namespace Track.Domain.Services
{
    public class LogService : RepositoryBase<RSTEntities>, ILogService
    {

        private static ILog TrackLog;
        private static ILog SessionLog;
        private static ILog ClickLog;


        public LogService()
        {
            log4net.Config.XmlConfigurator.Configure();
            TrackLog = log4net.LogManager.GetLogger("TrackLogger");
            SessionLog = log4net.LogManager.GetLogger("SessionLogger");
            ClickLog = log4net.LogManager.GetLogger("ClickLogger");
        }


        #region Ghost

        public void WriteGhostLog(int spendAccountId, string message)
        {

            using (var db = new RSTEntities())
            {
                var logEntry = new GhostLog()
                {
                    SpendAccountId = spendAccountId,
                    Message = message,
                    TimeStamp = DateTime.UtcNow
                };
                db.GhostLogs.Add(logEntry);
                db.SaveChanges();
            }
        }

        #endregion

        #region Email
        public void SendEmail(string toAddress, string subject, string body)
        {
            SendEmail(CreateMailMessage(to: toAddress, subject: subject, body: body));
        }
        public void SendEmail(MailMessage message)
        {
            try
            {
                using (var client = new SmtpClient())
                {
                    client.Send(message);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                //WriteError(ex);
            }
        }

        public MailMessage CreateMailMessage(string from = null, string to = null, string cc = null, string bcc = null, string replyTo = null, string subject = null, string body = null, bool htmlBody = false)
        {
            var mm = new MailMessage()
            {
                Sender = new MailAddress("reports@richrivermedia.com"),
                From = new MailAddress(from ?? "reports@richrivermedia.com")
            };

            (to ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(o => mm.To.Add(o));
            (cc ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(o => mm.CC.Add(o));
            (bcc ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(o => mm.Bcc.Add(o));
            (replyTo ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(o => mm.ReplyToList.Add(o));

            mm.Subject = subject;
            mm.Body = body ?? "";
            mm.IsBodyHtml = htmlBody;

            return mm;
        }
        #endregion

    }
}
