﻿using CsvHelper;
using FiftyOne.Foundation.Mobile.Detection;
using FiftyOne.Foundation.Mobile.Detection.Factories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Track.Data;
using Track.Domain.Helpers;
using Track.Domain.Interfaces;
using Track.Domain.Models;
using Track.Extensions;

namespace Track.Domain.Services
{
    
    public class YahooGeminiService : IYahooGeminiService
    {        
        IRSTRepository RSTRepository { get; set; }
        public YahooGeminiService(IRSTRepository rstRepository)
        {
            RSTRepository = rstRepository;
        }
        public string RetrieveSpend(DateTime date, int spendAccountId)
        {
            YahooOAuthClient yahoo = new YahooOAuthClient(RSTRepository);
            try {
                //load spend account
                yahoo.LoadSpendAccount(spendAccountId);
                yahoo.Date = date;
                yahoo.LoadConfig();
                //yahoo.GetToken();
                yahoo.RefreshTokens();
                string jobId = yahoo.GenerateCustomReport();
                if (!string.IsNullOrEmpty(jobId)) {
                    var reportEndpoint = yahoo.GetReportEndpoint(jobId);
                    if (!string.IsNullOrEmpty(reportEndpoint)) {
                        string csv = yahoo.GetReportData(reportEndpoint);
                        if (!string.IsNullOrEmpty(csv))
                        {
                            return csv;
                        }
                    }
                }

            } catch (Exception e) {
                GhostService.isErrorOccured = true;
                GhostService.comments += e.ToString();
                throw e;
            }
            return null;
        }

    }

    internal class YahooOAuthClient
    {
        public IRSTRepository RSTRepository { get; set; }
        private const string AuthorizeUrl = "https://api.login.yahoo.com/oauth2/request_auth";
        private const string TokenEndpoint = "https://api.login.yahoo.com/oauth2/get_token";

        public DateTime Date { get; set; }
        public SpendAccountConfig config { get; set; }
        public SpendAccount spendAccount { get; set; }

        public YahooOAuthClient(IRSTRepository rstRepository) {
            RSTRepository = rstRepository;
        }
        public void GetToken()
        {

            string authInfo = config.clientId + ":" + config.clientSecret;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(TokenEndpoint);
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Basic " + authInfo);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";


            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //&code=ygmk4e3
                string postData = "grant_type=authorization_code&redirect_uri=oob&code=" + config.authCode;
                streamWriter.Write(postData);
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                var getTokensObj = JsonConvert.DeserializeObject<GetTokensObject>(responseText);

                config.accessToken = getTokensObj.access_token;
                config.refreshToken = getTokensObj.refresh_token;

            }
        }

        public void RefreshTokens()
        {
            try
            {
                string authInfo = config.clientId + ":" + config.clientSecret;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.login.yahoo.com/oauth2/get_token");
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Basic " + authInfo);

                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string postData = "redirect_uri=oob&grant_type=refresh_token&refresh_token=" + config.refreshToken;
                    streamWriter.Write(postData);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    var getTokensObj = JsonConvert.DeserializeObject<GetTokensObject>(responseText);

                    config.accessToken = getTokensObj.access_token;
                    config.refreshToken = getTokensObj.refresh_token;
                    //update SpendAccount.AdditionalNotes with the new config                
                    spendAccount.AdditionalNotes = JsonConvert.SerializeObject(config);
                    //Persist changes to the DB
                    UpdateSpendAccountConfig();
                }
            }
            catch (Exception er)
            {
                Console.WriteLine("Unable to Refresh tokens : " + er);
                GhostService.isErrorOccured = true;
                GhostService.comments += er.ToString();
            }
        }

        public string GenerateCustomReport(string jsonRequest = "") {
            // first make call to API to make a report request.  Will get back a response with a 'jobId' that can then be used to make another call, which will have the URL for a CSV report
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.admanager.yahoo.com/v1/rest/reports/custom");
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + config.accessToken);

            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                if (string.IsNullOrEmpty(jsonRequest)) {

                    jsonRequest = "{ \"cube\": \"performance_stats\"," +
                                " \"fields\": [" +
                                "{ \"field\": \"Campaign ID\" }," +
                                "{ \"field\": \"Campaign Name\" }," +
                                "{ \"field\": \"Budget\" }," +
                                "{ \"field\": \"Spend\" }," +
                                "{ \"field\": \"Impressions\" }," +
                                "{ \"field\": \"Clicks\" }," +
                                "{ \"field\": \"Average CPC\" }," +
                                "{ \"field\": \"Average CPM\" }," +
                                "{ \"field\": \"Day\" }" +
                                "]," +
                                " \"filters\": [" +
                                "{" +
                                " \"field\": \"Advertiser ID\"," +
                                " \"operator\": \"=\"," +
                                " \"value\": \"" + config.advertiserId + "\"" +
                                "}," +
                                "{" +
                                "\"field\": \"Day\"," +
                                "\"operator\": \"between\"," +
                                "\"from\": \"" + Date.ToString("yyyy-MM-dd") + "\"," +
                                "\"to\": \"" + Date.ToString("yyyy-MM-dd") + "\"" +
                                "}" +
                                "]" +
                                "}";

                }
                streamWriter.Write(jsonRequest);

            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            string jobId = "";

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                var statsObj = JsonConvert.DeserializeObject<StatsObject>(responseText);
                jobId = statsObj.response.jobId;
            }

            return jobId;

        }

        public string GetReportEndpoint(string jobId) {

            string reportEndpoint;
            string errorCode;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.admanager.yahoo.com/v1/rest/reports/custom/" + jobId + "?advertiserId=" + config.advertiserId);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + config.accessToken);

            httpWebRequest.Method = "GET";
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(10000);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    var statsObj = JsonConvert.DeserializeObject<StatsObject>(responseText);

                    if (statsObj.response.status == "completed")
                    {
                        return reportEndpoint = statsObj.response.jobResponse;
                    }
                    else if (statsObj.errors.Count > 0)
                    {
                        errorCode = statsObj.errors[0].code;
                    }
                }

            }

            return "";

        }

        public string GetReportData(string reportEndpoint) {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(reportEndpoint);
            httpWebRequest.Method = "GET";

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var csvText = streamReader.ReadToEnd();
                return csvText;
                
            }
        }

        public List<SpendReport> MapCsvToList(string csvResults) {
            var csv = new CsvHelper.CsvReader(new StringReader(csvResults));
            List<SpendReport> spendReport = new List<SpendReport>();

            while (csv.Read()) {
                spendReport.Add(
                    new SpendReport() {
                        Campaign = csv.GetField("Campaign Name"),
                        Source = "Yahoo Gemini",
                        Date = DateTime.Parse(csv.GetField("Day")),
                        Impressions = int.Parse(csv.GetField("Impressions")),
                        Cost = decimal.Parse(csv.GetField("Spend")),
                        Clicks = int.Parse(csv.GetField("Clicks")),
                        AverageCPC = decimal.Parse(csv.GetField("Average CPC")),
                        AverageCPM = decimal.Parse(csv.GetField("Average CPM")),
                        SiteId = spendAccount.SiteId
                    }
                  );
            }
            return spendReport;
        }

        public void RegisterDbEntries(List<SpendReport> spendReportEntries) {
            int count = RSTRepository.LoadAds(spendReportEntries, spendAccount, null);
        }
                
        public void LoadConfig() {
            var configJSON = spendAccount.AdditionalNotes;
            SpendAccountConfig accountConfig = JsonConvert.DeserializeObject<SpendAccountConfig>(configJSON);
            this.config = accountConfig;
        }

        public void LoadSpendAccount(int spendAccountId)
        {
            spendAccount =  RSTRepository.GetSpendAccountById(spendAccountId);

        }

        public void UpdateSpendAccountConfig() {

            RSTRepository.SaveSpendAccount(spendAccount);
        }

        #region helpers
        public class GetTokensObject
        {
            public string access_token { get; set; }
            public int expires_in { get; set; }
            public string refresh_token { get; set; }
            public string xoauth_yahoo_guid { get; set; }
        }
        public class StatsObject
        {
            public List<StatsObjectError> errors { get; set; }
            public string timestamp { get; set; }
            public StatsObjectResponse response { get; set; }
        }

        public class StatsObjectResponse
        {
            public string jobId { get; set; }
            public string status { get; set; }
            public string jobResponse { get; set; }
        }

        public class StatsObjectError
        {
            public int errIndex { get; set; }
            public string code { get; set; } // error code
        }


        public class SpendAccountConfig
    {
        public string clientId { get; set; }
        public string clientSecret { get; set; }
        public string accessToken { get; set; }
        public string advertiserId { get; set; }
        public string refreshToken { get; set; }
        public string authCode { get; set; }

    }
        #endregion 
    }



}

