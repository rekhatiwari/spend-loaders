﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Track.Domain.Enums;
using Track.Domain.Interfaces;

namespace Track.Domain.Services
{
	public class FileService : IFileService
	{
		protected HttpServerUtilityBase Server { get; set; }

		public FileService(HttpServerUtilityBase server)
		{
			Server = server;
		}

        public SpendFileType DetermineFileFormat(string content)
        {
            if (string.IsNullOrEmpty(content)) return SpendFileType.Empty;

            if (content.StartsWith("<?xml version"))
                return SpendFileType.Xml;

            var count = Convert.ToDecimal(content.Length);
            //var val = Convert.ToDecimal(content.Count(c => c == '<'));

            decimal low = 0.001M;
            if (Convert.ToDecimal(content.Count(c => c == '<')) / count > low)
                return SpendFileType.Xml;
            else if (Convert.ToDecimal(content.Count(c => c == '\t')) / count > low)
                return SpendFileType.Tsv;
            else if (Convert.ToDecimal(content.Count(c => c == ',')) / count > low)
                return SpendFileType.Csv;

            return SpendFileType.Undetermined;
        }

        public byte[] Zip(string fileName, string fileType, byte[] byteEncoded)
        {
            byte[] fileZipped;
            using (var zip = new ZipFile(string.Format("ghostzipped_{0}", fileName)))
            {
                zip.AddEntry(string.Format("{0}.{1}", fileName, fileType), byteEncoded);

                using (MemoryStream zipStream = new MemoryStream())
                {
                    zip.Save(zipStream);
                    fileZipped = zipStream.ToArray();
                }
            }
            return fileZipped;
        }

        public string Unzip(Stream inputStream, int expectedFileCount, string expectedFileExtension)
        {
            string str = string.Empty;
            using (var zip = ZipFile.Read(inputStream))
            {
                zip.ParallelDeflateThreshold = -1;  // 

                foreach (var entry in zip)
                {
                    if (!string.IsNullOrEmpty(expectedFileExtension) && !entry.FileName.EndsWith(expectedFileExtension))
                        return string.Format("Error: invalid file extension found in ZIP file, expecting {0}", expectedFileExtension);

                    using (var ms = new MemoryStream())
                    {
                        entry.Extract(ms);
                        ms.Position = 0;
                        using (var reader = new StreamReader(ms))
                        {
                            str = reader.ReadToEnd();
                        }
                    }

                    if (!string.IsNullOrEmpty(str))
                        break;
                }
            }
            return str;
        }

        //GZip can only handle one file
        public string GUnzip(Stream inputStream)
        {
            string str = string.Empty;
            using (GZipStream decompressionStream = new GZipStream(inputStream, CompressionMode.Decompress))
            {
                using (var reader = new StreamReader(decompressionStream))
                {
                    str = reader.ReadToEnd();
                }
            }
            return str;
        }

        public string ConvertBytesToString(byte[] fileAsBytes, Encoding encoding)
        {
            return encoding.GetString(fileAsBytes);
        }

		public string SaveFile(byte[] fileAsBytes, int id, string spendAccount)
		{
			var fileName = string.Format("{0}..\\SpendFiles\\{1}_{2}.zip", Server.MapPath("~/"), id, spendAccount);
			using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
			{
				stream.Write(fileAsBytes, 0, fileAsBytes.Length);
			}
			return fileName;  //path
		}

		public byte[] ReadAllBytes(Stream inputStream)
		{
			inputStream.Position = 0;

			var buffer = new byte[16 * 1024];
			using (var ms = new MemoryStream())
			{
				int read;
				while ((read = inputStream.Read(buffer, 0, buffer.Length)) > 0)
				{
					ms.Write(buffer, 0, read);
				}
                inputStream.Position = 0;
				return ms.ToArray();
			}
		}

		public List<FileInfo> GetTrackingScriptsInfo()
		{
			var dir = new DirectoryInfo(string.Format("{0}\\p4TrackingScripts\\", Server.MapPath("~/")));

			return dir.GetFiles("*.php").OrderByDescending(o => o.Name).ToList();
		}

        public List<FileInfo> GetStyleScriptsInfo()
        {
            var dir = new DirectoryInfo(string.Format("{0}\\p4StyleScripts\\", Server.MapPath("~/")));

            return dir.GetFiles("*.php").OrderByDescending(o => o.Name).ToList();
        }

        public FileInfo GetTrackingScript(string fileName)
        {
            return new FileInfo(string.Format("{0}\\p4TrackingScripts\\{1}", Server.MapPath("~/"), fileName));
        }
        public FileInfo GetStyleScript(string fileName)
        {
            return new FileInfo(string.Format("{0}\\p4StyleScripts\\{1}", Server.MapPath("~/"), fileName));
        }

        public FileInfo getDocument(string fileName)
        {
            return new FileInfo(string.Format("{0}\\Files\\{1}", Server.MapPath("~/"), fileName));
        }

		public void SaveTrackingScript(HttpPostedFileBase file)
		{
			if (!file.FileName.EndsWith(".php"))
				throw new ArgumentException("Invalid file type. Must be Php.");

			var fi = new FileInfo(string.Format("{0}..\\TrackingScripts\\{1}", Server.MapPath("~/"), Path.GetFileName(file.FileName)));

			file.SaveAs(fi.FullName);
		}

        public string GenerateWordFenceScript(string trackingScriptContents)
        {
            var regex = new Regex("/\\*(?>(?:(?>[^*]+)|\\*(?!/))*)\\*/", RegexOptions.Multiline);
            return regex.Replace(trackingScriptContents, "/*\nPlugin Name: Wordfence Security\nDescription: Wordfence Security - Anti-virus and Firewall security plugin for WordPress\nVersion: 10.1\n*/\n", 1);

        }

        public string GenerateThinScript(string trackingScriptContents)
        {
            var startTag = "AUTOUPDATE";
            var endTag = "ENDAUTOUPDATE";
            if (trackingScriptContents.Contains(startTag) && trackingScriptContents.Contains(endTag))
            {
                int startIndex = trackingScriptContents.IndexOf(startTag) + startTag.Length;
                int endIndex = trackingScriptContents.IndexOf(endTag, startIndex);
                return trackingScriptContents.Remove(startIndex, endIndex - startIndex);
            }
            return trackingScriptContents;

        }


    }
}
