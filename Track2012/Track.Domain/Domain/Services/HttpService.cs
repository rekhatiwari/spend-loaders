﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Track.Domain.Interfaces;
using System.Web;
using System.Text.RegularExpressions;
using System.Net;

namespace Track.Domain.Services
{
	public class HttpService : IHttpService
	{
		public HttpContextBase Context { get; private set; }
		protected ILogService LogService { get; set; }

		public HttpService(HttpContextBase context, ILogService log)
		{
			Context = context;
			LogService = log;
		}

		/// <summary>
		/// Parses the domain from the url
		/// ex. http://123.com/?a=a&b=b returns 123.com
		/// </summary>
		/// <param name="url">url to be parsed (http://123.com/?a=a&b=b)</param>
		/// <returns>domain (123.com)</returns>
        public string ParseURLForDomain(string host)
		{
            try
            {
                if (!string.IsNullOrEmpty(host))
                {
                    if (!host.Contains("://"))
                        host = "http://" + host;

                    string domain = new Uri(host).Host;
                    domain = domain.Replace("www.", "");    // remove www. from domain if exists

                    // Strip sub-domain
                    if (domain.Contains("co.uk"))
                        return domain;

                    string[] domainSplit = domain.Split('.');

                    if (domainSplit.Length > 2)
                    {
                        domain = string.Format("{0}.{1}", domainSplit[domainSplit.Length - 2], domainSplit[domainSplit.Length - 1]);
                    }

                    return domain;
                }
            }
            catch
            {
                return null;
            }
            return null;
		}

		public string ParseRelatedTerms(string terms)
		{
			string dataText = "";
			if (!string.IsNullOrEmpty(terms))
			{
				string[] termsSplit = terms.Split(System.Environment.NewLine.ToCharArray());
				foreach (string term in termsSplit)
				{
					if (!string.IsNullOrEmpty(term))
						dataText += term + ",";
				}
				dataText = dataText.TrimEnd(new char[] { ',' });
			}
			return dataText;
		}

		public string ParseRelatedTermsForDisplay(string terms)
		{
			string displayText = "";
			if (!string.IsNullOrEmpty(terms))
			{
				string[] termsSplit = terms.Split(new char[] { ',' });
				foreach (string term in termsSplit)
				{
					displayText += term + System.Environment.NewLine;
				}
				displayText = displayText.TrimEnd(System.Environment.NewLine.ToCharArray());
			}
			return displayText;
		}

        public string GetHostName()
        {
            if (Context == null) return "";

            return Context.Server != null ? Context.Server.MachineName : string.Empty;
        }

		public string GetEntryPage()
		{
			if (Context == null) return "";

			return Context.Request.Url.AbsoluteUri;
		}

		public bool IsValidIP(string addr)
		{
			//create our match pattern
			var pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
			//create our Regular Expression object
			var check = new Regex(pattern);
			//boolean variable to hold the status
			bool valid = false;
			//check to make sure an ip address was provided
			if (addr == "")
			{
				//no address provided so return false
				valid = false;
			}
			else
			{
				//address provided so use the IsMatch Method
				//of the Regular Expression object
				valid = check.IsMatch(addr, 0);
			}
			//return the results
			return valid;
		}

		/// <summary>
		/// Gets UserAgent From HttpContext
		/// </summary>
		/// <param name="context">HttpContext</param>
		/// <returns>UA string</returns>
		public string GetUserAgent()
		{
			try
			{
				string uaFromQueryString = null;
				if (!String.IsNullOrEmpty(Context.Request["ua"]))
				{
					uaFromQueryString = (string)Context.Request["ua"];
					return uaFromQueryString;
				}

				return Context.Request.UserAgent == null ? string.Empty : Context.Request.UserAgent;
			}
			catch
			{
			}
			return string.Empty;
		}

		public string GetIpAddress()
		{
			if (Context == null) return "";

			try
			{
				string __ip;
				IPAddress address;
				if (!string.IsNullOrEmpty(Context.Request.Headers["X-Forwarded-For"]))
				{
					__ip = Context.Request.Headers["X-Forwarded-For"];

					if (__ip.IndexOf(',') > 0)
					{
						bool found = false;
						string[] ipTests = __ip.Split(new char[] { ',' });
						foreach (string ipTest in ipTests)
						{
							if (IPAddress.TryParse(ipTest, out address))
							{
								found = true;
								__ip = address.ToString();
								break;
							}
						}

						if (!found)
						{
							__ip = "127.0.0.1";
						}
					}
				}
				else if (System.Net.IPAddress.TryParse(Context.Request.UserHostAddress, out address))
				{
					__ip = address.ToString();
				}
				else
				{
					__ip = "127.0.0.1";
				}

				return __ip;
			}
			catch
			{
			}
			return string.Empty;
		}

		/*public void ClearStonwallCache(string id)
		{
			const string url = "http://api.getxmlfeeds.com/Sw.svc/Cache?id={0}";
			var web = new WebClient();
			web.DownloadStringCompleted += (o, e) => {  if (e.Error != null) { 
                //LogService.WriteError(e.Error); 
            } };
			web.DownloadStringAsync(new Uri(string.Format(url, id)));
		}*/

		public void DeleteCachedResource(string domain, string cacheKey)
		{
			try
			{
				var ub = new UriBuilder();
				ub.Scheme = "http";
				ub.Host = domain;
				ub.Path = "Cache/Clear/key";

				// Null or Empty CacheKey parameter will clear all Domain Cached items
				if (!string.IsNullOrEmpty(cacheKey))
					ub.Query = "CacheKey=" + cacheKey;

				var request = WebRequest.Create(ub.Uri.AbsoluteUri) as HttpWebRequest;
				request.Method = "DELETE";
				request.BeginGetResponse(r => { /* Don't care about the response, do nothing. */ }, null);
			}
			catch (Exception ex)
			{
                ex.ToString();
			}
		}

        public string GetNonThreatToken(DateTime dateTime)
        {
            dateTime = dateTime.ToUniversalTime();
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(dateTime.ToString("yyyy-MM-ddTHH:mm")));
        }

	}
}
