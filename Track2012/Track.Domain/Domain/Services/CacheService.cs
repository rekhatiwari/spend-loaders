﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.Configuration;
using Track.Domain.Interfaces;
using Track.Data;
using Track.Domain.Models;

namespace Track.Domain.Services
{
    public class CacheService : ICacheService
    {
        public ILogService LogService { get; set; }


        private int? cacheTimeout = null;
        public CacheService()
        {
            if (!this.cacheTimeout.HasValue)
            {
                int timeout;
                if (!int.TryParse(WebConfigurationManager.AppSettings["CacheTimeoutMinutes"], out timeout))
                    timeout = 5;

                this.cacheTimeout = timeout;
            }
        }

        private ReaderWriterLockSlim ghostCounterLock = new ReaderWriterLockSlim();
        public Dictionary<int, GhostCounterItem> GetGhostCounter()
        {
            var key = formatKey("ghostCounter");
            Dictionary<int, GhostCounterItem> cachedObject = null;

            ghostCounterLock.EnterUpgradeableReadLock();
            try
            {
                cachedObject = this.getCachedObject<Dictionary<int, GhostCounterItem>>(key);
                if (cachedObject == null)
                {
                    ghostCounterLock.EnterWriteLock();
                    try
                    {
                        cachedObject = new Dictionary<int, GhostCounterItem>();
                        this.cacheObject(key, cachedObject, 0, CacheItemPriority.NotRemovable);
                    }
                    finally
                    {
                        ghostCounterLock.ExitWriteLock();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                ghostCounterLock.ExitUpgradeableReadLock();
            }

            return cachedObject;
        }

        public void CacheRemove(string key)
        {
            cacheRemove(formatKey(key));
        }
        private void cacheRemove(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        private T getCachedObject<T>(string key) where T : class
        {
            return HttpRuntime.Cache[key] as T;
        }

        private void cacheObject<T>(string key, T value, int timeOut, CacheItemPriority priority)
        {
            try
            {
                // Check the parameters
                if (key == null || value == null)
                {
                    return;
                }

                if (timeOut <= 0)
                    HttpRuntime.Cache.Insert(key, value, null, DateTime.UtcNow.AddYears(1), Cache.NoSlidingExpiration, priority, null);
                else
                    HttpRuntime.Cache.Insert(key, value, null, DateTime.UtcNow.AddMinutes(timeOut), Cache.NoSlidingExpiration, priority, null);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private static string formatKey(string key)
        {
            return "t_" + key;
        }


        #region Cache Locks
        private ReaderWriterLock cacheLocker = new ReaderWriterLock();
        private Dictionary<string, ReaderWriterLock> cacheLocks = new Dictionary<string, ReaderWriterLock>();
        private TimeSpan cacheLockTimeout = TimeSpan.FromSeconds(5);
        private ReaderWriterLock getCacheLock(string key)
        {
            this.cacheLocker.AcquireReaderLock(this.cacheLockTimeout);

            try
            {
                if (!this.cacheLocks.ContainsKey(key))
                {
                    LockCookie __lc = this.cacheLocker.UpgradeToWriterLock(this.cacheLockTimeout);
                    try
                    {
                        if (!this.cacheLocks.ContainsKey(key))
                            this.cacheLocks.Add(key, new ReaderWriterLock());
                    }
                    finally
                    {
                        this.cacheLocker.DowngradeFromWriterLock(ref __lc);
                    }
                }

                return this.cacheLocks[key];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.cacheLocker.ReleaseReaderLock();
            }
        }
        #endregion
    }
}
