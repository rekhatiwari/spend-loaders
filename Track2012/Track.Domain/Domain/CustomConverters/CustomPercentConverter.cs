﻿using System;
using System.ComponentModel;

namespace Track.Domain.CustomConverters
{
    public class CustomPercentConverter : TypeConverter
    {

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            decimal response = 0;
            string val = value.ToString();
            val = val.Replace("%", "");
            if (decimal.TryParse(val, out response))
            {
                response = response / 100;
            }
            return response;
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(decimal);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            return value.ToString();
        }

        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            return true;
        }

    }
}
