﻿using System;

namespace Track.Domain.Helpers
{
    public class ExceptionHelper
    {

        public static string GetSimpleExcpetion(Exception ex)
        {
            var message = string.Empty;

            if (ex == null) message = null;

            if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                message = ex.InnerException.Message;
            else if (!string.IsNullOrEmpty(ex.Message))
                message = ex.Message;
            else
                message = ex.ToString();

            return message;
        }

    }
}
