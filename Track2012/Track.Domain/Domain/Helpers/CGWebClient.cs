﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Track.Domain.Helpers
{
    public class CGWebClient : WebClient
    {
        public string UserAgent { get; set; }
        public int Timeout { get; set; }
        public bool AutomaticDecompression { get; set; }

        private CookieContainer m_container = new CookieContainer();

        protected override WebRequest GetWebRequest(Uri address)
        {
            Console.WriteLine("Request being made to {0}", address.AbsoluteUri);

            // Uncomment to see cookie collection being passed around
            var c = m_container.GetCookies(address);
            for (int i = 0; i < c.Count; i++)
            {
                var j = c[i];
                Console.WriteLine(string.Format("{0}={1}", j.Name, j.Value));
            }

            WebRequest request = base.GetWebRequest(address);
            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).CookieContainer = m_container;

                if (!string.IsNullOrEmpty(this.UserAgent))
                    (request as HttpWebRequest).UserAgent = this.UserAgent;

                if (this.Timeout > 0)
                    (request as HttpWebRequest).Timeout = this.Timeout;

                if (this.AutomaticDecompression)
                    (request as HttpWebRequest).AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            }
            return request;
        }
    }

}
