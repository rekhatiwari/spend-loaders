﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Track.Domain.Helpers
{
	public class EnumHelper
	{
		public static List<SelectListItem> ToSelectList<T>() where T : struct, IConvertible
		{
			if (typeof(T).IsEnum)
				return Enum.GetValues(typeof(T)).Cast<T>().Select(o => new SelectListItem() { Text = o.ToString(), Value = Convert.ToInt32(o).ToString() }).ToList();

			throw new Exception("Type is invalid. Must be an enum");
		}
	}
}
