﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Track.Domain.Helpers
{
	public class RegexHelper
	{
		public const string DomainNamePattern = @"^[a-zA-Z0-9\-]+\.[a-zA-Z]{2,4}$";
		public const string EmailPattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

		public static bool IsMatch(string input, string pattern, RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant)
		{
			return Regex.IsMatch(input, pattern, options);
		}
	}
}
