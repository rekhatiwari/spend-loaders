﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Track.Data;
using Track.Domain.Models;

namespace Track.Domain.Interfaces
{
    public interface IParsingService
    {
        List<SpendReport> ParseOutbrainHexagramFile(byte[] fileBytes, SpendAccount spendAccount, out string dateRange);

        #region Yahoo Gemini
        List<SpendReport> ParseYahooGeminiCampaignPerformanceReport(byte[] fileBytes, SpendAccount spendAccount, out string dateRange);
        #endregion


        #region Mobile
        List<SpendReport> ParseMobileSpend(SpendAccount account, string fileContents, out string dateRange);
        #endregion

        #region Raw Spend
        List<SpendReport> ParseRawSpend(SpendAccount account, string fileContents, out string dateRange);
        #endregion

        #region Taboola
        List<SpendReport> ParseTaboolaFile(byte[] fileAsBytes, SpendAccount account, out string dateRange);
        #endregion

        #region Adblade
        List<SpendReport> ParseAdbladeFile(byte[] fileAsBytes, SpendAccount account, out string dateRange);
        #endregion

        #region Revcontent
        List<SpendReport> ParseRevcontentFile(byte[] fileAsBytes, SpendAccount account, out string dateRange);
        #endregion

        #region AdvertiseAdgroup
        List<SpendReport> ParseAdvertiseAdgroupFile(byte[] fileAsBytes, SpendAccount account, out string dateRange);
        #endregion
    }

}
