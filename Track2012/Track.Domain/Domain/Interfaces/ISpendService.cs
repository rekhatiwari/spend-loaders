﻿using Track.Data;
using Track.Domain.ViewModels;
using System;
using Track.Domain.Models;
using System.Collections.Generic;

namespace Track.Domain.Interfaces
{
	public interface ISpendService
	{
        void UploadSpend(GhostQueue entry, byte[] reportAsBytes);
        void UploadSpend(UploadSpendModel model);
        List<Exception> uploadRawSpend(string contents, int spendAccountId);
        byte[] RetrieveTaboolaSpend(int spendAccountId, DateTime date);
        byte[] RetrieveYahooGeminiSpend(int spendAccountId, DateTime date);
        byte[] RetrieveAdbladeSpend(int spendAccountId, DateTime date);
        byte[] RetrieveRevcontentSpend(int spendAccountId, DateTime date);
        byte[] RetrieveAdvertiseAdgroupSpend(int spendAccountId, DateTime date);
        //byte[] RetrieveBingAPISpend(int value, DateTime date);
    }
}
