﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Track.Domain.Interfaces
{
	public interface IHttpService
	{
		HttpContextBase Context { get; }

		string ParseURLForDomain(string url);
		string ParseRelatedTerms(string terms);
		string ParseRelatedTermsForDisplay(string terms);
        string GetHostName();
        string GetEntryPage();
		bool IsValidIP(string addr);
		string GetUserAgent();
		string GetIpAddress();
		/// <summary>
		/// Clears the Stonewall Cache.
		/// When id is a domain, it will clear all domain level cached items
		/// When id is null or empty, it will clear all global cached items
		/// </summary>
		/// <param name="id">DomainName or Empty</param>
		//void ClearStonwallCache(string id);
		void DeleteCachedResource(string domain, string cacheKey);
        string GetNonThreatToken(DateTime dateTime);
	}
}
