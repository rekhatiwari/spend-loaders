﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Track.Data;
using Track.Domain.Models;
using Track.Domain.ViewModels;

namespace Track.Domain.Interfaces
{
    public interface IRSTRepository
    {
        GhostQueue GetGhostEntry(int id);
        List<int> GetAllGhostEntries();
        GhostQueueViewModel GetGhostQueueForSpendAccount(int spendAccountId);
        void UpdateGhostRequest(int id, bool? requestSent, bool? requestFulfilled, bool? requestError);
        SpendAccount GetSpendAccountById(int id);
        void SaveSpendAccount(SpendAccount model);
        ASAUploadStatu CreateUploadStatus(SpendAccount account, string user, byte[] file);
        void UpdateUploadStatus(ASAUploadStatu asa, string message);
        Site GetSiteById(int id);
        int LoadAds(List<SpendReport> ads, SpendAccount account, TimeSpan? ts);
        void SpendReportsCurrencyConversion(int spendAccountId, List<SpendReport> adsSummarized);
        void PopulateSpendInSCMSPS(DateTime? date, int? accountId);
        void CompleteUploadStatus(ASAUploadStatu status, SpendAccount account, SimpleMailMessage message, System.Text.StringBuilder dc, bool updateStatus);
        void SendDailyStatusEmail();
        void AddToDailyJobStatus(DailyJobStatu record);
        //User GetUserByUserName(string userName);
        List<FinstatsSummaryModel> FinstatsSummary(DateTime? fromDate, DateTime? toDate, string userId, string divisionName, string groupName, string siteName);
    }
}
