﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using Track.Domain.Enums;

namespace Track.Domain.Interfaces
{
	public interface IFileService
	{
        SpendFileType DetermineFileFormat(string content);
        byte[] Zip(string fileName, string fileType, byte[] byteEncoded);
        string Unzip(Stream inputStream, int expectedFileCount, string expectedFileExtension);
        string GUnzip(Stream inputStream);
        string ConvertBytesToString(byte[] fileAsBytes, Encoding encoding);
		string SaveFile(byte[] fileAsBytes, int id, string spendAccount);
        byte[] ReadAllBytes(Stream inputStream);

        List<FileInfo> GetTrackingScriptsInfo();
        List<FileInfo> GetStyleScriptsInfo();

        FileInfo GetTrackingScript(string fileName);
        FileInfo GetStyleScript(string fileName);

        void SaveTrackingScript(HttpPostedFileBase file);

        string GenerateWordFenceScript(string trackingScriptContents);
        string GenerateThinScript(string trackingScriptContents);

        FileInfo getDocument(string fileName);
    }
}
