﻿using System;
using System.Collections.Generic;

namespace Track.Domain.Interfaces
{
    public interface IYahooGeminiService
	{
        string RetrieveSpend(DateTime date, int spendAccountId);
    }
}
