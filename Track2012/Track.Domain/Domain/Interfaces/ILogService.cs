﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Track.Domain.Models;

namespace Track.Domain.Interfaces
{
	public interface ILogService
	{
		//void WriteError(Exception ex);
		//void WriteError(string message, Exception ex);
		//void WriteError(string message);
		//void WriteWarning(string message);
		//void WriteInformation(string message);
		//void WriteFailureAudit(string message);
		//void WriteSuccessAudit(string message);

        void WriteGhostLog(int spendAccountId, string message);
        //void WriteScheduledTask(string message, int scheduledTaskId);

        //void LogSpend(string status, decimal spend, int spendAccountId, string spendAccountName);

        #region Email

        void SendEmail(string toAddress, string subject, string body);
        void SendEmail(MailMessage message);
        MailMessage CreateMailMessage(string from = null, string to = null, string cc = null, string bcc = null, string replyTo = null, string subject = null, string body = null, bool htmlBody = false);

        #endregion
    }
}
