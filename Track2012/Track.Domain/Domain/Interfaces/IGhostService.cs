﻿using System;
using Track.Data;

namespace Track.Domain.Interfaces
{
    public interface IGhostService
    {
        void ProcessGhostQueueItem(GhostQueue entry);
        void ProcessItem(GhostQueue entry, DateTime date);
    }
}
