﻿using System.Collections.Generic;
using Track.Data;
using Track.Domain.Models;

namespace Track.Domain.Interfaces
{
    public interface ICacheService
    {
        Dictionary<int, GhostCounterItem> GetGhostCounter();

        void CacheRemove(string key);
    }
}
