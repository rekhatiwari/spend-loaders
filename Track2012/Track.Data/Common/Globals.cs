﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Data.Common
{
    public static class Globals
    {
        public const string NewStatus = "New";
        public const string ProcessingStatus = "Processing";
        public const string CompletedStatus = "Complete";
        public const string FailedStatus = "Failed";
        public const string DeletedStatus = "Deleted";

        // Facebook
        public const string ApiVersion = "v2.7";
#if DEBUG
        public const string Host = "http://localhost:12696";
#else
        public const string Host = "https://track.p4pi.net";
#endif
    }
}
