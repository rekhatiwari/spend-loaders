﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Track.Data.Common.Attributes
{
    public class MaxKeywordAttribute : ValidationAttribute
    {
        public MaxKeywordAttribute(int maxKeywords)
        {
            this.MaxKeywords = maxKeywords;
        }

        public int MaxKeywords { get; private set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string keywords = Convert.ToString(value);

            if (keywords.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Length > this.MaxKeywords)
            {
                return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
            }

            return null;
        }
    }
}
