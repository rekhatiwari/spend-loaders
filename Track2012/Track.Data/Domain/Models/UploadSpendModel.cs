﻿using System.Web;

namespace Track.Domain.Models
{
	public class UploadSpendModel
	{
		public int SpendAccountId { get; set; }
		public HttpPostedFileBase File { get; set; }
        public int? GhostQueueId { get; set; }
	}
}
