﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Domain.Models
{
    public class TaboolaSpendReport
    {
        public DateTime Date { get; set; }
        public string Campaign { get; set; }
        public int Impressions { get; set; }
        public int Clicks { get; set; }
        public int Actions { get; set; }
        public decimal Spend { get; set; }

    }

    public sealed class TaboolaSpendReportMap : CsvClassMap<TaboolaSpendReport>
    {
        public TaboolaSpendReportMap()
        {
            Map(m => m.Date).Name("Date");
            Map(m => m.Campaign).Name("Campaign");
            Map(m => m.Impressions).Name("Impressions");
            Map(m => m.Clicks).Name("Clicks");
            Map(m => m.Actions).Name("Actions");
            Map(m => m.Spend).Name(new string[] { "Spend", "Spent" }).ConvertUsing(row =>
            {
                string spend = String.Empty;

                if (!row.TryGetField("Spend", out spend))
                {
                    if (!row.TryGetField("Spent", out spend))
                    {
                        return 0;
                    }
                }
                return Convert.ToDecimal(spend.Trim().Replace('$', ' ').Trim());
            });
        }
    }
}
