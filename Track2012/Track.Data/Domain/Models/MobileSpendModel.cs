﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Domain.Models
{
    public class MobileSpendModel
    {

        public string Day { get; set; }
        public string Campaign { get; set; }
        public string Impressions { get; set; }
        public string Clicks { get; set; }
        public string Cost { get; set; }

    }
}
