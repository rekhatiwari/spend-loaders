﻿using CsvHelper.Configuration;

namespace Track.Domain.Models
{
    public class OutbrainSpendReport
    {

        public string Date { get; set; }
        public string Campaign { get; set; }
        public string Clicks { get; set; }
        public string Cost { get; set; }
        public string Impressions { get; set; }
        public string CTR { get; set; }

    }

    public sealed class OutbrainSpendReportMap : CsvClassMap<OutbrainSpendReport>
    {
        public OutbrainSpendReportMap()
        {
            Map(m => m.Date).Name("Date");
            Map(m => m.Campaign).Name("Campaign");
            Map(m => m.Clicks).Name("Clicks");
            Map(m => m.Cost).Name("Cost");
            Map(m => m.Impressions).Name("Impressions");
            Map(m => m.CTR).Name("CTR(%)");
        }
    }

}
