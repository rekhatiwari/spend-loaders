﻿using System.Text;

namespace Track.Domain.Models
{
    public class SimpleMailMessage
    {

        public string Subject { get; set; }
        public StringBuilder Body { get; set; }

    }
}
