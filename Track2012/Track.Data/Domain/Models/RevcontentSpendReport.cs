﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Domain.Models
{
    public class RevcontentSpendReport
    {
        public DateTime Date { get; set; }
        public string Campaign { get; set; }
        public int Impressions { get; set; }
        public int Clicks { get; set; }
        public decimal Spend { get; set; }

    }

    public sealed class RevcontentSpendReportMap : CsvClassMap<RevcontentSpendReport>
    {
        public RevcontentSpendReportMap()
        {
            Map(m => m.Date).Name("Date");
            Map(m => m.Campaign).Name("Name");
            Map(m => m.Impressions).Name("Impressions").TypeConverterOption(NumberStyles.AllowThousands);
            Map(m => m.Clicks).Name("Clicks");
            Map(m => m.Spend).Name("Cost").ConvertUsing(row =>
            {
                string spend = String.Empty;
                if (!row.TryGetField("Cost", out spend))
                {
                    return 0;
                }
                return Convert.ToDecimal(spend.Trim().Replace('$', ' ').Trim());
            });
        }
    }
}
