﻿using System;

namespace Track.Domain.Models
{
    public class GhostCounterItem
    {
        public int Iterations { get; set; }
        public DateTime LastServiced { get; set; }
        public DateTime? RequestFulfilled { get; set; }
    }
}
