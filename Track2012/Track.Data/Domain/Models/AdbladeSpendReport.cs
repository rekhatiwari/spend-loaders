﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration;

namespace Track.Domain.Models
{
    public class AdbladeSpendReport
    {
        public DateTime Date { get; set; }
        public string Campaign { get; set; }
        public int Impressions { get; set; }
        public int Clicks { get; set; }
        public decimal Spend { get; set; }
    }

    public sealed class AdbladeSpendReportMap : CsvClassMap<AdbladeSpendReport>
    {
        public AdbladeSpendReportMap()
        {
            Map(m => m.Date).Name("Date");
            Map(m => m.Campaign).Name("Campaign Name");
            Map(m => m.Impressions).Name("Views");
            Map(m => m.Clicks).Name("Clicks");
            Map(m => m.Spend).Name(new string[] { "Spend", "Total Spend" }).ConvertUsing(row =>
            {
                string spend = String.Empty;

                if (!row.TryGetField("Spend", out spend))
                {
                    if (!row.TryGetField("Total Spend", out spend))
                    {
                        return 0;
                    }
                }
                return Convert.ToDecimal(spend.Trim().Replace('$', ' ').Trim());
            });
        }
    }

}


