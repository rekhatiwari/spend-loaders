﻿namespace Track.Domain.Models
{
    public class GhostReportItem
    {

        public string GoogleReportLink { get; set; }
        public bool IsEmailOnlyAccessUser { get; set; }
        public byte[] BingReportZipped { get; set; }

    }
}
