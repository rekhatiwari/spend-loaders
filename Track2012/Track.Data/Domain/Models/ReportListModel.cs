﻿using System;

namespace Track.Domain.Models
{
    public class ReportListModel
    {

        public string ReportName { get; set; }
        public Uri Uri { get; set; }

    }
}
