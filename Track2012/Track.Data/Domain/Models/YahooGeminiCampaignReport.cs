﻿using CsvHelper.Configuration;

namespace Track.Domain.Models
{
    public class YahooGeminiCampaignReport
    {

       //public string Day { get; set; }
        public string Date { get; set; }
        public string CampaignName { get; set; }
        public string Impressions { get; set; }
        public string Clicks { get; set; }
        public string Spend { get; set; }
        public string CTR { get; set; }
    }

    public sealed class YahooGeminiCampaignReportMap : CsvClassMap<YahooGeminiCampaignReport>
    {
        public YahooGeminiCampaignReportMap()
        {
            Map(m => m.Date).Name("Date");
            Map(m => m.CampaignName).Name("Campaign Name");
            Map(m => m.Impressions).Name("Impressions");
            Map(m => m.Clicks).Name("Clicks");
            Map(m => m.Spend).Name("Spend");
            Map(m => m.CTR).Name("CTR");

        }
    }
}
