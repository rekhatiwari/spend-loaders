﻿using System.Collections.Generic;
using Track.Data;

namespace Track.Domain.ViewModels
{
    public class GhostQueueViewModel
    {
        public int SpendAccountId { get; set; }
        public List<GhostQueue> GhostQueue { get; set; }
    }
}
