﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Track.Data;

namespace Track.Domain.ViewModels
{
	public class DashboardViewModel
	{
		public List<FinstatsSummaryModel> FinstatsSummary { get; set; }
		public FinstatsSummaryModel FinstatsSummaryTotal { get; set; }
	}
}
