﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Collections.Specialized;

namespace Track.Domain.ViewModels
{
    public class QueryStringAliasViewModel : IValidatableObject
	{
        [Display(Name = "Ad Source")]
        public string AdSource { get; set; }

        [Range(1, 99)]
        public int? AdSourceOrder { get; set; }

		[Display(Name = "Campaign")]
		public string Campaign { get; set; }

        [Range(1, 99)]
        public int? CampaignOrder { get; set; }

		[Display(Name = "Ad Group")]
		public string AdGroup { get; set; }

        [Range(1, 99)]
        public int? AdGroupOrder { get; set; }

		[Display(Name = "Keyword")]
		public string Keyword { get; set; }

        [Range(1, 99)]
        public int? KeywordOrder { get; set; }

		[Display(Name = "Query")]
		public string Query { get; set; }

        [Range(1, 99)]
        public int? QueryOrder { get; set; }

		[Display(Name = "Source Site")]
		public string SourceSite { get; set; }

        [Display(Name = "Traffic Type")]
        public string TrafficType { get; set; }

        [Display(Name = "Match Type")]
        public string MatchType { get; set; }

        [Range(1, 99)]
        public int? MatchTypeOrder { get; set; }

        [Range(1, 99)]
        public int? TrafficTypeOrder { get; set; }

		[Display(Name = "Redirect")]
		public string Redirect { get; set; }

		[Display(Name = "Type Tag")]
		public string TypeTag { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var allNames = this.ToNameValueCollection();

            var existingOrders = new HashSet<int>();

            foreach (var key in allNames.AllKeys)
            {
                var alias = allNames[key];
                var OrderValue = getOrder(key);
                if (OrderValue == null)
                {
                    continue;
                }

                if (String.IsNullOrEmpty(alias) && OrderValue != null)
                {
                    yield return new ValidationResult(String.Format("{0} has no alias", key));
                }

                if (existingOrders.Contains((int)OrderValue))
                {
                    yield return new ValidationResult(String.Format("{0} has a duplicate order value", key));
                }
                existingOrders.Add((int)OrderValue);
            }

            foreach (var value in existingOrders){
                if (value > existingOrders.Count)
                {
                    yield return new ValidationResult("Query String Alias orders must be sequential starting at 1");
                    break;
                }
            }
         }

        
		public NameValueCollection ToNameValueCollection()
		{
			return new NameValueCollection()
			{
				{ "adsource", AdSource },
                { "matchtype", MatchType },
                { "campaign", Campaign },
                { "adgroup", AdGroup },
				{ "keyword", Keyword },
				{ "query", Query },
				{ "sourcesite", SourceSite },
				{ "traffictype", TrafficType },
				{ "redirect", Redirect },
				{ "typetag", TypeTag },
			};
		}

        public int? getOrder(String aliasName)
        {

            var OrderLookup = new Dictionary<string, int?>(){
                {"adsource" ,   AdSourceOrder},
                {"matchtype",    MatchTypeOrder},
                {"campaign",    CampaignOrder},
                {"adgroup" ,    AdGroupOrder},
                {"keyword",     KeywordOrder },
                {"traffictype", TrafficTypeOrder},
                {"query",       QueryOrder}
            };
           
            int? order;
            OrderLookup.TryGetValue(aliasName, out order);
            return (order == 0) ? (int?)null : order;
        }

        public List<KeyValuePair<string, string>> getQueryOrderedString()
        {

            var returnList = new List<KeyValuePair<string, string>>();
            NameValueCollection qs = this.ToNameValueCollection();
            foreach (string key in qs)
            {
                if (!String.IsNullOrEmpty(qs[key])){
                    int order = getOrder(key)?? int.MaxValue;
                    returnList.Add(new KeyValuePair<string, string>(qs[key], key));
                }
            }

            returnList.Sort((firstPair, nextPair) =>
            {
                var firstOrder = getOrder(firstPair.Value) ?? int.MaxValue;
                var secondOrder = getOrder(nextPair.Value) ?? int.MaxValue;
                return firstOrder.CompareTo(secondOrder);
            });

            return returnList;
        }
	}

}
