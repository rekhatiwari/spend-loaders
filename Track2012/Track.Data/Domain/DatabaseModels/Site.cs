﻿using System.ComponentModel.DataAnnotations;

namespace Track.Data
{
    [MetadataType(typeof(SiteMeta))]
    public partial class Site
    {
        public class SiteMeta
        {
			[Required]
			[Display(Name = "Site Name")]
			public string Name { get; set; }
			[Display(Name = "Is Active")]
			public bool IsActive { get; set; }
			[Display(Name = "Always Redirect")]
			public bool AlwaysRedirect { get; set; }

            [DisplayFormat(DataFormatString = "{0:P0}")]
            public decimal? MaxBlendRatio { get; set; }
        }

    }
}
