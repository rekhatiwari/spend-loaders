﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Track.Data
{
    [MetadataType(typeof(GhostLogMeta))]
    public partial class GhostLog
    {
        public class GhostLogMeta
        {
            [Display(Name = "Added")]
            [DisplayFormat(DataFormatString = "MM/dd/yy H:mm:ss")]
            [DataType(DataType.Date)]
            public DateTime TimeStamp { get; set; }
        }

    }
}
