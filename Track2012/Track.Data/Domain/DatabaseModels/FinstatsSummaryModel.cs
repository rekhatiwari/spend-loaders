﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Track.Data
{
	[MetadataType(typeof(FinstatsSummaryModelMeta))]
	public partial class FinstatsSummaryModel
	{
        public DateTime? DateMin { get; set; }
        public DateTime? DateMax { get; set; }

		public class FinstatsSummaryModelMeta
		{
			[DisplayFormat(DataFormatString = DateFormats.DAY)]
			public DateTime DateID { get; set; }

            [DisplayFormat(DataFormatString = DateFormats.DAY)]
            public DateTime DateMin { get; set; }

            [DisplayFormat(DataFormatString = DateFormats.DAY)]
            public DateTime DateMax { get; set; }

		}
	}
}
