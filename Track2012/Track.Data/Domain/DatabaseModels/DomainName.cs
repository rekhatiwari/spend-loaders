﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Track.Data
{
	[MetadataType(typeof(DomainNameMeta))]
	public partial class DomainName
	{
		public class DomainNameMeta
		{
			[Display(Name = "Domain Name")]
			[Required]
            //[RegularExpression(@".+\..+", ErrorMessage = "Domains must be of the form xxxx.yyy (ie hotmail.com).  Note that subdomains (ie test.hotmail.com) are not allowed.")]
			public string DomainName1 { get; set; }
			[Display(Name = "Label")]
			public string DomainLabel { get; set; }
			[Display(Name = "Description")]
			public string Description { get; set; }
			[Display(Name = "Seed Term")]
			public string SeedTerm { get; set; }
			[Display(Name = "Is Active")]
			public string IsEnabled { get; set; }
			[Display(Name = "Is Graphed")]
			public string IsGraphed { get; set; }
			[Display(Name = "Always Redirect")]
			public string AlwaysRedirect { get; set; }
			[Display(Name = "Is Revenue Domain")]
			public string IsRevenueDomain { get; set; }
			[Display(Name = "Default Template")]
			public string SmoothieId { get; set; }
			[Display(Name = "Mobile Template")]
			public string MobileSmoothieId { get; set; }
			[Display(Name = "Override Related Keywords")]
            [AllowHtml]
			public string OverrideRelatedKeywords { get; set; }
            [Display(Name = "Show Search Bar")]
            public Nullable<bool> ShowSearchBar { get; set; }
            [Display(Name = "Site")]
			[Range(1, double.MaxValue)]
			public int SiteId { get; set; }
            [Display(Name = "Type")]
            public byte? TypeId { get; set; }

			[Display(Name = "Revenue Yesterday")]
			public string RevPast1Day { get; set; }
			[Display(Name = "Spend Yesterday")]
			public string SpendPast1Day { get; set; }
            [Display(Name = "Sessions Yesterday")]
            public string ApiSessionsPast1Day { get; set; }
		}
	}
}
