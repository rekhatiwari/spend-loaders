﻿using ExpressiveAnnotations.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace Track.Data
{
	[MetadataType(typeof(SpendAccountMeta))]
	public partial class SpendAccount
	{
		public class SpendAccountMeta
		{
			[Display(Name = "Site")]
			[Required]
			[Range(1, double.MaxValue, ErrorMessage = "Please choose a site.")]
			public int SiteId { get; set; }
			[Required]
			[Display(Name = "Provider")]
			public int ProviderId { get; set; }
			[Required]
			[Display(Name = "Report Type")]
			public int ReportTypeId { get; set; }
			[Display(Name = "Friendly Name")]
			public string Label { get; set; }

            [Required]
			[Display(Name = "Username")]
            public string Username { get; set; }

			[Display(Name = "Password")]
            [RequiredIf("OptInGoogleGhost == true", ErrorMessage = "Password is required for Ghost")]
            public string Password { get; set; }
			[Required]
			[Display(Name = "Identifier")]
			public string Identifier { get; set; }
			[Display(Name = "Is Active")]
			public bool IsActive { get; set; }

            [Display(Name = "Started On")]
			public DateTime StartedOn { get; set; }

            [Required]
            [Display(Name = "Time Zone")]
            public string TimeZone { get; set; }

            [Display(Name = "Observes DST")]
            public string ObservesDST { get; set; }

            [Display(Name = "Address")]
			public string Address { get; set; }

			[Display(Name = "Phone")]
			public string Phone { get; set; }

            [Display(Name = "Name On Account")]
			public string NameOnAccount { get; set; }

            [Display(Name = "Launch Location")]
			public string LaunchLocation { get; set; }

            [Display(Name = "Launch IP")]
			public string LaunchIp { get; set; }

            [Required]
			[Display(Name = "Spend Account Currency")]
			public string CurrencyCode { get; set; }

            [Required]
			[Display(Name = "Country Code")]
			public string CountryCode { get; set; }

            [Display(Name = "Credit Card Number")]
			public string CreditCardNumber { get; set; }

            [Display(Name = "Credit Card Country")]
			public string CreditCardCountry { get; set; }

            [Display(Name = "Credit Card Issuing Bank")]
			public string CreditCardIssuingBank { get; set; }

            [Display(Name = "Launch Notes")]
			public string LaunchNotes { get; set; }

            [Display(Name = "Auto pull spend")]
            public bool? OptInGoogleGhost { get; set; }

            [Display(Name = "Custom UA")]
            [RequiredIf("OptInGoogleGhost == true", ErrorMessage = "Custom AU is required for Ghost")]
            public string AgentString { get; set; }

            [Display(Name = "Reporting Email")]
            public string ReportingEmail { get; set; }

            [Display(Name = "Start Time")]
            public string GhostStartTime { get; set; }

            [Display(Name = "End Time")]
            public string GhostEndTime { get; set; }

            [Display(Name = "Status")]
            public string SpendStatusType { get; set; }

            [Display(Name = "Alert when missing spend")]
            public bool? AlertWhenMissingSpend { get; set; }

            [Display(Name = "Province/State")]
            public string Region { get; set; }

            [Display(Name = "City")]
            public string City { get; set; }

            [Display(Name = "Postal Code")]
            public string PostalCode { get; set; }

            [Display(Name = "Api Token")]
            public string Token { get; set; }
        }
    }
}

