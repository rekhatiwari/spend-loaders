﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Track.Data
{
	[MetadataType(typeof(ProxyMeta))]
	public partial class Proxy
	{
		public class ProxyMeta
		{
			[Display(Name = "Proxy IP Address")]
			public string IP { get; set; }
			[Display(Name = "Proxy Username")]
			public string UserName { get; set; }
			[Display(Name = "Proxy Password")]
			public string Password { get; set; }

		}
	}
}
