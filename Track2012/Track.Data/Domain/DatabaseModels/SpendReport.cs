﻿namespace Track.Data
{
    public partial class SpendReport
    {
        public static SpendReport CopyTo(SpendReport old)
        {

            var report = new SpendReport()
            {
                Date = old.Date,
                AdwordsAccount = old.AdwordsAccount,
                AdwordsId = old.AdwordsId,
                DomainName = old.DomainName,
                DomainId = old.DomainId,
                Source = old.Source,
                SourceType = old.SourceType,
                Campaign = old.Campaign,
                AdGroup = old.AdGroup,
                Keyword = old.Keyword,
                Clicks = old.Clicks,
                Impressions = old.Impressions,
                Cost = old.Cost,
                AverageCPC = old.AverageCPC,
                AveragePosition = old.AveragePosition,
                AverageCPM = old.AverageCPM,
                DestinationUrl = old.DestinationUrl,
                DestinationDomain = old.DestinationDomain,
                SiteId = old.SiteId,
                DateOffset = old.DateOffset,
                Hour = old.Hour,
                RedirectId = old.RedirectId,
                SpendReportsId = old.SpendReportsId,
                SpendAccount = old.SpendAccount,
                MatchTypeId = old.MatchTypeId,
                SpendMatchType = old.SpendMatchType,
                IsRolledUp = old.IsRolledUp,
                Bid = old.Bid,
                Budget = old.Budget
                
            };

            return report;
        }

    }
}
