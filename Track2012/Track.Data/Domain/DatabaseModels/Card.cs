﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Track.Data
{
    [MetadataType(typeof(CardMeta))]
    public partial class Card
    {
        public class CardMeta
        {
            public int ID { get; set; }
            public int ProgramID { get; set; }
            [Display(Name = "Credit Card Number")]
            public string CardNumber { get; set; }
            public string Status { get; set; }
            public string FullName { get; set; }
            public DateTime? DateCreated { get; set; }
            public string Card4Digits { get; set; }
            public string CVV { get; set; }
            public string Expiry { get; set; }
            public Guid? GroupID { get; set; }
            public string Notes { get; set; }
            public DateTime? StatusDate { get; set; }
            public Decimal? CreditLimit { get; set; }
            [Display(Name = "Credit Card Currency")]
            public string CardCurrency { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string Region { get; set; }
            [Display(Name = "Credit Card Country")]
            public string CountryCode { get; set; }
            public string PhoneNumber { get; set; }
            public Byte? StatusTypeId { get; set; }
            public string PostalCode { get; set; }
        }
    }
}
