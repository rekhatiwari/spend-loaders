﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Track.Data
{
    [MetadataType(typeof(GhostQueueMeta))]
    public partial class GhostQueue
    {
        public class GhostQueueMeta
        {
            [Display(Name = "Added")]
            [DisplayFormat(DataFormatString = DateFormats.DATE_AND_TIME)]
            [DataType(DataType.Date)]
            public DateTime? AddedToQueue { get; set; }

            [Display(Name = "Queued")]
			[DisplayFormat(DataFormatString = DateFormats.DATE_AND_TIME)]
            [DataType(DataType.Date)]
            public DateTime? QueuedUTCDateTime { get; set; }
        }
    }
}
