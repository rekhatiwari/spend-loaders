﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Track.Data
{
    [MetadataType(typeof(CardProgramMeta))]
    public partial class CardProgram
    {
        public class CardProgramMeta
        {
            public int ID { get; set; }
            public string AcctNum { get; set; }
            public string CurrencyCode { get; set; }
            [Display(Name = "Credit Card Issuing Bank")]
            public string IssuingBank { get; set; }
            public string IssuingCountry { get; set; }
            public int AccountCurrencyCode { get; set; }
            public Byte BusinessEntityID { get; set; }
            public bool IsActive { get; set; }
            public string Login { get; set; }
            public Decimal? Limit { get; set; }
            public Decimal? AvailableCredit { get; set; }
            public Byte? StatementDateDay { get; set; }
            public Byte? StatementDatePaymentLagInDays { get; set; }
        }
    }
}
