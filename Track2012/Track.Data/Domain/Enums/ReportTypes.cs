﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.Domain.Enums
{
	public enum ReportTypes
	{
		GoogleAdgroup = 1,
		GoogleKeyword = 3,
		MSNKeyword = 2,
		Adknowledge = 4,
		Yahoo = 5,
        OtherInline = 6,
        OtherExperimental = 7
	}
}
