﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.Domain.Enums
{
	public enum RtaHandlers
	{
		DefaultHandler,
		ARHandler,
		CleanARSessionsHandler
	}
}
