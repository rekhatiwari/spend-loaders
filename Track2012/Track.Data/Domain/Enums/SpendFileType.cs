﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Domain.Enums
{
    public enum SpendFileType
    {
        Xml,
        Csv,
        Tsv,
        Empty,
        Undetermined
    }
}
