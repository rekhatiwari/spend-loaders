﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.Domain.Enums
{
	public enum NotificationLevel
	{
		Info,
		Error,
		Alert,
		Warning
	}
}
