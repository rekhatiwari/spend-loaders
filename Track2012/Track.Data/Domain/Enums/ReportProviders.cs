﻿namespace Track.Domain.Enums
{
    public enum ReportProviders
    {
        Google = 1,
        MSN = 2,
        Adknowledge = 3,
        Yahoo = 4,
        Other = 5,
        MSNExperimental = 6,
        Undetermined = 7,
        Facebook = 8,
        Outbrain = 9,
        Disqus = 10,
        YahooGemini = 11,
        Adknown = 12,
        Raw = 13,
        Taboola = 14,
        Adblade = 15,
        AdvertiseAdgroup = 16,
        Revcontent = 17
    }
}
