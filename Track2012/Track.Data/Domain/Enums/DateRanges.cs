﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.Domain.Enums
{
	public enum DateRanges
	{
        Today,
		Yesterday,
		TwoDaysAgo,
        ThreeDaysAgo,
        ThreeDaysPriorToYesterday,
        LastThreeDays,
		LastSevenDays,
        SevenDaysPriorToYesterday,
        ThisWeek,
        LastWeek,
        LastFourteenDays,
        LastThirtyDays,
        ThisMonth,
        LastMonth,
        MonthToDate,
        Custom
	}
}
