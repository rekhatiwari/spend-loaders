﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Track.Domain.Enums
{
	public enum RtaTemplates
	{
		AR,
        NoSessions,
        NoClicks,
        Custom
	}
}
