﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Track.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class RSTEntities : DbContext
    {
        public RSTEntities()
            : base("name=RSTEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ASAUploadStatu> ASAUploadStatus { get; set; }
        public virtual DbSet<GhostQueue> GhostQueues { get; set; }
        public virtual DbSet<SpendAccount> SpendAccounts { get; set; }
        public virtual DbSet<SpendProvider> SpendProviders { get; set; }
        public virtual DbSet<SpendReport> SpendReports { get; set; }
        public virtual DbSet<SpendReportType> SpendReportTypes { get; set; }
        public virtual DbSet<SpendMatchType> SpendMatchTypes { get; set; }
        public virtual DbSet<SpendStatusType> SpendStatusTypes { get; set; }
        public virtual DbSet<Timezone> Timezones { get; set; }
        public virtual DbSet<SpendStatu> SpendStatus { get; set; }
        public virtual DbSet<SpendAccountMappingAlias> SpendAccountMappingAliases { get; set; }
        public virtual DbSet<SpendAccountInteraction> SpendAccountInteractions { get; set; }
        public virtual DbSet<GhostLog> GhostLogs { get; set; }
        public virtual DbSet<DailyJobStatu> DailyJobStatus { get; set; }
        public virtual DbSet<Site> Sites { get; set; }
    
        public virtual int SpendReports_Populate_SCM_SPS(Nullable<System.DateTime> date, Nullable<int> spendAccountId)
        {
            var dateParameter = date.HasValue ?
                new ObjectParameter("date", date) :
                new ObjectParameter("date", typeof(System.DateTime));
    
            var spendAccountIdParameter = spendAccountId.HasValue ?
                new ObjectParameter("spendAccountId", spendAccountId) :
                new ObjectParameter("spendAccountId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SpendReports_Populate_SCM_SPS", dateParameter, spendAccountIdParameter);
        }
    
        public virtual int SCM_SPS_SpendReports_Single_Populate(Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate, Nullable<int> spendAccountId)
        {
            var fromDateParameter = fromDate.HasValue ?
                new ObjectParameter("fromDate", fromDate) :
                new ObjectParameter("fromDate", typeof(System.DateTime));
    
            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("toDate", toDate) :
                new ObjectParameter("toDate", typeof(System.DateTime));
    
            var spendAccountIdParameter = spendAccountId.HasValue ?
                new ObjectParameter("spendAccountId", spendAccountId) :
                new ObjectParameter("spendAccountId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SCM_SPS_SpendReports_Single_Populate", fromDateParameter, toDateParameter, spendAccountIdParameter);
        }
    
        public virtual int SpendReportsDeleteOld(Nullable<System.DateTime> from, Nullable<System.DateTime> to, Nullable<int> accountId)
        {
            var fromParameter = from.HasValue ?
                new ObjectParameter("from", from) :
                new ObjectParameter("from", typeof(System.DateTime));
    
            var toParameter = to.HasValue ?
                new ObjectParameter("to", to) :
                new ObjectParameter("to", typeof(System.DateTime));
    
            var accountIdParameter = accountId.HasValue ?
                new ObjectParameter("accountId", accountId) :
                new ObjectParameter("accountId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SpendReportsDeleteOld", fromParameter, toParameter, accountIdParameter);
        }
    
        public virtual int SpendReportsCurrencyConversion(Nullable<int> spendAccountId, Nullable<System.DateTime> fromDate, Nullable<System.DateTime> toDate)
        {
            var spendAccountIdParameter = spendAccountId.HasValue ?
                new ObjectParameter("spendAccountId", spendAccountId) :
                new ObjectParameter("spendAccountId", typeof(int));
    
            var fromDateParameter = fromDate.HasValue ?
                new ObjectParameter("fromDate", fromDate) :
                new ObjectParameter("fromDate", typeof(System.DateTime));
    
            var toDateParameter = toDate.HasValue ?
                new ObjectParameter("toDate", toDate) :
                new ObjectParameter("toDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SpendReportsCurrencyConversion", spendAccountIdParameter, fromDateParameter, toDateParameter);
        }
    }
}
