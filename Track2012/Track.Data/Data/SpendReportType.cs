//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Track.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SpendReportType
    {
        public SpendReportType()
        {
            this.GhostQueues = new HashSet<GhostQueue>();
            this.SpendAccounts = new HashSet<SpendAccount>();
        }
    
        public byte Id { get; set; }
        public string Type { get; set; }
        public byte ProviderId { get; set; }
    
        public virtual ICollection<GhostQueue> GhostQueues { get; set; }
        public virtual ICollection<SpendAccount> SpendAccounts { get; set; }
        public virtual SpendProvider SpendProvider { get; set; }
    }
}
