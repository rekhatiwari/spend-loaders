//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Track.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class GhostQueue
    {
        public int Id { get; set; }
        public Nullable<int> SpendAccountId { get; set; }
        public Nullable<byte> ProviderId { get; set; }
        public Nullable<byte> ReportTypeId { get; set; }
        public string ReportingLabel { get; set; }
        public string Identifier { get; set; }
        public string ProxyIp { get; set; }
        public Nullable<int> ProxyPort { get; set; }
        public string ProxyUser { get; set; }
        public string ProxyPassword { get; set; }
        public Nullable<int> TimezoneId { get; set; }
        public string TimezoneAbbreviation { get; set; }
        public string TimezoneUTCOffset { get; set; }
        public Nullable<System.TimeSpan> QueuedTime { get; set; }
        public Nullable<System.DateTime> QueuedUTCDateTime { get; set; }
        public bool RequestSent { get; set; }
        public bool RequestFulfilled { get; set; }
        public bool RequestError { get; set; }
        public string UserAgent { get; set; }
        public string ReportingEmail { get; set; }
        public Nullable<System.DateTime> AddedToQueue { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    
        public virtual SpendAccount SpendAccount { get; set; }
        public virtual SpendProvider SpendProvider { get; set; }
        public virtual SpendReportType SpendReportType { get; set; }
    }
}
